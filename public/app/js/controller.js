var app= angular.module('Myapp', ['ngRoute','ngCookies','ngFileSaver','ui.bootstrap','ui.grid','ui.grid.selection','ui.grid.edit', 'ui.grid.rowEdit','ui.grid.cellNav', 'ui.grid.exporter','ui.mask','ngSanitize', 'ngCsv', 'ngDialog','angularjs-datetime-picker','angularMoment','ngFileUpload','imageupload']).constant('indiatimezone', {
    timezone: 'Asia/Kolkata' // e.g. 'Asia/Kolkata'
});
app.config(function($httpProvider) {
    //Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;
});

app.config(function($routeProvider) {
	$routeProvider
	.when('/',{
		templateUrl: 'app/dashboard/home.html',
    controller:'homeController'
	})
  .when('/feature/:id',{
    templateUrl: 'app/admin/feature.html',
    controller:'featureController'
  })
  .when('/tenant',{ 
    templateUrl: 'app/tenant/tenetlist.html',
     controller:'tenantController'
  })
	.when('/owner',{ 
    templateUrl: 'app/owner/owner.html',
     controller:'showownerController'
  })/*123*/
   .when('/ownerdetails/:id/:newOwner',{ 
    templateUrl: 'app/owner/ownerdetails.html',
     controller:'ownerdetailsController',
     controllerAs :'ownerdetails'
  })
  .when('/property',{ 
    templateUrl: 'app/property/available_property.html',
     controller:'showpropertyController'
  })
  //Agreement
  .when('/agreement',{ 
    templateUrl: 'app/project/agreement.html',
    controller:'projectController'
   })
  .when('/agreement_prop',{ 
    templateUrl: 'app/project/agreement_property.html',
    controller:'projectController'
   })
  .when('/select_tenant',{ 
    templateUrl: 'app/project/select_tenant.html',
    controller:'projectController'
   })
  /*project*/
  .when('/basic_info',{ 
    templateUrl: 'app/project/basic_info.html',
    controller:'projectController'
   })
  .when('/phase_setup/:projectid',{ 
    templateUrl: 'app/project/phase_setup.html',
    controller:'projectController'
   })
  .when('/building_setup/:phaseid',{ 
    templateUrl: 'app/project/building_setup.html',
    controller:'projectController'
   })
  .when('/wing_setup/:buildingid',{ 
    templateUrl: 'app/project/wing_setup.html',
    controller:'projectController'
   })
  .when('/floor_setup',{ 
    templateUrl: 'app/project/floor_setup.html',
    controller:'projectController'
   })
  .when('/unit_setup',{ 
    templateUrl: 'app/project/unit_setup.html',
    controller:'projectController'
   })
  .when('/flat_setup/:wingid/:floor/:flatcount/:numStyle',{
    templateUrl: 'app/project/flat_setup.html',
    controller:'projectController'
   })
  .when('/groundunit_setup/:phaseid/:groundunit',{
    templateUrl: 'app/project/groundunit_setup.html',
    controller:'projectController'
   })
 .when('/flat_details/:wingid',{ 
    templateUrl: 'app/project/flat_details.html',
    controller:'projectController'
   })
  /*project*/
  .when('/project_details/:id',{ 
    templateUrl: 'app/property/project_details.html',
    controller: 'projectController as projectDetail'
  })
   .when('/project_list/:all',{ 
    templateUrl: 'app/property/project_list.html',
    controller:'projectController'
  })
  /*.when('/project',{ 
    templateUrl: 'app/property/add_project.html',
     controller:'projectController'
  })*/
	.when('/property_details/:id',{ 
		templateUrl: 'app/property/property_details.html',
    controller: 'showpropertyController as propertyDetails'
	})
  /*88*/
  .when('/tenant_details/:id/:newTenant',{ 
    templateUrl: 'app/tenant/tenantdetails.html',
    controller: 'tenantController as tenantDetails'
  })
   .when('/addproperty/:id',{ 
    templateUrl: 'app/property/add_property.html', 
    controller:'addpropertycontroller'
  })
	.when('/addcommercial',{ 
		templateUrl: 'app/demo_page/add_commercial.html'
	})
	.when('/addpg',{ 
		templateUrl: 'app/demo_page/add_pg.html'
	})
	.when('/staff',{ 
		templateUrl: 'app/staff/staff.html',
		controller:'showStaffcontroller'
	})
	.when('/addstaff',{
		templateUrl: 'app/staff/addstaff.html',
		controller: 'staffController'
	})
	.when('/staffdetail/:memberId',{
		templateUrl: 'app/staff/staffdetail.html',
		controller: 'staffMemberDetailcontroller',
		controllerAs :'member'
	})
	.when('/rmprofile',{
		templateUrl: 'app/demo_page/rm_profile.html'
	})
	.when('/branch',{
		templateUrl: 'app/branch/branch.html',
		controller:'showBranchController'
	})
	.when('/addbranch',{
		templateUrl: 'app/branch/addbranch.html',
		controller: 'branchController'
	})
	.when('/servicereq',{
		templateUrl: 'app/demo_page/service_request.html'
	})
	.when('/servicerequested',{
		templateUrl: 'app/demo_page/requested_services.html'
	})
	.when('/showallleads',{
		templateUrl: 'app/lead/customer_leadGrid.html',
		controller: 'showleadController',
		controllerAs:'leadctrl'
	})
	.when('/viewlead',{
		templateUrl: 'app/lead/view_lead.html',
		controller:'leadFollowupcontroller as followup'
	})
  .when('/assignlead',{
    templateUrl:'app/lead/assignlead.html',
    controller : 'assignleadcontroller'
  })
	.when('/viewrequestedservice',{
		templateUrl: 'app/demo_page/view_requested_service.html'
	})
	.when('/closedrequestedservice',{
		templateUrl: 'app/demo_page/closed_requested_service.html'
	})
	.when('/branchprofile/:branchId',{
		templateUrl: 'app/branch/branch_profile.html',
		controller : 'branchDetailcontroller',
		controllerAs : 'branch'
	})
	.when('/collectorofficer',{
		templateUrl: 'app/demo_page/collector_off.html'
	})
	.when('/collectorprofile',{
		templateUrl: 'app/demo_page/collector_profile.html'
	})
	.when('/ownerlist',{ 
		templateUrl: 'app/demo_page/owner_list.html'
	})
	.when('/ownerregistration',{ 
		templateUrl: 'app/demo_page/owner_registration.html'
	})
	.when('/ownerprofile',{ 
		templateUrl: 'app/owner_profile.html'
	})
	.when('/tenentlist',{ 
		templateUrl: 'app/demo_page/tenent_list.html'
	})
	.when('/tenentprofile',{ 
		templateUrl: 'app/demo_page/tenent_profile.html'
	})
	.when('/tenentregistration',{ 
		templateUrl: 'app/demo_page/tenent_registration.html'
	})
	.when('/addlead',{ 
		templateUrl: 'app/lead/addlead.html',
		controller:'leadController'
	})
  .when('/editlead',{ 
    templateUrl: 'app/lead/edit_lead.html',
     controller:'editleadController as editleadctrl'
  })
	.when('/login',{ 
    templateUrl: 'app/authentication/login.html',
		controller: 'LoginController'
	})
	 .when('/setcredentials/:id',{
    templateUrl: 'app/staff/setuserpass.html',
    controller: 'credentialsController'
  })      

	
});
 app.run( ['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
    	//alert("test login1");
        // keep user logged in after page refresh

        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
                     
        }
 	
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        	//alert("test login2");
            // redirect to login page if not logged in and trying to access a restricted page
            setTimeout(function() {
                      $rootScope.menuData=  localStorage.testMenudata || {}; 
                 //alert("hello" + $rootScope.menuData);
                 angular.element('#topMenu').html($rootScope.menuData);
                 //alert(angular.toJson(angular.element('#topMenu').text()))
          },1000);
            

            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }
 
 ]);


app.controller('addpropertycontroller',['$routeParams','$scope','$http','projectService','$location',function($routeParams,$scope,$http,projectService,$location,server_url){

  var ownerid =  $routeParams.id;
  /* alert(ownerid);*/
  /*alert(propertyform.society_amenities);*/

  $scope.propertyform={};
  var today=new Date().setHours(0,0,0,0);
  $scope.today = new Date(today);
  $scope.addproperty = function(){

    var tempdate=$scope.propertyform.keyrdate;
    $scope.propertyform.keyrdate = tempdate.getFullYear() + "-" + ( tempdate.getMonth() >9 ? (tempdate.getMonth()+1) : "0" + (tempdate.getMonth() +1) )+ "-"  + (tempdate.getDate() >=10 ? tempdate.getDate() : "0" + tempdate.getDate());

    $scope.propertyform.society_amenities =$scope.propertyform.society_amenities.join();
    $scope.propertyform.owner_id = ownerid;
    $scope.propertyform.followup_service = angular.toJson($scope.propertyform.followup_service);
      /* alert('hello');
  alert(angular.toJson($scope.propertyform));*/
    var req={
    method: 'GET',
    url: '/InsertProperty',
    headers: {
          'Content-Type': 'application/json', data:angular.toJson($scope.propertyform)
     }}
     
          
      $http(req)
              .then(
                       function(d) {
                            $scope.propertyform={};
                            
                       },
                        function(errResponse){
                            console.error('Error while insertng property');
                        }
                   );
         $location.path('/ownerdetails/'+ownerid+'/ownerinfo');      
  };

    self.fetchAllProject = function(){
              projectService.fetchAllProject()
                  .then(
                               function(d) {
                                 $scope.projects=d;

                                  /*  self.property = d;*/
                                    /* alert(angular.toJson($scope.projects));*/
                                   /*  $scope.getArray= $scope.projects;*/
                               },
                                function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching projects');
                                }
                       );
          };
    self.fetchAllProject();

    $scope.pr=[];
   $scope.changepro=function(value){
   $scope.pr=value.project_name;
    var req={
      method: 'GET',
      url:  '/GetPhases',
      headers: {
            'Content-Type': 'application/json', data:angular.toJson(value)
       }};
       $http(req)
              .then(
                       function(d) {
                          $scope.allphases=[];
                          $scope.allphases=d.data;
                       },
                        function(errResponse){
                            console.error('Error while insertng phases');
                        });
              }
    $scope.ph=[];
     $scope.changephase=function(value){
     $scope.ph =value.name;
      var req={
        method: 'GET',
        url:  '/GetBuildings',
        headers: {
              'Content-Type': 'application/json', data:angular.toJson(value.id)
         }};
         $http(req)
                .then(function(d) {
                          $scope.allbuilding=d.data;
                         },
                          function(errResponse){
                              console.error('Error while insertng building');
                          });
      }
      $scope.b=[];
     $scope.changebuilding=function(value){
      $scope.b=value.name;
      var req={
        method: 'GET',
        url:  '/GetWings',
        headers: {
              'Content-Type': 'application/json', data:angular.toJson(value.id)
         }};
         $http(req)
                .then(
                         function(d) {
                          $scope.allwing=d.data;
                         },
                          function(errResponse){
                          console.error('Error while insertng wing');
                          });
              }
    $scope.w=[];
               $scope.changewing=function(value){
                 $scope.w=value.name;
                  var req={
                  method: 'GET',
                  url:  '/GetFlats',
                  headers: {
                        'Content-Type': 'application/json', data:angular.toJson(value.id)
                   }};
                   $http(req)
                          .then(function(d) {
                                    $scope.allflat=d.data;
                                   },
                                    function(errResponse){
                                    console.error('Error while insertng flats');
                                    });
                        }

               $scope.f=[];
               $scope.changeflats=function(value){
                 $scope.f=value.number;
               }


}]);

app.controller('projectController',['$scope','ngDialog','$rootScope','$location','$http','projectService','$routeParams','$q','$interval',function($scope,ngDialog,$rootScope,$location,$http,projectService,$routeParams,$q,$interval,server_url){
 $scope.requiredphaseid= $routeParams.phaseid;
 $scope.requiredgroundunitid= $routeParams.groundunit;
 $scope.requiredprojectid= $rootScope.projectid;
 $scope.requiredphasename= $rootScope.requiredphasename;

        $scope.myVar = false;
        $scope.myPar =  false;
          $scope.toggle = function() {
              $scope.myVar = !$scope.myVar;
              $scope.myPar = !$scope.myPar;
          };
        var self = this;
        $rootScope.project={};
        $rootScope.project.phases=[];
        // $rootScope.project.phases.buildings=[];
         //$rootScope.Project.phases.buildings.
 $scope.projects=[];
$scope.fetchAllProject = function(){
         
              projectService.fetchAllProject()
                  .then(
                               function(d) {
                                 $scope.projects=d;
                                 window.localStorage.samplepro=angular.toJson($scope.projects);
                                 
                                 /*$scope.projnm=$scope.projects[0].project_name;*/
                                  /*  self.property = d;*/
                                  /*  $scope.getArray= $scope.projects;*/
                               },
                                function(errResponse){
                                  console.log(errResponse);
                                  console.error('Error while fetching projects');
                                }
                       );
          };
         if($routeParams.all) $scope.fetchAllProject();

    
         $scope.addproject = function(){
          /*alert("addproject controller");*/
              $scope.project_details.society_amenities = $scope.project_details.society_amenities.join(); 
               $scope.project_details.location=localStorage.longlat||''; 
            /* alert(angular.toJson("addproject controller" +$scope.project_details));*/
              var req={
              method: 'POST',
              url:  '/InsertProject',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson($scope.project_details)
               }};
                    
                $http(req)
                      .then(
                               function(d) {
                                    $scope.project_details={};
                                   
                                    $rootScope.projectID=d.data.insertId;

                                  $location.path('/phase_setup/'+$rootScope.projectID); 

                               },
                                function(errResponse){
                                    console.error('Error while insertng project');
                                }
                       );
                      //$location.path('/project_list');
                  
            };
          var projectid =  $routeParams.id;
          var self = this;
          /*self.ProjectDetail=[];*/
          $scope.activeNewPhase=true;
          $scope.addPhaseControls=function(){ $scope.activeNewPhase=!$scope.activeNewPhase;};
            $scope.activeNewBuilding=true;
          $scope.addBuildingControls=function(){ $scope.activeNewBuilding=!$scope.activeNewBuilding;};
           $scope.activeNewWing=true;
          $scope.addWingControls=function(){ $scope.activeNewWing=!$scope.activeNewWing;};
  /*  $scope.PhaseDetails={}; */
           

   $scope.getphases = function(){
     var projectid= $routeParams.projectid;
    $scope.activeNewPhase=true;
     // alert(angular.toJson(phase));
    var req={
              method: 'GET',
              url:  '/GetPhases',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson(projectid)
               }};
                    
                $http(req)
                      .then(
                               function(d) {
                                  $scope.project_details={};
                                  $scope.PhaseDetails=d;
                                  $scope.PhaseDetail=d.data;
                                  $rootScope.requiredphasename=$scope.PhaseDetails.data[0].name;
                                  $rootScope.projectid=$scope.PhaseDetails.data[0].projectid;
                                  window.localStorage.phasedt=angular.toJson( $scope.PhaseDetail);
                                  /* alert(JSON.stringify(d));*/
                               },
                                function(errResponse){
                                    console.error('Error while insertng add project');
                                }
                       );
                      //$location.path('/project_list');
            };  
          
$scope.checkQuestions = function() {
    $scope.phasede=JSON.parse(window.localStorage.phasedt); 
        
 if ($scope.phasede.length > 0) { // your question said "more than one element"
   return true;
  }
  else {
   return false;
  }
};
  

            if( $routeParams.projectid){$scope.getphases();}
   $scope.getbuildings = function(){
     var phaseid= $routeParams.phaseid;
      $scope.activeNewPhase=true;
     // alert(angular.toJson(phase));
    var req={
              method: 'GET',
              url:  '/GetBuildings',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson(phaseid)
               }};
                    
                $http(req)
                      .then(
                               function(d) {
                                    $scope.project_details={};
                                  $scope.BuildingDetails=d;
                                   
                               },
                                function(errResponse){
                                    console.error('Error while insertng add project');
                                }
                       );
                      //$location.path('/project_list');
                  
            };  
            if( $routeParams.phaseid){$scope.getbuildings();}

   $scope.getwings = function(){
     var buildingid= $routeParams.buildingid;
      $scope.activeNewPhase=true;
     // alert(angular.toJson(phase));
    var req={
              method: 'GET',
              url:  '/GetWings',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson(buildingid)
               }};
                    
                $http(req)
                      .then(
                               function(d) {
                                    $scope.project_details={};
                                  $scope.WingDetails=d;
                               },
                                function(errResponse){
                                    console.error('Error while insertng add wing');
                                }
                       );
                      //$location.path('/project_list');
                  
            }; 
       if( $routeParams.buildingid){$scope.getwings();}
$scope.getFlats = function(wid){
     var wingid= wid || $routeParams.wingid;
      $scope.activeNewPhase=true;
     var req={
              method: 'GET',
              url:  '/GetFlats',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson(wingid)
               }};
                $http(req)
                      .then(
                               function(d) {
                                    $scope.project_details={};
                                  $scope.FlatDetails=d;
                                  if($scope.FlatDetails.data && $scope.FlatDetails.data.length >0){
                                    $scope.gridOptions.data=$scope.FlatDetails.data;
                                   }
                                },
                                function(errResponse){
                                    console.error('Error while insertng add project');
                                }
                       );
            }; 
            if($routeParams.wingid){$scope.getFlats();}

  $scope.GetGroundUnits = function(){
    $scope.requiredphaseid= $routeParams.phaseid;
    $scope.activeNewPhase=true;
    var req={
              method: 'GET',
              url:  '/GetGroundUnits',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson($scope.requiredphaseid)
               }};
               $http(req)
                      .then(
                               function(d) {
                                  $scope.project_details={};
                                  $scope.groundDetails=d;
                                 
                                  if($scope.groundDetails.data && $scope.groundDetails.data.length >0){
                                    $scope.gridOptions.data=$scope.groundDetails.data;
                                   }
                               },
                                function(errResponse){
                                    console.error('Error while insertng add project');
                                }
                       );
            };
           
if( $routeParams.phaseid){$scope.GetGroundUnits();}
             

  $scope.addphases = function(phase){
     phase.projectid= $routeParams.projectid;
      $scope.activeNewPhase=true;
     /* alert(angular.toJson(phase));*/
    var req={
              method: 'POST',
              url:  '/AddPhase',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson(phase)
               }};
                    
                $http(req)
                      .then(
                               function(d) {
                                    $scope.project_details={};
                                  /*  alert(angular.toJson(self.propertyform));*/
                                  /*$location.path('/building_setup/'+d.data.insertId);*/ 
                               },
                                function(errResponse){
                                    console.error('Error while insertng add phases');
                                }
                       );
                      //$location.path('/project_list');
                      $scope.getphases();
                  
            };


  $scope.addBuilding = function(building){
    delete building.building;
    delete building.ground;
    building.phase_id= $routeParams.phaseid;   
                  var req={
                            method: 'POST',
                            url:  '/AddBuilding',
                            headers: {
                                  'Content-Type': 'application/json', data:angular.toJson(building)
                             }};
                              $http(req)
                                    .then(
                                             function(d) {
                                                  $scope.project_details={};
                                             },
                                              function(errResponse){
                                                  console.error('Error while insertng building');
                                              }
                                     );
                                   $scope.getbuildings();
    };
    $scope.addGroundunit = function(ground){
    $scope.groundunit=ground;
    delete ground.ground;
    var allgrounddata={"groundunit":ground.groundunit,"name":$scope.requiredphasename,"projectid":$scope.requiredprojectid};
    var req={
              method: 'POST',
              url: '/UpdatePhase',
              headers: {
                    'Content-Type': 'application/json', data:angular.toJson(allgrounddata)
               }};
                $http(req)
                      .then(
                               function(d) {
                                    $scope.project_details={};
                                },
                                function(errResponse){
                                    console.error('Error while insertng phase');
                                }
                       );
    };

    if($routeParams.phaseid){
      $scope.tphase=$routeParams.phaseid;
      if( $scope.groundDetails && $scope.groundDetails.data.length >0) {$scope.groundunits=$scope.groundDetails;
          }else{}

    $scope.gridOpt=   { data: $scope.data, columnDefs: $scope.columnDefs,enableRowSelection: true,
    enableSelectAll: true,
    enableFiltering: true, };

     $scope.data = [
     { name: 'Jyoti', title: 'CEO' },
     { name: 'Pooja', title: 'Lowly Developer' }
   ];

     $scope.columnDefs = [
       {name: 'name', enableCellEdit: true},
       {name: 'title', enableCellEdit: true}
     ];
         $scope.gridOptions = {
                              rowHeight: 40,
                              enableCellEditOnFocus: true,
                              enableRowSelection: true,
                              enableSelectAll: true,
                              enableFiltering: true,                                  
                            };
                               
            $scope.gridOptions.enableCellEditOnFocus = true;       
                                        
            $scope.gridOptions.columnDefs = [
                   { name: 'phase_id', enableCellEdit: false },
                   { name: 'carpet_area',type:'number',enableCellEdit: true},
                   { name: 'builtup_area',enableCellEdit: true},
                   { name: 'superbuiltup_area',enableCellEdit: true},
                   { name: 'BHK'},
                   { name: 'use_Type',cellEditableCondition:true ,editableCellTemplate: 'ui-grid/dropdownEditor', editDropdownValueLabel: 'use_Type', editDropdownOptionsArray: [
                      { id: 1, use_Type: 'Residential' }, 
                      { id: 2, use_Type: 'Commercial' } ,
                      {id:3, use_Type:'PG/Hostel'}
                    ] },
                        
                   { name: 'phase_id' ,visible:false},         
                   { name: 'details',enableFiltering: false,visible:false,
                       cellTemplate:'<div class="ui-grid-cell-contents"><button class="btn btn-primary btn-xs btn-block" ng-click="grid.appScope.showEditDetails(row)">Edit</button></div>' 
                   }
                 ];
                 $scope.gridOptions.data=$scope.groundunits;
                 $scope.msg={};
                  
  }

 function addgrounds(grcount,fl){
         grcount=$scope.requiredgroundunitid;
         var tempgroundss=[];
        for(let j=1;j<=grcount;j++){
        var grounits= angular.copy(fl) ||{phase_id:$scope.tphase,carpet_area:090,builtup_area:0,superbuiltup_area:0,BHK:1,use_Type:1,use_type_category:1};
         tempgroundss.push(grounits);
         }
         addgroundtoDB(tempgroundss);
 }

    $scope.groundsources=[];
    $scope.groundsources = [{ phase_id:$scope.tphase,carpet_area:090,builtup_area:0,superbuiltup_area:0,BHK:1,use_Type:1,use_type_category:1}];
    $scope.copyGround = function (obj,mul) {
      addgrounds($scope.groundunit,obj);
    };

      function addgroundtoDB(groundus){
                  var allgroundus=[];
                  if(groundus.length>0){
                        angular.forEach(groundus,function(gu){
                                    var grunit=[];
                                    grunit.push(gu.phase_id);
                                    grunit.push(gu.carpet_area);
                                    grunit.push(gu.builtup_area);
                                    grunit.push(gu.superbuiltup_area);
                                    grunit.push(gu.BHK);
                                    grunit.push(parseInt(gu.use_Type));
                                    grunit.push(parseInt(gu.use_type_category));
                                    allgroundus.push(grunit);
                        });}
      var req={
                method: 'POST',
                url:  '/AddGroundUnits',
                headers: {
                      'Content-Type': 'application/json', data:angular.toJson(allgroundus)
              }};
              $http(req)
              .then(
                       function(d) {
                           $scope.GetGroundUnits();
                       },
                        function(errResponse){
                            console.error('Error while insertng GetGroundUnits');
                        }
               );
            };

    
  $scope.addwing = function(wing){
    $rootScope.fltcount=wing.flat_count_per_floor;
            wing.building_id=$routeParams.buildingid;       
                  var req={
                            method: 'POST',
                            url: '/AddWing',
                            headers: {
                                  'Content-Type': 'application/json', data:angular.toJson(wing)
                             }};
                                  
                              $http(req)
                                    .then(
                                             function(d) {
                                                  $scope.project_details={};
                                                /*  alert(angular.toJson(self.propertyform));*/
                                               /* $location.path('/flat_setup/'+d.data.insertId+"/"+wing.floor +"/"+wing.flat_count_per_floor+"/"+ wing.numbering_style||0); */
                                             },
                                              function(errResponse){
                                                  console.error('Error while insertng add project');
                                              }
                                     );
                                    //$location.path('/project_list');
                                     $scope.getwings();
            };
           

  
  if($routeParams.wingid && $routeParams.floor && $routeParams.flatcount){
          $scope.tfloor=$routeParams.floor;
          $scope.tflats=$routeParams.flatcount;
          $scope.twing=$routeParams.wingid;
          $scope.tnumStyle=$routeParams.numStyle;
         // $scope.getFlats($routeParams.wingid).then(function(da){
             if( $scope.FlatDetails && $scope.FlatDetails.data.length >0) {$scope.flats=$scope.FlatDetails;
             }else{
                   // addFlats($scope.tfloor,$scope.tflats);
             }
          //})

          
    $scope.gridOpt=   { data: $scope.data, columnDefs: $scope.columnDefs,enableRowSelection: true,
    enableSelectAll: true,
    enableFiltering: true, };

     $scope.data = [
     { name: 'Bob', title: 'CEO' },
     { name: 'Frank', title: 'Lowly Developer' }
   ];

   $scope.columnDefs = [
     {name: 'name', enableCellEdit: true},
     {name: 'title', enableCellEdit: true}
   ];

//////saving a grid row
$scope.saveRow = function( rowEntity ) {

  var req={
  method: 'POST',
  url:  '/UpdateFlat',
  headers: {
        'Content-Type': 'application/json', data:angular.toJson(rowEntity)
   }};
   $http(req);
    
    // create a fake promise - normally you'd use the promise returned by $http or $resource
    var promise = $q.defer();
    $scope.gridApi.rowEdit.setSavePromise( rowEntity, promise.promise );
 
    // fake a delay of 3 seconds whilst the save occurs, return error if gender is "male"
    $interval( function() {
      if (rowEntity.use_Type === 1 ){
        promise.reject();
      } else {
        promise.resolve();
      }
    }, 3000, 1);
  };

//////end of saving a grid row

                    $scope.gridOptions = {
                                    rowHeight: 40,
                                    enableCellEditOnFocus: true,
                                    enableFiltering: true,
                                    enableRowSelection: true,
                                    enableSelectAll: true,
                                    enableFiltering: true,                                  
                                    //enableGridMenu: false,                                   
                                    //exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                                  };
                               
            $scope.gridOptions.enableCellEditOnFocus = true;       
/*44*/                                        
            $scope.gridOptions.columnDefs = [
                   { name: 'floor', enableCellEdit: false },
                   { name: 'wing_id', enableCellEdit: false },
                   { name: 'number' },
                   { name: 'carpet_area',type:'number',enableCellEdit: true},
                   { name: 'builtup_area',enableCellEdit: true},
                   { name: 'superbuiltup_area',enableCellEdit: true},
                   { name: 'BHK'},
                   { name: 'use_Type',cellEditableCondition:true ,editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'useType', editDropdownValueLabel: 'use_Type', editDropdownOptionsArray: [
                      { id: 1, use_Type: 'Residential' }, 
                      { id: 2, use_Type: 'Commercial' } ,
                      {id:3, use_Type:'PG/Hostel'}
                    ] },
                        
                   { name: 'wing_id' ,visible:false},         
                   { name: 'details',enableFiltering: false,visible:false,
                       cellTemplate:'<div class="ui-grid-cell-contents"><button class="btn btn-primary btn-xs btn-block" ng-click="grid.appScope.showEditDetails(row)">Edit</button></div>' 
                   }
                 
                 ];   
                 $scope.gridOptions.data=$scope.flats;
                 $scope.msg={};
                  $scope.gridOptions.onRegisterApi = function(gridApi){
          //set gridApi on scope
                  $scope.gridApi = gridApi;
                  gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
                  gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
                    $scope.msg.lastCellEdited = 'edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue ;
                    $scope.$apply();
                  });
        };
         
  }



/*  .filter('useType', function() {
      var usetypeHash = {
        1: 'Residential',
        2: 'Commercial',
        2: 'PG/Hostel'

      };

      return function(input) {
        if (!input){
          return '';
        } else {
          return usetypeHash[input];
        }
      };
    })*/


 function addFlats(floor,flatcount,fl){
          floor= floor||$scope.tfloor;
         flatcount=flatcount||$scope.tflats;
         var tempflats=[];
        
          for(let i=1;i<=floor;i++){

            for(let j=1;j<=flatcount;j++){

            var flat= angular.copy(fl) ||{floor: i, wing_id:$scope.twing,carpet_area:090,builtup_area:0,superbuiltup_area:0,BHK:1,use_Type:1,use_type_category:1};
            
             (function(a,b){ flat.number= $scope.tnumStyle && $scope.tnumStyle ==1 ? ""+a+b : a+"0"+b;
                flat.floor=i;
              
           })(i,j);
               tempflats.push(flat);
            }
          }
          console.log("========================================");
          console.log(angular.toJson(tempflats));
          console.log("========================================");
          addflatstoDB(tempflats);
 }
              $scope.sources=[];
                $scope.sources = [{ wing_id:$scope.twing,carpet_area:090,builtup_area:0,superbuiltup_area:0,BHK:1,use_Type:1,use_type_category:1}];
                $scope.copyText = function (obj,mul) {
                   addFlats($scope.tfloor,$scope.tflats,obj);
                };
                 
       function addflatstoDB(flats){
                  var allflats=[];

                   if(flats.length>0){
                        angular.forEach(flats,function(f){
                                    var flat=[];
                                    flat.push(f.floor);
                                    flat.push(f.wing_id);
                                    flat.push(f.number);
                                    flat.push(f.carpet_area);
                                    flat.push(f.builtup_area);
                                    flat.push(f.superbuiltup_area);
                                    flat.push(f.BHK);
                                    flat.push(parseInt(f.use_Type));
                                    flat.push(parseInt(f.use_type_category));
                                    allflats.push(flat);
                        });

                  }

                  var req={
                            method: 'POST',
                            url:  '/AddFlats',
                            headers: {
                                  'Content-Type': 'application/json', data:angular.toJson(allflats)
                             }};
                                  
                              $http(req)
                                    .then(
                                             function(d) {
                                                 $scope.getFlats($scope.twing);
                                             },
                                              function(errResponse){
                                                  console.error('Error while insertng add flats');
                                              }
                                     );
                                    //$location.path('/project_list');
                  
            };


              $scope.showpropertydrp = function(){
                $scope.allprojects=window.localStorage.samplepro;
                ngDialog.open({template: 'app/project/agreement_property.html',appendClassName: 'ngdialog-theme-default1',controller:'projectController',scope: $scope});
             };
               $scope.$on("closengDdialog",function(){
                   ngDialog.close();
                 });

              $scope.pr=[];
               $scope.changepro=function(value){
                $scope.pr=value.project_name;
                var req={
                  method: 'GET',
                  url:  '/GetPhases',
                  headers: {
                        'Content-Type': 'application/json', data:angular.toJson(value.ID)
                   }};
                   $http(req)
                          .then(
                                   function(d) {
                                      $scope.allphases=[];
                                      $scope.allphases=d.data;
                                   },
                                    function(errResponse){
                                        console.error('Error while insertng add project');
                                    });
                          }

               $scope.ph=[];
               $scope.changephase=function(value){
               $scope.ph =value.name;
                var req={
                  method: 'GET',
                  url:  '/GetBuildings',
                  headers: {
                        'Content-Type': 'application/json', data:angular.toJson(value.id)
                   }};
                   $http(req)
                          .then(function(d) {
                                    $scope.allbuilding=d.data;
                                   },
                                    function(errResponse){
                                        console.error('Error while insertng add project');
                                    });
                            }

               $scope.b=[];
               $scope.changebuilding=function(value){
                $scope.b=value.name;
                var req={
                  method: 'GET',
                  url:  '/GetWings',
                  headers: {
                        'Content-Type': 'application/json', data:angular.toJson(value.id)
                   }};
                   $http(req)
                          .then(
                                   function(d) {
                                    $scope.allwing=d.data;
                                   },
                                    function(errResponse){
                                    console.error('Error while insertng add project');
                                    });
                        }
               

               $scope.w=[];
               $scope.changewing=function(value){
                 $scope.w=value.name;
                  var req={
                  method: 'GET',
                  url:  '/GetFlats',
                  headers: {
                        'Content-Type': 'application/json', data:angular.toJson(value.id)
                   }};
                   $http(req)
                          .then(function(d) {
                                    $scope.allflat=d.data;
                                   },
                                    function(errResponse){
                                    console.error('Error while insertng add project');
                                    });
                        }

               $scope.f=[];
               $scope.changeflats=function(value){
                 $scope.f=value.number;
               }

               $scope.getalldt=function()
               {
                 $scope.ftwgphbgpro = $scope.f + " " + $scope.w + " "+ $scope.b + " "+ $scope.ph +" "+ $scope.pr;
                 window.localStorage.dt=$scope.ftwgphbgpro;
                 alert("data saved to select property please refresh the page")
               }
               $scope.datap=window.localStorage.dt;
               
               
              self.projectDetail = function(){ 
                projectService.fetchProjectdetail(projectid).then(function(res){
                self.ProjectDetail = res[0];
                });};
 
                $scope.startdate = moment();
                $scope.setagreementenddt=function(a,b){
                  $scope.enddate = moment(a).add(b,'M').subtract(1,'days').format("DD-MM-YYYY");
                  }

              /*$scope.showselecttenant = function(){
                ngDialog.open({template: 'app/project/select_tenant.html',appendClassName: 'ngdialog-theme-default1',controller:'projectController',scope: $scope});
             };*/

             /*$scope.inputs = [];
             $scope.tenant=[];
              $scope.addfield=function(ten){
                $scope.tenant.push(ten);
                $scope.inputs.push({});
              }*/

              $scope.tenant=[];
                $scope.addTenant=function()
                {
                    this.tenant.push({tenantnm:$('#ten-opt option:selected').val()});
                }
                $scope.del=function(ind)
                {
                    this.tenant=this.tenant.splice(ind,1);
                }
                
            //agreement form
            $scope.showHideTest = false;
            //self.projectDetail();
            self.ProjectDetail={ID :projectid };
           self.addProjectUpdata = function(){ 
            projectService.addProjectUpdata(self.ProjectDetail)
                      .then(
                                   function(d) {

                                         d;
                                         $scope.ProjectDetail={};
                                         
                                         $scope.ProjectDetail={ID :projectid };
                                        $location.path('project_list/:all');

                                   },
                                    function(errResponse){
                                      console.error(errResponse);
                                      console.error('Error while fetching Projects'+(angular.toJson(errResponse)));
                                    }

                           );

                        
              };



}]);


app.controller('showpropertyController',['$scope','$http','propertyService','$routeParams',function($scope,$http,propertyService,$routeParams,server_url){
  

          var self = this;
        $scope.properties='';
          /*self.property=[];*/
               
          self.fetchAllProperty = function(){
              propertyService.fetchAllProperty()
                  .then(
                               function(d) {
                                 $scope.properties=d;
                                    self.property = d;
                                    /* alert(angular.toJson( $scope.properties));*/
                                     $scope.getArray= $scope.properties;
                               },
                                function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching property');
                                }
                       );
          };
          self.fetchAllProperty();

          
          var propertyid =  $routeParams.id;
          var self = this;
          self.PropertyDetail=[];
         
 
          self.propertyDetail = function(){ 
           /* alert("controller propertyDetail" +propertyid);*/
             propertyService.fetchPropertydetail(propertyid).then(function(res){
              self.PropertyDetail =res[0];
            /*  alert(angular.toJson(self.PropertyDetail));*/
          
              });
         
          };
          self.propertyDetail();


           self.ownertenantDetail = function(){ 
           /* alert("controller propertyDetail" +propertyid);*/
             propertyService.fetchOwnerTenantdetail(propertyid)
                  .then(
                          function(d){
                            $scope.TenantDetail=d;
                          /*  alert(angular.toJson($scope.TenantDetail));*/
                          },
                           function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching owner');
                          }
                      );
         
          };
          self.ownertenantDetail();


}]);

/*88*/
app.controller('tenantController',['$scope','propertyService','$location','$routeParams','Upload','$compile',function($scope,propertyService,$location,$routeParams,server_url,Upload,$compile){
  

           $scope.myVar = true;
           $scope.myPar =  false;
           $scope.toggle = function() {
              $scope.myVar = !$scope.myVar;
              $scope.myPar = !$scope.myPar;
           };

          var newTenant =  $routeParams.newTenant; 
          var tenantid =  $routeParams.id;
          var self = this;
         /*alert(angular.toJson(self.tenantid));*/
         /* self.PropertyDetail=[];*/
 
          self.tenantDetail = function(){ 
           /* alert("controller tenant Detail " +tenantid);*/
             propertyService.fetchTenantdetail(tenantid).then(function(res){
              self.TenantDetail =res[0];
              /*alert(angular.toJson(self.TenantDetail));*/
          
              });
         
          };
          self.tenantDetail();
          /*updat*/

    
       self.TenantDetail={ID :tenantid };

       self.addTenantUpdata = function(){ 
       /*alert("addTenantUpdata controller");*/
       self.TenantDetail.birth_date =moment(self.TenantDetail.birth_date).format('L');
       self.TenantDetail.tenant_anni_date =moment(self.TenantDetail.tenant_anni_date).format('L');
      /* alert(angular.toJson(self.TenantDetail));*/
       propertyService.addTenantUpdata(self.TenantDetail)
                  .then(
                               function(d) {

                                     self.TenantDetail= d;
                                     /*self.TenantDetail={};*/
                                     self.TenantDetail={ID :tenantid };
                                   /*  alert("hello");
                                     alert(angular.toJson(self.TenantDetail));*/
                                    $location.path('/tenant');

                               },
                                function(errResponse){
                                  console.error(errResponse);
                                  console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                }

                       );

                    
          };
          /*end*/        
             self.tenentlist = function(){ 
           /* alert("controller propertyDetail" +propertyid);*/
             propertyService.TenantList()
                  .then(
                          function(d){
                            $scope.Tenantlist=d;
                     /*       alert(angular.toJson($scope.TenantDetail));*/
                          },
                           function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching owner');
                          }
                      );
         
          };
          self.tenentlist();

          $scope.onChange = function (files) {
          if(files[0] == undefined) return;
          $scope.fileExt = files[0].name.split(".").pop()
        }
        
        $scope.isImage = function(ext) {
          if(ext) {
            return ext == "jpg" || ext == "jpeg"|| ext == "gif" || ext=="png"
          }
        }

}]);



app.controller('leadController',['$scope','$http','$location','$filter','StaffService',function($scope,$http,$location,$filter,StaffService,server_url){
	
            self=this;
            $scope.leadform={
            followupdate:moment(new Date()).format("DD/MM/YYYY"),
            followuptime:moment(new Date())
            };
            var today=new Date().setHours(0,0,0,0);
            $scope.today = new Date(today);
            $scope.cdate = new Date();
           // $scope.leadform.followuptime =  $filter('date')(new Date(), 'hh:mm a');
            $scope.followupdate = moment(new Date()).format("DD/MM/YYYY") ;//$filter("date")(Date.now(), 'yyyy-MM-dd');
            /*var x = new Date();
            $scope.leadform.followupdate=x.getDate() + "/" + (x.getMonth()+1) + "/" + x.getFullYear();*/

            /*var y = new Date();
            $scope.leadform.followuptime=y.getHours( )+ ":" +  y.getMinutes(); */

            self.allstaff=function (){
                          StaffService.fetchAllStaff()   
                           .then(
                                           function(d) {
                                                $scope.allstaff = d;
                                                //alert(angular.toJson(self.allStaff));
                                                 
                                           },
                                            function(errResponse){
                                                console.error('Error while fetching Currencies');
                                            }
                                   );
                                }
            self.allstaff();
        $scope.Addlead = function(){
             /* alert(angular.toJson($scope.leadform));*/
            	var tempdate=$scope.leadform.req_start_date;
               $scope.leadform.followuptime = moment($scope.leadform.followuptime).locale('en').format("hh:mm A");

             var req={
             	method: 'POST',
             	url:  '/InsertLead',
             	headers: {
              		  'Content-Type': 'application/json', data:angular.toJson($scope.leadform)
               }};
             				
             		$http(req).then(
                                function(success){
                                     $scope.leadform={};
                                 },
                                 function(error){}
                            );
                	$location.path('/showallleads');
                	
            };


}]);


/*app.controller('editleadController',['$scope','$routeParams','LeadService','$http','$location','StaffService', '$rootScope' ,function($scope,$routeParams,LeadService,$http,$location,StaffService,$rootScope){  

self=this;
$scope.editleadform={};
var today=new Date().setHours(0,0,0,0);
$scope.today = new Date(today);
var leadId =  $rootScope.leadEditId;
self.leadDetails=LeadService.fetchLeadSpecificData(leadId);

self.leadDetails.req_start_date= !self.leadDetails.req_start_date? (new Date()).toString() :self.leadDetails.req_start_date;

self.leadDetails.req_start_date=moment(self.leadDetails.req_start_date,'DD/MM/YYYY').format("DD/MM/YYYY")
self.leadDetails.req_start_date= self.leadDetails.req_start_date=="Invalid date"? moment(new Date()).format("DD/MM/YYYY") :self.leadDetails.req_start_date;



$scope.Addlead = function(){
  var tempdate=$scope.editleadform.req_start_date;

      var req={
  method: 'POST',
  url:  '/InsertLead',
  headers: {
        'Content-Type': 'application/json', data:angular.toJson(self.leadDetails)
   }};
        
    $http(req);
    $location.path('/showallleads');
      
};
$scope.Editlead = function(alldata){
  delete(alldata.leadDetails.date);
 // delete(alldata.leadDetails.req_start_date);
  delete(alldata.leadDetails.next_followup_date);
  delete(alldata.leadDetails.next_followup_time);
  console.log(angular.toJson(alldata.leadDetails));
  var req={
  method: 'POST',
  url:  '/UpdateLeadData',
  headers: {
        'Content-Type': 'application/json', data:angular.toJson(alldata.leadDetails)
   }};
        
    $http(req).then(function(){alert("Lead edited Successfully");
                      $rootScope.$broadcast("closengDdialog","");
                      $location.path('/showallleads');
                      });
    
      
};


}]);*/
app.controller('showleadController',['$scope','$location','LeadService','FileSaver','uiGridConstants','ngDialog', '$rootScope', '$timeout' ,function($scope,$location,LeadService,FileSaver,uiGridConstants,ngDialog, $rootScope, $timeout){
	//$scope.$scope = $scope;

          var self = this;
          self.lead={visitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.leads=[];
          $scope.someProp = 'abc',
            $scope.showEditDetails = function(id){
                    $timeout(function()
                    {
                       $rootScope.leadEditId=id.entity.ID;
                         ngDialog.open({template: 'app/lead/edit_lead.html', controller:'editleadController as editleadctrl', scope: $scope});

                    },100);
                 };
       
                 $scope.$on("closengDdialog",function(){
                   self.fetchAllLeads();
                   ngDialog.close();
                 });

                $scope.showDetails = function(id){

                  // alert(angular.toJson(id.entity));
                   /*$location.path('/viewlead/lead/'+ id.entity.ID);*/

                 
                   $location.path('/viewlead/lead/'+ id.entity.ID);

                };

                 $scope.getTableHeight = function() {
                       var rowHeight = 40; // your row height
                       var headerHeight = 40; // your header height
                       return {
                          height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
                       };
                    };
                    $scope.gridOptions = {
                    rowHeight: 40,
						      	enableFiltering: false,
						    		onRegisterApi: function(gridApi){
						      		$scope.gridApi = gridApi;
                      // $scope.gridOptions.enableFullRowSelection =true;
                      gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    /*$location.path('/editlead/'+ row.entity.ID);*/
                    /*$location.path('/viewlead/lead/'+ row.entity.ID);*/
                        $rootScope.leadId=row.entity.ID;
                         $rootScope.source="lead";
                        //$scope.T_Url = app/lead/view_lead.html;
                          ngDialog.open({template: 'app/lead/view_lead.html', controller:'leadFollowupcontroller as followup', scope: $scope});


                         });
                        },
               				enableGridMenu: true,
    							    enableSelectAll: true,
    							    exporterCsvFilename: 'myFile.csv',
    							    exporterPdfDefaultStyle: {fontSize: 9},
    							    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
    							    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
    							    exporterPdfHeader: { text: "Ajinka Real State ", style: 'headerStyle' },
    							    exporterPdfFooter: function ( currentPage, pageCount ) {
    							      return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
    							    },
    							    exporterPdfCustomFormatter: function ( docDefinition ) {
    							      docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
    							      docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
    							      return docDefinition;
    							    },
    							    exporterPdfOrientation: 'portrait',
    							    exporterPdfPageSize: 'LETTER',
    							    exporterPdfMaxGridWidth: 500,
    							    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
    							     

           					}
                     
          
                              
            $scope.gridOptions.columnDefs = [
         { name: 'person_name' },
         { name: 'contact_number'},
         { name: 'lead_type'},
         { name: 'max_rent'},
         { name: 'max_deposit'},
         { name: 'location'},
         { name: 'property_type'},
         { name: 'property_type_detail'},
         { name: 'reference',visible:false},
         { name: 'remarks',visible:false},
         { name: 'status',visible:false},
         { name: 'date' ,visible:false},
         
         {name: 'email',visible:false},
         {name: 'req_start_date',visible:false},
         {name: 'location',visible:false},
         {name: 'property_type_Detail',visible:false},
         {name: 'min_deposit',visible:false},
         {name: 'min_rent',visible:false},
         {name: 'landline_no',visible:false},
         {name: 'assign_staff_id',visible:false},
         {name: 'furnished_type',visible:false},
         {name: 'folowup_remarks',visible:false},
         
      /*   { name: 'details',enableFiltering: false,
             cellTemplate:'<div class="ui-grid-cell-contents" ><button class="btn btn-primary btn-xs btn-block" ng-click="grid.appScope.showEditDetails(row)">Edit</button></div>' 
         }
*/
         /*,
         { name: 'delete',cellTemplate:'<div class="ui-grid-cell-contents ng-binding ng-scope"><button class="btn btn-danger {{getExternalScopes().deleteButtonClass(row)}} btn-xs btn-block" ng-click="getExternalScopes().delete($event, row)"><span class="glyphicon glyphicon-trash"></span></button></div>'}
*/
       ];     
          self.fetchAllLeads = function(){
              LeadService.fetchAllLeads()
                  .then(
                               function(d) {
                                    self.leads = d;
                                     $scope.gridOptions.data=d;
                                    
                               },
                                function(errResponse){
                                	console.log(errResponse);
                                    console.error('Error while fetching leads');
                                }
                       );
          };
          self.fetchAllLeads();

           $scope.toggleFiltering = function(){
    $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
    $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
  };

 
 
 
  $scope.download = function() {
  	
  	 var t = JSON.stringify( $scope.leads);
    var data = new Blob(t, { type: 'text/html;charset=utf-8' });
    FileSaver.saveAs(data, 'text.txt');
  };
}]);

app.controller('branchController',['$scope','$http','$location',function($scope,$http,$location,server_url){

  $http.get('js/state.json').success(function(response){
      $scope.myData = response;
    });
	
$scope.branchForm={};

$scope.addBranch = function(){
	
 //	alert(angular.toJson($scope.branchForm));
    	var req={
 	method: 'POST',
 	url: '/InsertBranch',
 	headers: { 'Content-Type': 'application/json', data:angular.toJson($scope.branchForm)
   }};
 				
 		$http(req).then(
                               function(d) {
                                    $scope.branchForm={};
                               },
                                function(errResponse){
                                    console.error('Error while insertng branch');
                                }
                       );
    		$location.path('/branch');    	
}
}]);


app.controller('showBranchController',['$scope','BranchService',function($scope,BranchService){
	

          var self = this;
         // self.branch={visitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.branches=[];
               
          self.fetchAllBranches = function(){
              BranchService.fetchAllBranches()
                  .then(
                               function(d) {
                                    self.branches = d;
                                    var b = angular.copy(d);
                                    for (var i = b.length - 1; i >= 0; i--) {
                                    delete b[i].ID;
                                    };  
                                    //alert(angular.toJson(b));
                                    $scope.getArray= b;
                               },
                                function(errResponse){
                                	console.log(errResponse);
                                    console.error('Error while fetching branches');
                                }
                       );
          };
          self.fetchAllBranches();
           $scope.getHeader = function () {return ["Branch Name","Contact No","Email","Branch Address","City","state","Zipcode","Bank Name","Bank Account No","Bank ifsc Code","Description","landline No"]};
}]);

app.controller('branchDetailcontroller',['$scope','$routeParams','BranchService',function($scope,$routeParams,BranchService){
  
     var branchId =  $routeParams.branchId;
    
          var self = this;
          
          
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allStaff=[];
         
               
          self.fetchAllStaff = function(branchId){
              BranchService.fetchAllStaff(branchId)
               .then(
                               function(d) {
                                    self.allStaff = d;
                                    
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };

        
           self.branchDetails=BranchService.fetchBranchSpecificData(branchId);
           
          self.fetchAllStaff(branchId);
         
            
}]);

app.controller('staffController',['$scope','$http','$location',function($scope,$http,$location,server_url){
  
$scope.staffForm={};
$scope.value =0;


/*============================*/
/*$scope.single = function(image) {
                    var formData = new FormData();
                    formData.append('image', image, image.name);
                    $http.post('upload', formData, {
                        headers: { 'Content-Type': false },
                        transformRequest: angular.identity
                    }).success(function(result) {
                        $scope.uploadedImgSrc = result.src;
                        $scope.sizeInBytes = result.size;
                    });
                };*/
/*===============================*/



$scope.addStaff = function(){ 
  /*  delete $scope.staffForm.user_id;
    delete $scope.staffForm.pass;*/
   /*alert(angular.toJson($scope.staffForm));*/

  var req={
  method: 'POST',
  url: '/InsertStaffMember',
  headers: {
        'Content-Type': 'application/json', data:angular.toJson($scope.staffForm)
   }};
        
    $http(req).then(
                               function(d) {
                                    $scope.staffForm={};
                                    console.log(angular.toJson(d));
                                    $location.path('/setcredentials/'+d.data.insertId);
                               },
                                function(errResponse){
                                    console.error('Error while insertng staff');
                                }
                       );
}

}]);


//credentialsController
app.controller('credentialsController',['$scope','$location','AuthenticationService','$routeParams',function($scope,$location,AuthenticationService,$routeParams){
//var id=$routeParams.id;
$scope.staffinfo={};
$scope.saveCredentials = function(){ 
  
  
            AuthenticationService.SaveCredentials( $scope.staffinfo.username, $scope.staffinfo.password, $routeParams.id).then( function (response) {
                
                    $location.path('/feature/'+response.insertId);
                
            },function(){})
        };
/*=================================*/
 $scope.passwordLength = 8;
    $scope.addUpper       = true;
    $scope.addNumbers     = true;
    $scope.addSymbols        = true;

    $scope.createPassword = function(){
        var lowerCharacters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        var upperCharacters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        var numbers = ['0','1','2','3','4','5','6','7','8','9'];
        var symbols = ['#', '$', '%', '&', '*', '@'];
        var finalCharacters = lowerCharacters;
        if($scope.addUpper){
            finalCharacters = finalCharacters.concat(upperCharacters);
        }
        if($scope.addNumbers){
            finalCharacters = finalCharacters.concat(numbers);
        }
        if($scope.addSymbols){
            finalCharacters = finalCharacters.concat(symbols);
        }
        var passwordArray = [];
        for (var i = 1; i < $scope.passwordLength; i++) {
            passwordArray.push(finalCharacters[Math.floor(Math.random() * finalCharacters.length)]);
        };
        $scope.staffinfo.password = passwordArray.join("");
    };

/*==================================*/



}]);

app.controller('showStaffcontroller',['$scope','StaffService',function($scope,StaffService){
	

          var self = this;
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allStaff=[];

               
          self.fetchAllStaff = function(){
              StaffService.fetchAllStaff()
                  .then(
                               function(d) {
                                    self.allStaff = d;
                                    //alert(angular.toJson(self.allStaff));
                                    var s = angular.copy(d);
                                    for (var i = s.length - 1; i >= 0; i--) {
                                                                       
                                    delete s[i].ID;
                                    delete s[i].image_path;
                                    delete s[i].comment;
                                    };
                                    //alert(angular.toJson(s));
                                    $scope.getArray= s;
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
          self.fetchAllStaff();
          $scope.getHeader = function () {return ["Staff Name","Email Id","Mobile No","Local Address","City","State","Zip Code","Residential Address","Landline No","address Proof","Id Proof","assigned_branch","assigned_designation"]};
}]);

/*vl*/
app.controller('leadFollowupcontroller',['$scope','$rootScope','$routeParams','LeadService','ownerService','$filter','$http','$location', '$timeout','amMoment' ,function($scope,$rootScope,$routeParams,LeadService,ownerService,$filter,$http,$location,$timeout,amMoment){
	     

      /*==============*/
      $scope.tab = 1;
      $scope.setTab = function(newTab){
      $scope.tab = newTab;
      };
      $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
      };
      /*==============*/




      /*toggle*/
        $scope.myVar = true;
        $scope.myPar =  false;
        $scope.toggle = function() {
            $scope.myVar = !$scope.myVar;
            $scope.myPar = !$scope.myPar;
        };     

      /*toggle end*/


      $scope.myremark= false;
       $scope.remks = function() {
        $scope.myremark = !$scope.myremark;
       };




      var today=new Date().setHours(0,0,0,0);
      $scope.today = new Date(today);
		  var leadId =  $rootScope.leadId;
      var home =  $rootScope.home;
      $scope.next_followup_date = $filter("date")(Date.now(), 'yyyy-MM-dd');
      $scope.isCollapsed=true;

      var self = this;
      $scope.converttoclient = function(leadDetails){
         
            if (leadDetails.lead_type == "owner"){
             // alert("owner click");
              delete(leadDetails.ID);
              delete(leadDetails.reference);
              delete(leadDetails.remarks);
              delete(leadDetails.lead_type);
              delete(leadDetails.status);
              delete(leadDetails.date);
              delete(leadDetails.priority);
              delete(leadDetails.req_start_date);
              delete(leadDetails.location);
              delete(leadDetails.property_type);
              delete(leadDetails.property_type_detail);
              delete(leadDetails.max_rent);
              delete(leadDetails.max_deposit);
              delete(leadDetails.min_deposit);
              delete(leadDetails.min_rent);
              delete(leadDetails.assign_staff_id);
              delete(leadDetails.details);
              delete(leadDetails.next_followup_date);
              delete(leadDetails.next_followup_time);
              delete(leadDetails.furnished_type);
      
                  var req={
                        method: 'POST',
                        url: '/InsertOwner',
                        headers: {
                              'Content-Type': 'application/json', data:angular.toJson(leadDetails)
                         }};
             
                      $http(req).then(function(success){
                         /*alert(angular.toJson(success));*/
                      /*alert("Are u sure u want to convert it to client");*/
                      alert("Lead Convert To client Successfully");
                      $rootScope.$broadcast("closengDdialog","");
                      $location.path('/ownerdetails/'+success.data.insertId+"/ownerdetail"); 
                      },function(error){alert();});
                       
             } 
             else if (leadDetails.lead_type == "tenant"){
            //  alert("tenent click");
                  delete(leadDetails.ID);
                  delete(leadDetails.reference);
                  delete(leadDetails.remarks);
                  delete(leadDetails.lead_type);
                  delete(leadDetails.status);
                  delete(leadDetails.date);
                  delete(leadDetails.priority);
                  delete(leadDetails.req_start_date);
                  delete(leadDetails.location);
                  delete(leadDetails.property_type);
                  delete(leadDetails.property_type_detail);
                  delete(leadDetails.max_rent);
                  delete(leadDetails.max_deposit);
                  delete(leadDetails.min_deposit);
                  delete(leadDetails.min_rent);
                  delete(leadDetails.assign_staff_id);
                  delete(leadDetails.details);
                  delete(leadDetails.next_followup_date);
                  delete(leadDetails.next_followup_time);
                  delete(leadDetails.furnished_type);
      
                    var req={
                        method: 'POST',
                        url: '/InsertTenant',
                        headers: {
                              'Content-Type': 'application/json', data:angular.toJson(leadDetails)
                         }};
             
                      $http(req).then(function(success){
                        alert("Lead Convert To Tenant Successfully");
                        $rootScope.$broadcast("closengDdialog","");
                        $location.path('/tenant_details/'+success.data.insertId+"/tenantdetail"); 

                    },function(error){alert();});

                  }
          }
          
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allFollowups=[];
          //self.allFollowups=test;
          self.newFollowup={lead_id :leadId };
         /*  self.newFollowup.next_followup_date= moment();*/

         /* var y = new Date();
          self.newFollowup.next_followup_time=y.getHours( )+ ":" +  y.getMinutes();*/

            //$scope.a=10;   
          self.fetchAllFollowups = function(leadId){
               LeadService.fetchAllFollowups(leadId)
                  .then(
                               function(d) {
                                    self.allFollowups = d;
                                   /* alert(angular.toJson("fetchAllFollowups c"+self.allFollowups));*/
                                    $timeout(function(){
                                      $scope.$apply(function(){self.allFollowups = d;});},0);
                                    
                                    var p = self.allFollowups
                                    for (var i = p.length - 1; i >= 0; i--) {
                                    delete p[i].ID;
                                    delete p[i].staff_id;
                                    delete p[i].lead_id;
                                    };
                                    //alert(angular.toJson(p));
                                    $scope.getArray= p;
                                    //alert(angular.toJson(self.allFollowups));
                               },
                                function(errResponse){
                                    console.error('Error while fetching follows');
                                }
                       );
          };
      
           $scope.updateleads= function(data){
         
            var id=  $routeParams.leadId || $rootScope.leadId;
            var status= data;
            var alldata=[];
            alldata.push(status);
            alldata.push(id);
            
           alert(angular.toJson(alldata));
            var req={
            method: 'POST',
            url: '/updatelead',
            headers: {
                  'Content-Type': 'application/json', data:angular.toJson(alldata)
             }};
             $http(req).then(
                                function(d) {
                                  //if($scope.dialog){ $scope.dialog.close();}
                                  $rootScope.$broadcast("closengDdialog","");
                                  
                                  }, function(errResponse){
                                      console.error('Error while insertng updating');
                                                      }
                                             );
                            // $location.path('/showallleads'); 

          };

          
           self.alltask=[];
           self.fetchAlltask = function(leadId){
          /*  alert(" fatchalltask c "+leadId);*/
              LeadService.fetchAlltask(leadId)
                  .then(
                               function(d) {
                                    self.alltask = d;

                                  /*  alert(angular.toJson(self.alltask));
*/                                  
                               },
                                function(errResponse){
                                    console.error('Error while fetching get task');
                                }
                       );
          };
          self.fetchAlltask(leadId);

          

          self.addtask =function(){
            self.task.lead_id =  leadId;
            amTimezone:'Asia/Kolkata';
            amDateFormat:'MM.DD.YYYY';
           self.task.schdule_time=moment(self.task.schdule_time.toString()).format("HH:mm A");
           self.task.reminder_before_task=moment(self.task.reminder_before_task.toString()).format("HH:mm A");
           LeadService.addtask(self.task)
                  .then(
                               function(d) {

                                     d;
                                     self.task={};
                                  
                               },
                                function(errResponse){
                                  console.error(errResponse);
                                    console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                }

                       );

          };

          self.addactivity = function(){
           self.activity.lead_id =  leadId;
                LeadService.addactivity(self.activity)
                  .then(
                               function(d) {

                                     d;
                                     self.activity={};
                                   
                               },
                                function(errResponse){
                                  console.error(errResponse);
                                    console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                }

                       );

          };
        
            self.Stafflist = function(){
              LeadService.listStaff()
                  .then(
                               function(d) {
                                    self.stafflist = d;
                                    /*alert(angular.toJson(self.stafflist));*/
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
          self.Stafflist();
        
         
         self.addFollowup = function(){ 
       
         amTimezone:'Asia/Kolkata';
         amDateFormat:'MM.DD.YYYY';
         /*moment.tz.setDefault("Asia/Kolkata");*/
         self.newFollowup.next_followup_date=moment(self.newFollowup.next_followup_date).format('L');//.format('LL');
         self.newFollowup.next_followup_time=moment(self.newFollowup.next_followup_time.toString()).format("HH:mm A");

		   //self.newFollowup.next_followup_date =new Date(tempdate.getFullYear() + "-" + ( tempdate.getMonth() >9 ? (tempdate.getMonth()+1) : "0" + (tempdate.getMonth() +1) )+ "-"  + (tempdate.getDate() >=10 ? tempdate.getDate() : "0" + tempdate.getDate()));
      /*alert(tempdate);
      self.newFollowup.next_followup_date=tempdate;*/
     // alert(self.newFollowup.next_followup_date);
     // alert(angular.toJson($scope.followup.newFollowup));
        LeadService.addFollowup(self.newFollowup)
                  .then(
                               function(d) {

                                     d;
                                     self.newFollowup={};
                                     self.fetchAllFollowups(leadId);
                                     self.newFollowup={lead_id :leadId };
                               },
                                function(errResponse){
                                  console.error(errResponse);
                                    console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                }

                       );
          };
           
          
           if($rootScope.source=="current")
           {     
              //self.leadDetails=$rootScope.followuplead[0];
              self.leadDetails=LeadService.fetchLeadSpecificData(leadId, $rootScope.reminderes);
              //alert(angular.toJson(self.leadDetails)); 

           }
           else if($rootScope.source=="pending")
            {  
              //self.leadDetails=$rootScope.pendingleads[0];
              //alert(angular.toJson($rootScope.pendingleads)); 
              self.leadDetails=LeadService.fetchLeadSpecificData(leadId,$rootScope.pendingleads);
               //alert(angular.toJson(self.leadDetails)); 

           }
           else{
            self.leadDetails=LeadService.fetchLeadSpecificData(leadId,angular.fromJson(sessionStorage.GetAllLeads));
           // alert("my lead "+ leadId+ $rootScope.source + []);
            }

           
             
            self.viewleadupdate = function(){ 
              delete(self.leadDetails.date);
              delete(self.leadDetails.next_followup_date);
              delete(self.leadDetails.next_followup_time);
            /*  alert(angular.toJson(self.leadDetails));*/
               LeadService.viewleadupdate(self.leadDetails)
                          .then(
                                       function(d) {

                                             d;
                                             self.leadDetails={};
                                             self.leadDetails={ID :leadId };
                                             /*alert("hello");
                                             alert(angular.toJson(self.leadDetails));*/
                                              $rootScope.$broadcast("closengDdialog","");
                                              $location.path('/showallleads');
                                           /* $location.path('/owner');*/

                                       },
                                        function(errResponse){
                                          console.error(errResponse);
                                          console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                        }

                               );

                            
                  };
                       self.fetchAllFollowups(leadId);
          $scope.getHeader = function () {return ["Remark","Date","Next Followup Date"]};
       
          

}]);

/*123*/
app.controller('ownerdetailsController',['$scope','$location','$routeParams','ownerService',function($scope,$location,$routeParams,ownerService,server_url){
 
          var newOwner =  $routeParams.newOwner;
          var ownerId =  $routeParams.id;
          $scope.myVar = true;
          $scope.myPar =  false;
          $scope.toggle = function() {
              $scope.myVar = !$scope.myVar;
              $scope.myPar = !$scope.myPar;
          };
          
          var self = this;
          self.OwnerDetail=[];
          self.localODetails=[];

          self.ownerDetail = function(){ 
           ownerService.fetchOwnerdetailData(ownerId).then(function(res){
              self.OwnerDetail =res[0];
          
              });
         
          };
 
       self.ownerDetail();
       self.owneres=[]; 
       self.OwnerDetail={ID :ownerId };
       self.addOwnerUpdata = function(){ 
       self.OwnerDetail.birth_date =moment(self.OwnerDetail.birth_date).format('L');
       self.OwnerDetail.owner_anni_date =moment(self.OwnerDetail.owner_anni_date).format('L');
       ownerService.addOwnerUpdata(self.OwnerDetail)
                  .then(
                               function(d) {

                                     d;
                                     self.OwnerDetail={};
                                     self.OwnerDetail={ID :ownerId };
                                    /* alert("hello");
                                     alert(angular.toJson(self.OwnerDetail));*/
                                    $location.path('/owner');

                               },
                                function(errResponse){
                                  console.error(errResponse);
                                  console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                }

                       );

                    
          };
          
           self.fetchOwnerProperty = function(){
            /*alert(angular.toJson("controller "+ownerId));*/
                ownerService.fetchOwnerProperty(ownerId)
                  .then(
                               function(d) {
                                 $scope.OwnerProperty=d;
                                   /*alert(angular.toJson( $scope.OwnerProperty));*/
                                    
                               },
                                function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching owner');
                                }
                       );
          };
          self.fetchOwnerProperty();

          
  }]);

app.controller('showownerController',['$scope','ownerService',function($scope,ownerService,server_url){
  

          var self = this;
        $scope.owneres='';
          /*self.property=[];*/
               
          self.fetchAllOwner = function(){
              ownerService.fetchAllOwner()
                  .then(
                               function(d) {
                                 $scope.owneres=d;
                                    var b = angular.copy(d);
                                    for (var i = b.length - 1; i >= 0; i--) {
                                    delete b[i].ID;
                                    }; 
                                     $scope.getArray= b; 
                                     /*alert(angular.toJson( $scope.owneres));*/
                                     /*$scope.getArray= $scope.owneres;*/
                               },
                                function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching owner');
                                }
                       );
          };
          self.fetchAllOwner();
           $scope.getHeader = function () {return ["Owner Name","Email Id","Mobile No","Landline No"]};

}]);


app.controller('homeController',['$rootScope','$scope', '$http', '$q', '$filter', '$timeout', '$location', 'ngDialog' ,function($rootScope,$scope, $http, $q, $filter, $timeout, $location,ngDialog)
  {
$timeout(function(){
  $('.table').trigger('footable_redraw');
}, 100);
  
    GetAllFollowupreminder = function(leadId) {
                     $http.get('/GetAllFollowupreminder')
                            .then(
                                    function(response){
                                        $rootScope.source= "current";
                                        $rootScope.reminderes = response.data;
                                       /* alert(angular.toJson("GetAllFollowupreminder"+ $rootScope.reminder));*/
                                       /* alert(angular.toJson( $rootScope.reminder));*/
                                        var test= new Date();
                                      
                                    },  
                                    function(errResponse){
                                        console.error('Error while fetching reminder');
                                        return $q.reject(errResponse);
                                    }
                                  );
            }
            GetAllFollowupreminder();

             $scope.showReminder = function(id,showreminder){
                  
                   
                   $rootScope.leadId=id;
                   $rootScope.home="home";
                   $rootScope.source = showreminder;
                  // alert(angular.toJson(   $rootScope.source));
                    
                  $scope.dialog= ngDialog.open({template: 'app/lead/view_lead.html', controller:'leadFollowupcontroller as followup', scope: $scope});
                };
             $scope.$on("closengDdialog",function(){
                                
                               ngDialog.close();

                             });



  getPendingleads = function(){
    $http.get('/getPendingleads')
          .then(
            function(response){
              $rootScope.source= "pending";
              $rootScope.pendingleads = response.data;
               /*alert(angular.toJson("getPendingleads"+ $rootScope.pendingleads));*/
               /*alert(angular.toJson( $rootScope.pendingleads));*/
            },
            function(errResponse){
              console.error("Error while fetching pending Leads");
              return $q.reject(errResponse);
            }
            );
    }
    getPendingleads();
            $scope.showDetails = function(id,showpending){
                  
                   
                   $rootScope.leadId=id;
                   $rootScope.home="home";
                   $rootScope.source=showpending;
                 //  alert(angular.toJson($rootScope.source));
                   ngDialog.open({template: 'app/lead/view_lead.html', controller:'leadFollowupcontroller as followup', scope: $scope});
                };

                  $scope.$on("closengDdialog",function(){
                                getPendingleads();
                               ngDialog.close();

                             });


  }]);



app.controller('staffMemberDetailcontroller',['$scope','$routeParams','$location','BranchService',function($scope,$routeParams,$location,BranchService){
    
     var memberId =  $routeParams.memberId;
      $scope.myVar = true;
          $scope.myPar =  false;
          $scope.toggle = function() {
              $scope.myVar = !$scope.myVar;
              $scope.myPar = !$scope.myPar;
          };

          /*alert("showStaffcontroller");
*/

          var self = this;
        
         self.staffDetail = function(){ 
           /* alert("controller staff Detail " +memberId);*/
             BranchService.fetchStaffMemberData(memberId).then(function(res){
              self.memberDetails =res[0];
            /*  alert(angular.toJson(self.memberDetails));*/
          
              });
         
          };
          self.staffDetail();
          /*=======================*/

            self.memberDetails={ID :memberId };

               self.addStaffUpdata = function(){ 
             /*  alert("addStaffUpdata controller");
                alert(angular.toJson(self.memberDetails));*/
               BranchService.addStaffUpdata(self.memberDetails)
                          .then(
                                       function(d) {

                                             self.memberDetails= d;
                                             /*self.memberDetails={};*/
                                             self.memberDetails={ID :memberId };
                                            /* alert("hello");
                                             alert(angular.toJson(self.memberDetails));*/
                                            $location.path('/staff');

                                       },
                                        function(errResponse){
                                          console.error(errResponse);
                                          console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                        }

                               );

                            
                  };



            
}]);





app.controller('featureController',['$scope','$routeParams','featureService','$http','$location',function($scope,$routeParams,featureService,$http,$location){
  

        var self = this;
        // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
        $scope.selectedSubMenus=[];
        $scope.allFeature=[];

         $scope.showMe = false;
          $scope.myFunc = function() {
              $scope.showMe = !$scope.showMe;
          }
               
          self.fetchAllFeature = function(){
              featureService.fetchAllFeature()
                  .then(
                               function(d) {
                                    $scope.allFeature = d;
                                  
                                    //alert(angular.toJson($scope.allFeature));
                                    
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
          self.fetchMainMenu = function(){
              featureService.fetchMainMenu()
                  .then(
                               function(d) {
                                    $scope.mainMenu = d;
                                  
                                    //alert(angular.toJson($scope.mainMenu));
                                    
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };

          /**/
          $scope.staffinfo={};
          $scope.addFeature = function(){
           
            var subfeature=$scope.selectedSubMenus.filter(function(val){return val !== null;});
            var alldata={"staffid": $routeParams.id,"selectedSubMenus":subfeature};
                var req={
              method: 'POST',
              url: '/InsertFeature',
              headers: { 'Content-Type': 'application/json', data:angular.toJson(alldata)
               }};
                    
                $http(req).then(
                                           function(d) {
                                                $scope.subfeature={};
                                                //alert(angular.toJson(subfeature));
                                           },
                                            function(errResponse){
                                                console.error('Error while insertng InsertFeature');
                                            }
                                   );
                   $location.path('/staff');      
            },
          self.fetchAllFeature();
          self.fetchMainMenu();
        
}]);


app.controller('LoginController', LoginController);
 
    LoginController.$inject = ['$location', 'AuthenticationService','$scope' ];
    function LoginController($location, AuthenticationService,$scope) {
        var vm = this;
        $scope.username ='';
        $scope.password='';
        $scope.result='';
     
        //vm.login = login;
 
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();
 
        $scope.login=function() {
            vm.dataLoading = true;
            $scope.result='';
            AuthenticationService.Login( $scope.username, $scope.password, function (response) {
                if (response.success) {
                	//alert(response);
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/');
                } else {
                	//alert("fail "+response);
                    //$scope.result="Invalid user id or Password";
                      angular.element('#result').addClass('alert alert-danger');
                     angular.element('#result').html("Invalid user id or Password");
                    vm.dataLoading = false;
                }
            });
        };
    }

 app.controller('assignleadcontroller',['$scope','AssignService','StaffService','$http','$timeout',function($scope,AssignService,StaffService,$http,$timeout){
  

          var self = this;
         // self.branch={visitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          $scope.assignleads=[];
           $scope.staff=StaffService.fetchAllStaff()    .then(
                               function(d) {
                                    $scope.allstaff = d;
                                    //alert(angular.toJson(self.allStaff));
                                   
                                    
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );

           
               
          self.fetchAllassignlead = function(){
              AssignService.fetchAllassignlead()
                  .then(
                               function(d) {
                                    $scope.assignleads = d;
                               },
                                function(errResponse){
                                  console.log(errResponse);
                                    console.error('Error while fetching assignlead');
                                }
                       );
          };

          $scope.savelead = function(staffid,leadid ){
            var alldata=[staffid,leadid];
           
                var req={
              method: 'POST',
              url: '/Insertassignstaff',
              headers: { 'Content-Type': 'application/json', data:angular.toJson(alldata)
               }};
                    
                $http(req).then(
                                           function(d) {
                                                self.fetchAllassignlead();
                                                
                                                
                                                angular.element('#alertify').removeClass('alertify alertify-hide alertify-hidden alertify-isHidden').addClass('alertify alertify-show alertify-alert');
                                                  angular.element('#alertify-cover').removeClass('alertify-cover-hidden').addClass('alertify-cover');
                                                $timeout(function(){
                                                  angular.element('#alertify').removeClass('alertify alertify-show alertify-alert').addClass('alertify alertify-hide alertify-hidden alertify-isHidden');
                                                  angular.element('#alertify-cover').removeClass('alertify-cover').addClass('alertify-cover-hidden');
                                                
                                                },3000);
                                                //alert(angular.toJson(subfeature));
                                           },
                                            function(errResponse){
                                                console.error('Error while insertng InsertFeature');
                                            }
                                   );
                  


          };


          self.fetchAllassignlead();
           
}]);
