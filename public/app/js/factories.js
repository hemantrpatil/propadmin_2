'use strict';
 
app.factory('LeadService', ['$http', '$q','$filter', function($http, $q,$filter,server_url){
    var GetAllLeads={};
     var selectedData={};

    return {
         getSelectedData: function(){ return selectedData},
            fetchAllLeads: function() {
                    var req={
                                method: 'GET',
                                url:'/GetAllLeads',
                                withCredentials: true,
                                headers: {
                                      'Content-Type': 'application/json'
                                      
                                  }
                               };
                         return $http(req)
                            .then(
                                    function(response){
                                        GetAllLeads=response.data;
                                        sessionStorage.GetAllLeads =angular.toJson(GetAllLeads);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log(angular.toJson(errResponse));
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

            fetchAllFollowups: function(leadId) {
                /*alert("factory fetchAllFollowups "+leadId);*/
                    return $http.get('/GetFollowupDetails/' + leadId)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             fetchAlltask: function(leadId) {
                /*alert("factory fetchAlltask "+leadId);*/
                    return $http.get('/GettaskDetails/' + leadId)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching Get task Details');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
             fetchLeadSpecificData:function(leadId,GetAllLeads){
                
                if(!GetAllLeads || GetAllLeads==[]) { GetAllLeads = angular.fromJson(sessionStorage.GetAllLeads);
                }
                if(GetAllLeads){
                     
                var single_object = $filter('filter')(GetAllLeads, function (d) {return d.ID == leadId;})[0];
                //alert(angular.toJson(single_object));
                selectedData=single_object;
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
            }
             },
             
             addFollowup: function(user){
                
                    var req={
                                method: 'POST',
                                url: '/InsertFollowup',
                                headers: {
                                            'Content-Type': 'application/json', data:angular.toJson(user)
                                         }
                         };
                    return $http(req)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

             listStaff: function() {
                    return $http.get('/GetStafflist')
                            .then(
                                    function(response){
                                       var GetStafflist= response.data;
                                        sessionStorage.GetStafflist =angular.toJson(GetStafflist);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            
              addactivity: function(user){
                /*alert("factory");*/
                    var req={
                                method: 'POST',
                                url: '/Insertactivity',
                                headers: {
                                            'Content-Type': 'application/json', data:angular.toJson(user)
                                         }
                         };
                    return $http(req)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

              addtask: function(user){
                /*alert("add task factory");*/
                    var req={
                                method: 'POST',
                                url: '/Inserttask',
                                headers: {
                                            'Content-Type': 'application/json', data:angular.toJson(user)
                                         }
                         };
                    return $http(req)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

           
            createUser: function(user){
                    return $http.post(server_url+'/user/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateUser: function(user, id){
                    return $http.put(server_url+'/user/'+id, user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            
            deleteUser: function(id){
                    return $http.delete(server_url+'/user/'+id)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

            viewleadupdate: function(leadId){
               alert(angular.toJson( leadId));
            var req={
                        method: 'POST',
                        url: '/Insertviewleadupdate',
                        headers: {
                                    'Content-Type': 'application/json', data:angular.toJson(leadId)
                                 }
                 };
            return $http(req)
                    .then(
                            function(response){
                                return response.data;
                            }, 
                            function(errResponse){
                                console.log('Error while creating owner');
                                return $q.reject(errResponse);
                            }
                    );
            }
         
    };
 
}]);


app.factory('BranchService', ['$http', '$q', '$filter',function($http, $q,$filter,server_url){
    var GetAllBranches={};
    var GetAllStaff=[];
 
    return {
         
            fetchAllBranches: function() {
                    return  $http.get('/GetAllBranches')
                            .then(
                                    function(response){
                                       
                                        GetAllBranches=response.data;
                                        sessionStorage.GetAllBranches =angular.toJson(GetAllBranches);
                                       //alert(angular.toJson( GetAllBranches));
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             fetchBranchSpecificData:function(BranchId){
                //alert(angular.toJson(GetAllLeads));
                if(GetAllBranches=[]) GetAllBranches = angular.fromJson(sessionStorage.GetAllBranches);
                var single_object = $filter('filter')(GetAllBranches, function (d) {return d.ID == BranchId;})[0];
                
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
             },
             
             fetchAllStaff: function(BranchId) {
                    return $http.get('/GetAllStaff/' )//+ BranchId)
                            .then(
                                    function(response){
                                       GetAllStaff= response.data;
                                       sessionStorage.GetAllStaff =angular.toJson(GetAllStaff);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching staff data');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            fetchStaffMemberData: function(memberId){
                var defferd=$q.defer();
              /*  alert(angular.toJson(" fetchStaffMemberData"));*/
                    $http.get('/GetStaffDetails/'+memberId)
                     .then(
                                    function(response){
                                       
                                        /*alert(angular.toJson("fetchStaffMemberData factory "+response.data));*/
                                        sessionStorage.TenantDetails =angular.toJson(response.data);
                                       defferd.resolve(response.data);
                                       
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        defferd.reject(errResponse);
                                    }


                            );
                
                       return defferd.promise;
              },
              
                   addStaffUpdata: function(memberId){
                    /*alert(angular.toJson(memberId));*/
                    var req={
                                method: 'POST',
                                url: '/InsertStaffUpdate',
                                headers: {
                                            'Content-Type': 'application/json', data:angular.toJson(memberId)
                                         }
                         };
                    return $http(req)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log('Error while creating owner');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            createUser: function(user){
                    return $http.post(server_url+'/user/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateUser: function(user, id){
                    return $http.put(server_url+'/user/'+id, user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteUser: function(id){
                    return $http.delete(server_url+'/user/'+id)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting user');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);
app.factory('StaffService', ['$http', '$q', function($http, $q,server_url){
 
    return {
         
            fetchAllStaff: function() {
                    return $http.get('/GetAllStaff')
                            .then(
                                    function(response){
                                       var GetAllStaff= response.data;
                                        sessionStorage.GetAllStaff =angular.toJson(GetAllStaff);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
              
            createUser: function(user){
                    return $http.post(server_url+'/user/', user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while creating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            updateUser: function(user, id){
                    return $http.put(server_url+'/user/'+id, user)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while updating user');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
             
            deleteUser: function(id){
                    return $http.delete(server_url+'/user/'+id)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while deleting user');
                                        return $q.reject(errResponse);
                                    }
                            );
            }
         
    };
 
}]);

app.factory('AuthenticationService', AuthenticationService);
 
    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'UserService'];
    function AuthenticationService($http, $cookieStore, $rootScope, $timeout, UserService,server_url) {
        var service = {};
 
        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        service.SaveCredentials = SaveCredentials;
        return service;
 
        function Login(username, password, callback) {
 
            /* Dummy authentication for testing, uses $timeout to simulate api call
             ----------------------------------------------*/
      

            $timeout(function () {
                var response;
                 var encodedPassword=Base64.encode( password);
                
                UserService.GetByUsername(username,encodedPassword)
                    .then(function (user) {
   // alert(user.passMD5);

                       // if (user !== null && user.password === password) {
                         if (user !== null && user.passMD5 === encodedPassword) {
                            response = { success: true };
                        } else {
                            response = { success: false, message: 'Username or password is incorrect' };
                        }
                        callback(response);
                    });
            }, 1000);
 
            /* Use this for real authentication
             ----------------------------------------------*/
            //$http.post('/api/authenticate', { username: username, password: password })
            //    .success(function (response) {
            //        callback(response);
            //    });
 
        }
 
        function SetCredentials(username, password) {
            var authdata = Base64.encode(username + ':' + password);
 
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };
 
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
            $http.get('/getMenuDetails/'+ angular.toJson(username)).then(
               
                     function(res){ 
                       //var finalMenu =formatMenu(res.data);
                       localStorage.testMenudata=res.data;
                       $rootScope.menuData= res.data   //finalMenu;
                     angular.element('#topMenu').html($rootScope.menuData);
                      //alert(angular.toJson($rootScope.menuData));
                      },function(err){ console.log("error : " + err)});

        }
 
        function ClearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
        }

        function SaveCredentials(username, password,id) {
             var encPassword = Base64.encode(password);
            var user=[];
            user.push({'email':username,'passMD5':encPassword,'custom1':id});
            return $http.post('/InsertCredentials', user).then(handleSuccess, handleError('Error creating user'));


        }
        function handleSuccess(res) {
            return res.data;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }

    }
//end of Auth service
   var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};

app.factory('UserService', UserService);
 
    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};
 
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
 
        return service;
 
        function GetAll() {
            return $http.get('/api/users').then(handleSuccess, handleError('Error getting all users'));
        }
 
        function GetById(id) {
            return $http.get('/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }
 
        function GetByUsername(username,password) {

            return  $http.get('/GetLoginDetails/' + angular.toJson(username) +'/'+ angular.toJson(password)).then(handleSuccess, handleError('Error getting user by username'));
        }
 
        function Create(user) {
            return $http.post('/api/users', user).then(handleSuccess, handleError('Error creating user'));
        }
 
        function Update(user) {
            return $http.put('/api/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }
 
        function Delete(id) {
            return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }
 
        // private functions
 
        function handleSuccess(res) {
            return res.data;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

     app.factory('ownerService', ['$http', '$q', '$filter',function($http, $q,$filter,server_url){
    var GetAllOwner={};
   
    return {
         
            fetchAllOwner: function() {
                    return $http.get('/GetAllOwner')
                            .then(
                                    function(response){
                                       
                                        GetAllOwner=response.data;
                                        sessionStorage.GetAllOwner =angular.toJson(GetAllOwner);
                                       //alert(angular.toJson( GetAllOwner));
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },/*123*/
            fetchOwnerdetailData:function(ownerId){
                var defferd=$q.defer();
                // alert(angular.toJson(" fetchNewOwnerdetailData"));
                    $http.get('/GetOwnerDetails/'+ownerId)
                     .then(
                                    function(response){
                                       
                                        /*alert(angular.toJson("fetchNewOwnerdetailData factory "+response.data));*/
                                      /*  sessionStorage.newOwner =angular.toJson(response.data);*/
                                       defferd.resolve(response.data);
                                       
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        defferd.reject(errResponse);
                                    }


                            );
                
                       return defferd.promise;
              },
      
            

              addOwnerUpdata: function(ownerId){
                
                    var req={
                                method: 'POST',
                                url: '/InsertOwnerUpdate',
                                headers: {
                                            'Content-Type': 'application/json', data:angular.toJson(ownerId)
                                         }
                         };
                    return $http(req)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log('Error while creating owner');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            /*456*/
              fetchOwnerProperty: function(ownerId) {
                 /*alert(angular.toJson("factory "+ownerId));*/
                return $http.get('/GetOwnerProperty/'+ownerId)
                        .then(
                                function(response){
                                   
                                   var GetOwnerProperty=response.data;
                                    sessionStorage.GetOwnerProperty =angular.toJson(GetOwnerProperty);
                                  /* alert(angular.toJson( GetOwnerProperty));*/
                                    return response.data;
                                }, 
                                function(errResponse){
                                    console.error('Error while fetching users');
                                    return $q.reject(errResponse);
                                }
                        );
            },

        
         }}
        ]);

     app.factory('projectService', ['$http', '$q', '$filter',function($http, $q,$filter,server_url){
        var GetAllProject={};
            return {

                         fetchAllProject: function() {
                            return $http.get('/GetAllProject')
                                    .then(
                                            function(response){
                                               
                                                GetAllProject=response.data;
                                               /* sessionStorage.GetAllProject =angular.toJson(GetAllProject);
                                               alert(angular.toJson( GetAllProject));*/
                                                return response.data;
                                            }, 
                                            function(errResponse){
                                                console.error('Error while fetching users');
                                                return $q.reject(errResponse);
                                            }
                                    );
                         },

             fetchProjectdetail:function(projectid){
                var defferd=$q.defer();
                // alert(angular.toJson(" fetchNewOwnerdetailData"));
                    $http.get('/GetProjectDetails/'+projectid)
                     .then(
                                    function(response){
                                       
                                      /*  alert(angular.toJson("GetProjectDetails factory "+response.data));
                                        sessionStorage.Projectdetail =angular.toJson(response.data);*/
                                       defferd.resolve(response.data);
                                       
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        defferd.reject(errResponse);
                                    }


                            );
                
                       return defferd.promise;
              },
            /*66*/    
              addProjectUpdata: function(projectid){
               /* alert(angular.toJson("factory project id "+projectid));*/
                    var req={
                                method: 'POST',
                                url: '/InsertProjectUpdate',
                                headers: {
                                            'Content-Type': 'application/json', data:angular.toJson(projectid)
                                         }
                         };
                    return $http(req)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log('Error while creating project update');
                                        return $q.reject(errResponse);
                                    }
                            );
            },

                   };

     }]);
/*123*/
    app.factory('propertyService', ['$http', '$q', '$filter',function($http, $q,$filter,server_url){
    var GetAllProprety={};
    var GetAllStaff=[];
  
 
    return {
         
            fetchAllProperty: function() {
                    return $http.get('/GetAllProprety')
                            .then(
                                    function(response){
                                       
                                        GetAllProprety=response.data;
                                        sessionStorage.GetAllProprety =angular.toJson(GetAllProprety);
                                       //alert(angular.toJson( GetAllProprety));
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            /*99*/
           
            /**/
             fetchBranchSpecificData:function(BranchId){
                //alert(angular.toJson(GetAllLeads));
                if(GetAllProprety=[]) GetAllProprety = angular.fromJson(sessionStorage.GetAllProprety);
                var single_object = $filter('filter')(GetAllProprety, function (d) {return d.id == BranchId;})[0];
                
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
             },
            
             fetchPropertydetail:function(propertyid){
                var defferd=$q.defer();
                // alert(angular.toJson(" fetchNewOwnerdetailData"));
                    $http.get('/GetPropertyDetails/'+propertyid)
                     .then(
                                    function(response){
                                       
                                       /* alert(angular.toJson("GetPropertyDetails factory "+response.data));
                                        sessionStorage.Propertydetail =angular.toJson(response.data);*/
                                       defferd.resolve(response.data);
                                       
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        defferd.reject(errResponse);
                                    }


                            );
                
                       return defferd.promise;
              },
             
                fetchOwnerTenantdetail:function(propertyid){
                var defferd=$q.defer();
               /* alert(angular.toJson("fetchNewOwnerdetailData "+propertyid));*/
                    $http.get('/GetOwnerTenantDetails/'+propertyid)
                     .then(
                                    function(response){
                                       
                                        /*alert(angular.toJson("Get Tenant Details factory "+response.data));
                                        sessionStorage.Propertydetail =angular.toJson(response.data);*/
                                        defferd.resolve(response.data);
                                       
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        defferd.reject(errResponse);
                                    }


                            );
                
                       return defferd.promise;
              },

              /*88*/
               TenantList:function(){
                var defferd=$q.defer();
               /* alert(angular.toJson("fetchNewOwnerdetailData "+propertyid));*/
                    $http.get('/GetTenantList')
                     .then(
                                    function(response){
                                       
                                       /* alert(angular.toJson("Get Tenant list factory "+response.data));
                                        sessionStorage.TenentList =angular.toJson(response.data);*/
                                        defferd.resolve(response.data);
                                       
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        defferd.reject(errResponse);
                                    }


                            );
                
                       return defferd.promise;
              },
              /*88*/


                fetchTenantdetail:function(tenantid){
                var defferd=$q.defer();
                // alert(angular.toJson(" fetchNewOwnerdetailData"));
                    $http.get('/GetTenantDetails/'+tenantid)
                     .then(
                                    function(response){
                                       
                                       /* alert(angular.toJson("GetTenantDetails factory "+response.data));*/
                                        sessionStorage.TenantDetails =angular.toJson(response.data);
                                       defferd.resolve(response.data);
                                       
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        defferd.reject(errResponse);
                                    }


                            );
                
                       return defferd.promise;
              },
                 addTenantUpdata: function(tenantid){
                    /*alert(angular.toJson(tenantid));*/
                    var req={
                                method: 'POST',
                                url: '/InsertTenantUpdate',
                                headers: {
                                            'Content-Type': 'application/json', data:angular.toJson(tenantid)
                                         }
                         };
                    return $http(req)
                            .then(
                                    function(response){
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.log('Error while creating owner');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
         }}
        ]);

    

        app.factory('featureService', ['$http', '$q', function($http, $q,server_url){
 
         return {
            fetchAllFeature: function() {
                    return $http.get('/GetAllFeature')
                            .then(
                                    function(response){
                                       var GetAllFeature= response.data;
                                        sessionStorage.GetAllFeature =angular.toJson(GetAllFeature);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
            fetchMainMenu: function() {
                    return $http.get('/GetMainMenu')
                            .then(
                                    function(response){
                                       var GetMainMenu= response.data;
                                        sessionStorage.GetMainMenu =angular.toJson(GetMainMenu);
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
    }}]);

app.factory('AssignService', ['$http', '$q', '$filter',function($http, $q,$filter,server_url){
    var fetchAllassignlead={};
    
 
    return {
         
            fetchAllassignlead: function() {
                    return $http.get('/fetchAllassignlead')
                            .then(
                                    function(response){
                                       
                                        fetchAllassignlead=response.data;
                                        sessionStorage.fetchAllassignlead =angular.toJson(fetchAllassignlead);
                                       //alert(angular.toJson( GetAllBranches));
                                        return response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching users');
                                        return $q.reject(errResponse);
                                    }
                            );
            },
}}]);