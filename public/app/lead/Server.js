var express         =         require("express");
var mysql           =         require("mysql");
var app             =         express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//app.use(bodyParser());
/*
  * Configure MySQL parameters.
*/
var connection      =         mysql.createConnection({
        host        :         "localhost",
        user        :         "root",
        password    :         "propadmin1",
        database    :         "rentmanagement"
       
        
});

connection.connect(function(error){
  if(error)
    {
      console.log("Problem with MySQL"+error);
    }
  else
    {
      console.log("Connected with Database");
    }
});

/*
  * Configure Express Server.
*/

app.use(express.static(__dirname + '/angular'));
app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,data");
  next();
});
/*
  * Define routing of the application.
*/
app.get('/',function(req,res){
  res.sendfile('index.html');
});

app.get('/GetAllLeadDetails',function(req,res)
{
  connection.query("SELECT distinct tl.person_name, tl.contact_number, tl.email from rentmanagement.tbl_lead tl join rentmanagement.tbl_lead_followup tlf on tlf.lead_id=tl.ID where tlf.next_followup_date = convert_tz(current_date(),@@session.time_zone,'+05:30')",function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});

app.get('/GetAllLeads',function(req,res){
  connection.query("SELECT * from tbl_lead",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/load',function(req,res){
  connection.query("SELECT * FROM rentmanagement.tbl_lead",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});//
app.get('/GetFollowupDetails/:leadId',function(req,res){
  var varLeadId =req.params.leadId;
  
  console.log(varLeadId);
  connection.query("SELECT * FROM rentmanagement.tbl_lead_followup where lead_id =  " + varLeadId +
            " order by date desc",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});



app.get('/GetAllBranches',function(req,res){
  var varLeadId =req.params.leadId;
  
  console.log(varLeadId);
  connection.query("SELECT * FROM rentmanagement.tbl_branch" ,function(err,rows){
    //connection.query("CALL sp_get_branch()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
            


          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetAllStaff',function(req,res){
  var varStaff =req.params.Staff;
  
  console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_staff" ,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetAllProprety',function(req,res){
  //var varStaff =req.params.Staff;
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_property" ,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetloginDetails/:id/:password',function(req,res){
  var email =JSON.parse(req.params.id);
  var password =JSON.parse(req.params.password);
  var temp =[];
  temp.push(email);
  temp.push(password);
  
  console.log(req.params.password);
  connection.query("SELECT email,memberID,passMD5 FROM tbl_membership_users where email = ?  and passMD5 = ?",  temp ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { console.log(rows);
          res.end(JSON.stringify(rows[0]));
        }
  });
});
app.get('/getMenuDetails',function(req,res){
  


  
  console.log(req.params.data);
  connection.query("SELECT  tm.name AS menuName, tmiii.name AS subMenuName,tmiii.html   AS subMenuHTML,tm.html AS menuHTML  FROM `tbl_menu_items` AS tmiii"+
        " INNER JOIN  `tbl_menu` AS tm ON tm.id=tmiii.menu_id LIMIT 0, 1000",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          var test=formatMenu(rows)
         // console.log(JSON.stringify(test));
          res.end(test);
        }
  });
});

       function formatMenu (rows){
        
        var sb = '';
        var mainMenuPrefix ="<li  data-toggle='collapse'  class='collapsed'";
        var mainMenuPrefixEnd =">";
        var subMenuPrefix ="<ul class='sub-menu collapse'";
        var subMenuPrefixEnd =">";
        var mainMenuSuffix ="</li>";
        var subMenuSuffix ="</ul>";
        var mainmenu=true;
        var previous="test";
        
        
for (var i = 0; i < rows.length; i++) {
 // alert(rows[i]["menuName"]);
      if(mainmenu && ! (rows[i]["menuName"]===previous) ){
        //alert(rows[i]["menuName"]);
        if(previous!="test"){
                  sb= sb + subMenuSuffix;
                  //sb=sb+mainMenuSuffix;
                }
        sb=sb+mainMenuPrefix + "  data-target='#" +rows[i]["menuName"]+"'"+mainMenuPrefixEnd;
      sb=sb+rows[i]["menuHTML"];
      sb=sb+mainMenuSuffix;
       sb=sb+subMenuPrefix + "  id='" +rows[i]["menuName"]+"'"+subMenuPrefixEnd;

      }
       previous=rows[i]["menuName"];
           sb=sb+rows[i]["subMenuHTML"];
          if(previous !="test" && ! rows[i]["menuName"]===previous) {
            sb=sb+subMenuSuffix;
           // sb=sb+mainMenuSuffix;
            
          }

    } //end of for
            sb=sb+subMenuSuffix;
           //sb=sb+mainMenuSuffix;
            
            console.log(sb);
return sb;

    } // end  of funcntion

 

app.post('/insertLead',function(req,res){
  var varLead =JSON.parse(req.get('data'));
  
  console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_lead` SET  ?",  varLead ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/InsertBranch/',function(req,res){
  var varBranch =JSON.parse(req.get('data'));
  
  console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_branch` SET  ?",  varBranch ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/InsertCredentials',function(req,res){
  var varCredentils = req.body;  //  JSON.parse(req.get('data'));
 console.log(JSON.stringify(req.body));
  
  //console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_membership_users` SET  ?",  varCredentils ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/InsertStaffMember',function(req,res){
  var varStaff =JSON.parse(req.get('data'));
  
  console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_staff` SET  ?",  varStaff ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/InsertFollowup/:data',function(req,res){
  var varFollowUp =JSON.parse(req.params.data);
  
  console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_lead_followup` SET  ?",  varFollowUp ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { 
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/InsertProperty',function(req,res){
 
    
  console.log("hello"+ JSON.stringify(req.headers));
   var varProperty =JSON.parse(req.get('data'));
  connection.query("INSERT INTO `rentmanagement`.`tbl_property` SET  ?",  varProperty ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { 
          res.end(JSON.stringify(rows));
        }
  });
});

/* connection.query("SELECT  tm.name AS menuName, tmiii.name AS subMenuName,tmiii.html   AS subMenuHTML,tm.html AS menuHTML  FROM `tbl_menu_items` AS tmiii"+
        " INNER JOIN  `tbl_menu` AS tm ON tm.id=tmiii.menu_id LIMIT 0, 1000",function(err,rows)*/

app.get('/GetAllFeature',function(req,res){
  var varFeature =req.params.Feature;
  
  console.log(varFeature);
 connection.query("select distinct tm.id as mainid, tm.name as mainmenu, tmi.id as tmiid, tmi.name as tmsubmenu from rentmanagement.tbl_menu tm inner join rentmanagement.tbl_menu_items tmi on tm.id= tmi.menu_id ",function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetMainMenu',function(req,res){
  var varFeature =req.params.Feature;
  
  console.log(varFeature);
 connection.query("select distinct tm.id as mainid, tm.name as mainmenu from rentmanagement.tbl_menu tm inner join rentmanagement.tbl_menu_items tmi on tm.id= tmi.menu_id ",function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/InsertFeature',function(req,res){
 
    
 // console.log(JSON.stringify(req.headers));
   var varFeature =JSON.parse(req.get('data'));
   var allmenus=varFeature.selectedSubMenus;
   var TopWrap=[];
   var bulkRecords=[];
   for(var i=0;i<allmenus.length;i++){
    var row=[];
    row.push(parseInt(varFeature.staffid));
    row.push(allmenus[i]);
    bulkRecords.push(row);
    }
    TopWrap.push(bulkRecords)
    var sql="INSERT INTO `rentmanagement`.`tbl_person_menu_items` ( staff_id, menu_items_id) VALUES  ?";

   
    console.log(JSON.stringify(TopWrap));
    connection.query(sql,  TopWrap ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { 
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/fetchAllassignlead',function(req,res){
  var varLeadId =req.params.leadId;
  
  console.log(varLeadId);
  connection.query("SELECT * FROM rentmanagement.tbl_lead where assign_staff_id is NULL" ,function(err,rows){
    //connection.query("CALL sp_get_branch()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
            


          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/Insertassignstaff',function(req,res){
  var varLead =JSON.parse(req.get('data'));
  //var staffid=varLead.staffid;
  //var leadid= varLead.leadid;
  
  
  console.log(varLead);
  connection.query("UPDATE `rentmanagement`.`tbl_lead` SET assign_staff_id=? where ID=  ? ",varLead,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

/*
/*
  * Start the Express Web Server.
*/
app.listen(3000,function(){
  console.log("It's Started on PORT 3000");
});
