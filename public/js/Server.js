var express         =         require("express");
var mysql           =         require("mysql");
var app             =         express();
var compression     =         require('compression');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(compression());
//app.use(bodyParser());
/*
  * Configure MySQL parameters.
*/
var connection      =         mysql.createConnection({
        host        :         "localhost",
        user        :         "root",
        password    :         "propadmin",
        database    :         "rentmanagement",
        port        :         "3306"
});

connection.connect(function(error){
  if(error)
    {
      console.log("Problem with MySQL"+error);
    }
  else
    {
      console.log("Connected with Database");
    }
});

/*
  * Configure Express Server.
*/

app.use(express.static(__dirname + '/angular'));
/*app.use(express.static(__dirname + '/public'));*/
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,data");
  next();
});
/*
  * Define routing of the application.
*/
app.get('/',function(req,res){
  res.sendfile('index.html');
});

app.get('/GetAllFollowupreminder',function(req,res)
{

 // connection.query("SELECT DISTINCT tl.*,tlf.details followup_remark FROM rentmanagement.tbl_lead tl JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID WHERE tlf.next_followup_date =CURRENT_DATE() AND tlf.status IS NULL  ORDER BY next_followup_time ASC ",function(err,rows)
//SELECT tl.* ,tlf.next_followup_time,tlf.next_followup_date,tlf.status  FROM rentmanagement.tbl_lead tl JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID WHERE  STR_TO_DATE(tlf.next_followup_date,'%d/%m/%Y') < DATE_FORMAT(CURRENT_DATE() ,'%Y-%m-%d') AND tlf.status IS NULL ORDER BY tlf.next_followup_date, tlf.next_followup_time 

/*SELECT tl.* ,tlf.next_followup_time,tlf.next_followup_date,tlf.status  FROM rentmanagement.tbl_lead tl
JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID 
WHERE tlf.status IS NULL 
ORDER BY tlf.next_followup_date, tlf.next_followup_time*/
connection.query("SELECT tl.* ,tlf.next_followup_time,tlf.next_followup_date,tlf.status  FROM rentmanagement.tbl_lead tl JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID WHERE  STR_TO_DATE(tlf.next_followup_date,'%m/%d/%Y') = DATE_FORMAT(CURRENT_DATE() ,'%Y-%m-%d') AND tlf.status IS NULL ORDER BY tlf.next_followup_date, tlf.next_followup_time",function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});

/*app.get('/GetAllLeadDetails',function(req,res)
{

 // connection.query("SELECT DISTINCT tl.*,tlf.details followup_remark FROM rentmanagement.tbl_lead tl JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID WHERE tlf.next_followup_date =CURRENT_DATE() AND tlf.status IS NULL  ORDER BY next_followup_time ASC ",function(err,rows)
//SELECT tl.* ,tlf.next_followup_time,tlf.next_followup_date,tlf.status  FROM rentmanagement.tbl_lead tl JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID WHERE  STR_TO_DATE(tlf.next_followup_date,'%d/%m/%Y') < DATE_FORMAT(CURRENT_DATE() ,'%Y-%m-%d') AND tlf.status IS NULL ORDER BY tlf.next_followup_date, tlf.next_followup_time 

SELECT tl.* ,tlf.next_followup_time,tlf.next_followup_date,tlf.status  FROM rentmanagement.tbl_lead tl
JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID 
WHERE tlf.status IS NULL 
ORDER BY tlf.next_followup_date, tlf.next_followup_time
connection.query("SELECT tl.* ,tlf.next_followup_time,tlf.next_followup_date,tlf.status  FROM rentmanagement.tbl_lead tl JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID WHERE  STR_TO_DATE(tlf.next_followup_date,'%d/%m/%Y') = DATE_FORMAT(CURRENT_DATE() ,'%Y-%m-%d') AND tlf.status IS NULL ORDER BY tlf.next_followup_date, tlf.next_followup_time",function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});*/


app.get('/GetAllLeads',function(req,res){
/*  SELECT tbl.* , tblf.details, tblf.next_followup_date, tblf.next_followup_time FROM tbl_lead tbl JOIN tbl_lead_followup tblf ON tblf.ID = tbl.ID WHERE tblf.status is null ORDER BY ID DESC*/
  connection.query("SELECT tbl.* , tblf.details, tblf.next_followup_date, tblf.next_followup_time FROM tbl_lead tbl JOIN tbl_lead_followup tblf ON tblf.lead_id = tbl.ID WHERE tblf.status IS NULL ORDER BY ID DESC",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/load',function(req,res){
  connection.query("SELECT * FROM rentmanagement.tbl_lead ",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});//
app.get('/GetFollowupDetails/:leadId',function(req,res){
  var varLeadId =req.params.leadId;
  console.log(varLeadId);
  connection.query("SELECT * FROM rentmanagement.tbl_lead_followup where lead_id =  " + varLeadId +
            " order by ID DESC ",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});



app.get('/GetAllBranches',function(req,res){
  var varLeadId =req.params.leadId;
  console.log(varLeadId);
  connection.query("SELECT * FROM rentmanagement.tbl_branch" ,function(err,rows){
    //connection.query("CALL sp_get_branch()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetAllStaff',function(req,res){
  var varStaff =req.params.Staff;
  connection.query("SELECT * FROM rentmanagement.tbl_staff" ,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetAllOwner',function(req,res){
  //var varStaff =req.params.Staff;
  connection.query("SELECT * FROM rentmanagement.tbl_owner" ,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetOwnerProperty/:ownerId',function(req,res){
 var varOwner =req.params.ownerId;
 connection.query("SELECT * FROM rentmanagement.tbl_property where owner_id = ?" ,varOwner,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetOwnerDetails/:Owner',function(req,res){
  var varOwner =req.params.Owner;
  console.log("Get id " + varOwner);
  connection.query("SELECT * FROM rentmanagement.tbl_owner where ID = ?" ,varOwner,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/InsertProject',function(req,res){
  var varproject =JSON.parse(req.get('data'));
  connection.query("INSERT INTO `rentmanagement`.`tbl_project` SET  ?",  varproject ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));  
        }
 return compression.filter(req, res)
  });
    

});

app.post('/AddPhase',function(req,res){
 /* var UpdatePhases =JSON.parse(req.get('data'));
  
  var temp=[{Phases:UpdatePhases[0]},{ID:UpdatePhases[1]}];
  console.log(JSON.parse(req.get('data')));
   console.log(JSON.stringify(temp));
  connection.query("UPDATE `rentmanagement`.`tbl_project` SET  ? where ?",  temp ,function(err,rows){
    */
    var varphase =JSON.parse(req.get('data'));
    connection.query("INSERT INTO `rentmanagement`.`tbl_phase` SET  ?", varphase ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else 
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/AddBuilding',function(req,res){
 /* var UpdatePhases =JSON.parse(req.get('data'));
  
  var temp=[{Phases:UpdatePhases[0]},{ID:UpdatePhases[1]}];
  console.log(JSON.parse(req.get('data')));
   console.log(JSON.stringify(temp));
  connection.query("UPDATE `rentmanagement`.`tbl_project` SET  ? where ?",  temp ,function(err,rows){
    */
    var varbuilding =JSON.parse(req.get('data'));
console.log(req.get('data'));
  
  connection.query("INSERT INTO `rentmanagement`.`tbl_building` SET  ?",  varbuilding ,function(err,rows){
    
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else 
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/AddWing',function(req,res){
 /* var UpdatePhases =JSON.parse(req.get('data'));
  
  var temp=[{Phases:UpdatePhases[0]},{ID:UpdatePhases[1]}];
  console.log(JSON.parse(req.get('data')));
   console.log(JSON.stringify(temp));
  connection.query("UPDATE `rentmanagement`.`tbl_project` SET  ? where ?",  temp ,function(err,rows){
    */
    var varbuilding =JSON.parse(req.get('data'));
console.log(req.get('data'));
  
  connection.query("INSERT INTO `rentmanagement`.`tbl_wings` SET  ?",  varbuilding ,function(err,rows){
    
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else 
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/AddFlats',function(req,res){
 /* var UpdatePhases =JSON.parse(req.get('data'));
  
  var temp=[{Phases:UpdatePhases[0]},{ID:UpdatePhases[1]}];
  console.log(JSON.parse(req.get('data')));
   console.log(JSON.stringify(temp));
  connection.query("UPDATE `rentmanagement`.`tbl_project` SET  ? where ?",  temp ,function(err,rows){
    */
    var varflats =JSON.parse(req.get('data'));
console.log(req.get('data'));
  
  connection.query("INSERT INTO `rentmanagement`.`tbl_flat`(`wing_id`,`number`,`carpet_area`, `builtup_area`,`superbuiltup_area`,`BHK`,`use_Type`,`use_type_category`) VALUES  ?",  [varflats] ,function(err,rows){
  

    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else 
        {
          res.end(JSON.stringify(rows));
        }
  });
});




app.get('/GetProjectDetails/:projectid',function(req,res){
  var varprojectid =req.params.projectid;
  console.log("Get varproject id " + varprojectid);
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_project where ID = ?" ,varprojectid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetPhases',function(req,res){
  
  var varprojectid =JSON.parse(req.get('data'));
  console.log("Get varproject id " + varprojectid);
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_phase where projectid = ?" ,varprojectid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetBuildings',function(req,res){
  
  var varPhaseid =JSON.parse(req.get('data'));
  console.log("Get varPhaseid id " + varPhaseid);
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_building where phase_id = ?" ,varPhaseid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetWings',function(req,res){
  
  var varBuildingid =JSON.parse(req.get('data'));
 // console.log("Get varPhaseid id " + varPhaseidvarPhaseid);
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_wings where building_id = ?" ,varBuildingid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetFlats',function(req,res){
  
  var varWingid =JSON.parse(req.get('data'));
 // console.log("Get varPhaseid id " + varPhaseidvarPhaseid);
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_flat where wing_id = ?" ,varWingid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});


app.get('/GetPropertyDetails/:propertyid',function(req,res){
  var varpropertyid =req.params.propertyid;
  connection.query("SELECT * FROM rentmanagement.tbl_property where property_id = ?" ,varpropertyid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetOwnerTenantDetails/:propertyid',function(req,res){
  var varpropertyid =req.params.propertyid;
  /*console.log("Get Property id " + varpropertyid);*/
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_tenant where property_id = ?" ,varpropertyid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetTenantList',function(req,res){
  /*var varpropertyid =req.params.propertyid;*/
  /*console.log("Get Property id " + varpropertyid);*/
  
  //console.log(varStaff);
 connection.query("SELECT * FROM tbl_tenant WHERE property_id IS NULL" ,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
/*456*/
app.get('/GetTenantDetails/:tenantid',function(req,res){
  var vartenantid =req.params.tenantid;
  /*console.log("Get Property id " + varpropertyid);*/
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_tenant where ID = ?" ,vartenantid,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetAllProprety',function(req,res){
  //var varStaff =req.params.Staff;
  
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_property" ,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL" + err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetAllProject',function(req,res){
  //var varStaff =req.params.Staff;
  
  
  //console.log(varStaff);
 connection.query("SELECT * FROM rentmanagement.tbl_project" ,function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});


app.get('/GetloginDetails/:id/:password',function(req,res){
  var email =JSON.parse(req.params.id);
  var password =JSON.parse(req.params.password);
  var temp =[];
  temp.push(email);
  temp.push(password);
  console.log(req.params.password);
  connection.query("SELECT email,memberID,passMD5 FROM tbl_membership_users where email = ?  and passMD5 = ?",  temp ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { console.log(rows);
          res.end(JSON.stringify(rows[0]));
        }
  });
});

app.get('/getMenuDetails/:id',function(req,res){
  var email=JSON.parse(req.params.id);
  connection.query("SELECT DISTINCT tm.name AS menuName, tmiii.name AS subMenuName, tmiii.html AS subMenuHTML,tm.html AS menuHTML,tm.order  FROM `tbl_menu_items` AS tmiii RIGHT JOIN  `tbl_menu` AS tm ON tm.id=tmiii.menu_id "+
     " INNER JOIN tbl_person_menu_items AS tpmi ON tpmi.menu_items_id= tmiii.id "+
   " INNER JOIN tbl_membership_users AS ts  ON ts.memberID= tpmi.staff_id WHERE ts.email= ? order by tm.order ",email,function(err,rows){

    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          var test=formatMenu(rows)
         // console.log(JSON.stringify(test));
          res.end(test);
        }
  });
});

       function formatMenu (rows){
        
        var sb = '';
        var mainMenuPrefix ="<li  data-toggle='collapse'  class='collapsed'";
        var mainMenuPrefixEnd =">";
        var subMenuPrefix ="<ul class='sub-menu collapse'";
        var subMenuPrefixEnd =">";
        var mainMenuSuffix ="</li>";
        var subMenuSuffix ="</ul>";
        var mainmenu=true;
        var previous="test";
        
        
for (var i = 0; i < rows.length; i++) {
 // alert(rows[i]["menuName"]);
      if(mainmenu && ! (rows[i]["menuName"]===previous) ){
        //alert(rows[i]["menuName"]);
        if(previous!="test"){
                  sb= sb + subMenuSuffix;
                  //sb=sb+mainMenuSuffix;
                }
        sb=sb+mainMenuPrefix + "  data-target='#" +rows[i]["menuName"]+"'"+mainMenuPrefixEnd;
      sb=sb+rows[i]["menuHTML"];
      sb=sb+mainMenuSuffix;
       sb=sb+subMenuPrefix + "  id='" +rows[i]["menuName"]+"'"+subMenuPrefixEnd;

      }
       previous=rows[i]["menuName"];
           sb=sb+rows[i]["subMenuHTML"];
          if(previous !="test" && ! rows[i]["menuName"]===previous) {
            sb=sb+subMenuSuffix;
           // sb=sb+mainMenuSuffix;
            
          }

    } //end of for
            sb=sb+subMenuSuffix;
           //sb=sb+mainMenuSuffix;
            
            console.log(sb);
return sb;

    } // end  of funcntion

app.post('/InsertSignup/',function(req,res){
  var varSignup =JSON.parse(req.get('data'));
  connection.query("INSERT INTO `rentmanagement`.`tbl_membership_users` SET  ?",  varSignup ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/insertLead',function(req,res){
  var varLead =JSON.parse(req.get('data'));
  var leaddata=varLead;
  leaddata.status="pending";
  delete leaddata.folowup_remarks;
  delete leaddata.followupdate;
  delete leaddata.followuptime;
  var newItem =[];
    /*newItem.push({"next_followup_date":varLead.followupdate});*/
  connection.query("INSERT INTO `rentmanagement`.`tbl_lead` SET  ?",  leaddata ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
  /*       console.log("test rows leadid "+JSON.stringify(rows));*/
              newItem.push({"lead_id":rows.insertId, "Details":varLead.folowup_remarks,"next_followup_date":varLead.followupdate});
/*console.log("test value "+JSON.stringify(newItem));*/
          connection.query("INSERT INTO rentmanagement.tbl_lead_followup SET ?",  newItem ,function(err,rows){
              if(err)
                {
                  console.log("Problem with MySQL"+err);
                }
                else
                  { 
                    res.end(JSON.stringify(rows));
                  }
         });
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/UpdateLead',function(req,res){
  var updateLead =JSON.parse(req.get('data'));
  /*var temp=[{status:updateLead[0]},{ID:updateLead[1]}];*/
  var temp=[{ID:updateLead.ID}];
  console.log(temp);
  console.log(JSON.parse(req.get('data')));
  /* console.log(JSON.stringify(temp));*/
  connection.query("UPDATE `rentmanagement`.`tbl_lead` SET  ? where ID=?",  temp ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else 
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/InsertBranch/',function(req,res){
  var varBranch =JSON.parse(req.get('data'));
  console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_branch` SET  ?",  varBranch ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/InsertCredentials',function(req,res){
  var varCredentils = req.body;  //  JSON.parse(req.get('data'));
 console.log(JSON.stringify(req.body));
  
  //console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_membership_users` SET  ?",  varCredentils ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/InsertStaffMember',function(req,res){
  var varStaff =JSON.parse(req.get('data'));
  
  console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_staff` SET  ?",  varStaff ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/InsertFollowup',function(req,res){
  var varFollowUp =JSON.parse(req.get('data'));
  
  //console.log(req.params.data);
  connection.query("INSERT INTO `rentmanagement`.`tbl_lead_followup` SET ?",  varFollowUp ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {  var status=[];
          status.push("close");
          status.push(JSON.stringify(rows.insertId));
          status.push(varFollowUp.lead_id);
          console.log(JSON.stringify(status));
          connection.query("update `rentmanagement`.`tbl_lead_followup`  SET status =? where ID != ? AND  lead_id=?", status,function(err,rows){
          if(err)
            {
              console.log("Problem with MySQL"+err);
            }
            else
              { 
                
                res.end(JSON.stringify(rows));

              }
        });
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));

        }
  });
});
app.post('/InsertOwnerUpdate',function(req,res){
  var varOwnerUp =JSON.parse(req.get('data'));
  
  console.log(varOwnerUp);
  //console.log(req.params.data);
          var owner=[];
         
          owner.push(varOwnerUp);
          owner.push(varOwnerUp.ID);
          console.log(JSON.stringify(varOwnerUp));
         connection.query("update `rentmanagement`.`tbl_owner`  SET ?  where  ID=?",owner, function(err,rows){
          if(err)
            {
              console.log("Problem with MySQL"+err);
            }
            else
              { 
                
                res.end(JSON.stringify(rows));

              }
        });

});
app.post('/InsertTenantUpdate',function(req,res){
  var varTenantUp =JSON.parse(req.get('data'));
  
  /*console.log(varTenantUp);
  console.log(req.params.data);*/
          var tenant=[];
         
          tenant.push(varTenantUp);
          tenant.push(varTenantUp.ID);
          console.log(JSON.stringify(varTenantUp));
         connection.query("update `rentmanagement`.`tbl_tenant`  SET ?  where  ID=?",tenant, function(err,rows){
          if(err)
            {
              console.log("Problem with MySQL"+err);
            }
            else
              { 
                
                res.end(JSON.stringify(rows));

              }
        });

});
app.post('/InsertProjectUpdate',function(req,res){
  var varProectUp =JSON.parse(req.get('data'));
  
  console.log(varProectUp);
  //console.log(req.params.data);
          var project=[];
         
          project.push(varProectUp);
          project.push(varProectUp.ID);
          console.log(JSON.stringify(varProectUp));
         connection.query("update `rentmanagement`.`tbl_project`  SET ?  where  ID=?",project, function(err,rows){
          if(err)
            {
              console.log("Problem with MySQL"+err);
            }
            else
              { 
                
                res.end(JSON.stringify(rows));

              }
        });

});
/*456*/
app.get('/InsertProperty',function(req,res){
 
    
  console.log("hello"+ JSON.stringify(req.headers));
   var varProperty =JSON.parse(req.get('data'));
  connection.query("INSERT INTO `rentmanagement`.`tbl_property` SET  ?",  varProperty ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { 
          res.end(JSON.stringify(rows));
        }
  });
});

/* connection.query("SELECT  tm.name AS menuName, tmiii.name AS subMenuName,tmiii.html   AS subMenuHTML,tm.html AS menuHTML  FROM `tbl_menu_items` AS tmiii"+
        " INNER JOIN  `tbl_menu` AS tm ON tm.id=tmiii.menu_id LIMIT 0, 1000",function(err,rows)*/

app.get('/GetAllFeature',function(req,res){
  var varFeature =req.params.Feature;
  
  console.log(varFeature);
 connection.query("select distinct tm.id as mainid, tm.name as mainmenu, tmi.id as tmiid, tmi.name as tmsubmenu from rentmanagement.tbl_menu tm inner join rentmanagement.tbl_menu_items tmi on tm.id= tmi.menu_id ",function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetMainMenu',function(req,res){
  var varFeature =req.params.Feature;
  
  console.log(varFeature);
 connection.query("select distinct tm.id as mainid, tm.name as mainmenu from rentmanagement.tbl_menu tm inner join rentmanagement.tbl_menu_items tmi on tm.id= tmi.menu_id ",function(err,rows){
  //connection.query("CALL sp_get_staff()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/InsertFeature',function(req,res){
 
    
 // console.log(JSON.stringify(req.headers));
   var varFeature =JSON.parse(req.get('data'));
   var allmenus=varFeature.selectedSubMenus;
   var TopWrap=[];
   var bulkRecords=[];
   for(var i=0;i<allmenus.length;i++){
    var row=[];
    row.push(parseInt(varFeature.staffid));
    row.push(allmenus[i]);
    bulkRecords.push(row);
    }
    TopWrap.push(bulkRecords)
    var sql="INSERT INTO `rentmanagement`.`tbl_person_menu_items` ( staff_id, menu_items_id) VALUES  ?";

   
    console.log(JSON.stringify(TopWrap));
    connection.query(sql,  TopWrap ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { 
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/InsertOwner',function(req,res){
  var varOwner =JSON.parse(req.get('data'));
  
  console.log(req.get('data'));
   console.log("test");
  connection.query("INSERT INTO `rentmanagement`.`tbl_owner` SET  ?",  varOwner ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
    

});
app.post('/InsertTenant',function(req,res){
  var varTenant =JSON.parse(req.get('data'));
  
  console.log(req.get('data'));
   console.log("test");
  connection.query("INSERT INTO `rentmanagement`.`tbl_tenant` SET  ?",  varTenant ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
    

});



app.get('/fetchAllassignlead',function(req,res){
  var varLeadId =req.params.leadId;
  
  console.log(varLeadId);
  connection.query("SELECT * FROM rentmanagement.tbl_lead where assign_staff_id is NULL" ,function(err,rows){
    //connection.query("CALL sp_get_branch()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
            


          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/Insertassignstaff',function(req,res){
  var varLead =JSON.parse(req.get('data'));
  //var staffid=varLead.staffid;
  //var leadid= varLead.leadid;
  
  console.log(varLead);
  connection.query("UPDATE `rentmanagement`.`tbl_lead` SET assign_staff_id=? where ID=  ? ",varLead,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

/*
/*
  * Start the Express Web Server.
*/
app.listen(4000,function(){
  console.log("It's Started on PORT 4000");
});

app.post('/updatelead',function(req,res){
 var varLead =JSON.parse(req.get('data'));
 status=varLead[0];
 ID=varLead[1];
   console.log(status+ID);
  connection.query("UPDATE `rentmanagement`.`tbl_lead` SET status= ? where ID=? ",varLead,function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.post('/UpdateEditLead',function(req,res){
 var upeditlead =JSON.parse(req.get('data'));
 var temp= [];
  temp.push(upeditlead);
 temp.push({ID:upeditlead.ID});
   /*console.log(status+ID);*/
console.log(temp);
  connection.query("UPDATE `rentmanagement`.`tbl_lead` SET ? where ?",temp,function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/getPendingLeads',function(req,res)
{

  connection.query("SELECT tl.* ,tlf.next_followup_time,tlf.next_followup_date,tlf.status  FROM rentmanagement.tbl_lead tl JOIN rentmanagement.tbl_lead_followup tlf ON tlf.lead_id=tl.ID WHERE  STR_TO_DATE(tlf.next_followup_date,'%m/%d/%Y') < DATE_FORMAT(CURRENT_DATE() ,'%Y-%m-%d') AND tlf.status IS NULL ORDER BY tlf.next_followup_date, tlf.next_followup_time ",function(err,rows)


  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});