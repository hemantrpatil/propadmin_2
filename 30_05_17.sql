/*
SQLyog Community v12.2.1 (64 bit)
MySQL - 5.5.48 : Database - rentmanagement
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rentmanagement` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `rentmanagement`;

/*Table structure for table `btl_branch_staff` */

CREATE TABLE `btl_branch_staff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `btl_branch_staff` */

/*Table structure for table `membership_users` */

CREATE TABLE `membership_users` (
  `memberID` varchar(20) NOT NULL,
  `passMD5` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `groupID` int(10) unsigned DEFAULT NULL,
  `isBanned` tinyint(4) DEFAULT NULL,
  `isApproved` tinyint(4) DEFAULT NULL,
  `custom1` text,
  `custom2` text,
  `custom3` text,
  `custom4` text,
  `comments` text,
  `pass_reset_key` varchar(100) DEFAULT NULL,
  `pass_reset_expiry` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`memberID`),
  KEY `groupID` (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `membership_users` */

insert  into `membership_users`(`memberID`,`passMD5`,`email`,`signupDate`,`groupID`,`isBanned`,`isApproved`,`custom1`,`custom2`,`custom3`,`custom4`,`comments`,`pass_reset_key`,`pass_reset_expiry`) values 
('1',NULL,NULL,'2016-02-18',1,0,1,NULL,NULL,NULL,NULL,'Anonymous member created automatically on 2016-02-18',NULL,NULL),
('2','63a9f0ea7bb98050796b649e85481845','ved71714@gmail.com','2016-02-18',2,0,1,NULL,NULL,NULL,NULL,'Admin member created automatically on 2016-02-18',NULL,NULL),
('3','dGVzdA==','test@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tbl_activity` */

CREATE TABLE `tbl_activity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `remark` text,
  `lead_id` int(11) DEFAULT NULL,
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_activity` */

insert  into `tbl_activity`(`ID`,`activity_type`,`status`,`remark`,`lead_id`) values 
(3,'had phone conversation','prospect','aaaaaaa',178),
(4,'sent a email','disqualified','aaaaaaaaaaaaaaabbbbbbbbbbbbbbb',149),
(5,'had phone conversation','disqualified','aaaaaaaaa',178);

/*Table structure for table `tbl_add_task` */

CREATE TABLE `tbl_add_task` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `task_type` varchar(30) DEFAULT NULL,
  `remark` text,
  `assign_task` varchar(30) DEFAULT NULL,
  `schdule_date` varchar(30) DEFAULT NULL,
  `schdule_time` varchar(30) DEFAULT NULL,
  `reminder_before_task` varchar(30) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_add_task` */

insert  into `tbl_add_task`(`ID`,`task_type`,`remark`,`assign_task`,`schdule_date`,`schdule_time`,`reminder_before_task`,`lead_id`) values 
(8,'meeting','hhhhhhhh','hemantrajesh','23/05/2017','17:00 PM','13:00 PM',176),
(9,'follow up','sssssssssss','amol','15/05/2017','12:00 PM','23:00 PM',178),
(10,'site visit','ppppp','testEmp2','23/05/2017','19:00 PM','18:00 PM',178);

/*Table structure for table `tbl_branch` */

CREATE TABLE `tbl_branch` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `contact` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `address` text,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zipcode` varchar(6) DEFAULT NULL,
  `bank_name` varchar(60) DEFAULT NULL,
  `bank_account_no` varchar(35) DEFAULT NULL,
  `bank_ifsc_code` varchar(35) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `landline_no` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_branch` */

insert  into `tbl_branch`(`ID`,`name`,`contact`,`email`,`address`,`city`,`state`,`zipcode`,`bank_name`,`bank_account_no`,`bank_ifsc_code`,`description`,`landline_no`) values 
(1,'test','555555','test@mail.com','null','ciit','state','null','SBI','1111111111','sbin-2222','des',NULL),
(2,'Chinchwad','2065111900','Chinchwad@teest.com','null','Pune','Maharashtra','null','ICICI','22222222222','icici22356','desc',NULL),
(3,'bbn','1122222','nmnm@fffg','null','dfdffds','fdsfsd','null','AXIS','3333333','test',NULL,NULL),
(4,'test','null','test@gmail1.com','null','Pune','Maharashtra','null','null','66666','56566565656','null',NULL),
(5,'5555','5555555555','55555@tttttt.com','555555','Pune','Maharashtra','555555','tttttttt','555555555555555','55555555555','tttttt','55555555555');

/*Table structure for table `tbl_branch_staff` */

CREATE TABLE `tbl_branch_staff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_branch_staff` */

/*Table structure for table `tbl_building` */

CREATE TABLE `tbl_building` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `phase_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_building` */

insert  into `tbl_building`(`id`,`name`,`project_id`,`phase_id`) values 
(1,'testbuilding',NULL,3),
(2,'testBuiildingCT',NULL,4),
(3,'test3rdphasebuilding',NULL,5),
(4,'A',NULL,6),
(5,'4',NULL,7),
(6,'1',NULL,8),
(7,'a',NULL,9),
(8,'sukhwani',NULL,10),
(9,'1',NULL,12),
(10,'a',NULL,12),
(11,'a',NULL,13),
(12,'a',NULL,14),
(13,'1234',NULL,14),
(14,'1',NULL,15),
(15,'klkl',NULL,15),
(16,'building1',NULL,16),
(17,'hhhhh',NULL,17),
(18,'2',NULL,24),
(19,'1',NULL,24),
(20,'2',NULL,24),
(21,'2',NULL,24),
(22,'1',NULL,24),
(23,'1',NULL,24),
(24,'ddd',NULL,38),
(25,'sukhwani',NULL,38),
(26,'sssssss',NULL,38),
(27,'dfsfd',NULL,38),
(28,'dcsc',NULL,38),
(29,'casca',NULL,38),
(30,'casca1111111',NULL,38),
(31,'sukhwani555555555',NULL,38),
(32,'new',NULL,38),
(33,'23',NULL,53),
(34,'2',NULL,53),
(35,'gg',NULL,53),
(36,NULL,NULL,53),
(37,'abc',NULL,53),
(38,'dsd',NULL,53),
(39,'ttaa',NULL,59),
(40,'23',NULL,60),
(41,'1212',NULL,62),
(42,'ffff',NULL,64),
(43,'A',NULL,66),
(44,'Krushna Heights',NULL,67),
(45,'yyyy',NULL,16),
(46,'bbbb',NULL,69),
(47,'aaaa',NULL,68),
(48,'cfgfg',NULL,63);

/*Table structure for table `tbl_building_setup` */

CREATE TABLE `tbl_building_setup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `building_in_phase` int(20) DEFAULT NULL,
  `building_name` varchar(40) DEFAULT NULL,
  `building_no` int(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_building_setup` */

/*Table structure for table `tbl_flat` */

CREATE TABLE `tbl_flat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(10) DEFAULT NULL,
  `wing_id` varchar(20) DEFAULT NULL,
  `carpet_area` decimal(10,0) DEFAULT NULL,
  `builtup_area` decimal(10,0) DEFAULT NULL,
  `superbuiltup_area` decimal(10,0) DEFAULT NULL,
  `BHK` int(11) DEFAULT NULL,
  `use_Type` int(11) DEFAULT NULL,
  `use_type_category` int(11) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_flat` */

insert  into `tbl_flat`(`id`,`number`,`wing_id`,`carpet_area`,`builtup_area`,`superbuiltup_area`,`BHK`,`use_Type`,`use_type_category`,`floor`) values 
(28,'11','3','5000','5000','5000',2,2,4,NULL),
(29,'12','3','0','0','0',1,1,1,NULL),
(30,'13','3','0','0','0',1,1,1,NULL),
(31,'14','3','0','0','0',1,1,1,NULL),
(32,'15','3','0','0','0',1,1,1,NULL),
(33,'21','3','0','0','0',1,1,1,NULL),
(34,'22','3','0','0','0',1,1,1,NULL),
(35,'23','3','0','0','0',1,1,1,NULL),
(36,'24','3','0','0','0',1,1,1,NULL),
(37,'25','3','0','0','0',1,1,1,NULL),
(38,'31','3','0','0','0',1,1,1,NULL),
(39,'32','3','0','0','0',1,1,1,NULL),
(40,'33','3','0','0','0',1,1,1,NULL),
(41,'34','3','0','0','0',1,1,1,NULL),
(42,'35','3','0','0','0',1,1,1,NULL),
(43,'41','3','0','0','0',1,1,1,NULL),
(44,'42','3','0','0','0',1,1,1,NULL),
(45,'43','3','0','0','0',1,1,1,NULL),
(46,'44','3','0','0','0',1,1,1,NULL),
(47,'45','3','0','0','0',1,1,1,NULL),
(48,'51','3','0','0','0',1,1,1,NULL),
(49,'52','3','0','0','0',1,1,1,NULL),
(50,'53','3','0','0','0',1,1,1,NULL),
(51,'54','3','0','0','0',1,1,1,NULL),
(52,'55','3','0','0','0',1,1,1,NULL),
(53,'101','5','2000','2200','2300',2,1,3,NULL),
(54,'102','5','0','0','0',1,1,1,NULL),
(55,'103','5','0','0','0',1,1,1,NULL),
(56,'201','5','0','0','0',1,1,1,NULL),
(57,'202','5','0','0','0',1,1,1,NULL),
(58,'203','5','0','0','0',1,1,1,NULL),
(59,'301','5','0','0','0',1,1,1,NULL),
(60,'302','5','0','0','0',1,1,1,NULL),
(61,'303','5','0','0','0',1,1,1,NULL),
(62,'101','6',NULL,'0','0',1,1,1,NULL),
(63,'102','6',NULL,'0','0',1,1,1,NULL),
(64,'201','6',NULL,'0','0',1,1,1,NULL),
(65,'202','6',NULL,'0','0',1,1,1,NULL),
(66,'101','7','832','1313','0',3,1,0,NULL),
(67,'102','7','832','1313','0',3,1,1,NULL),
(68,'103','7',NULL,'0','0',1,1,1,NULL),
(69,'104','7',NULL,'0','0',1,1,1,NULL),
(70,'105','7',NULL,'0','0',1,1,1,NULL),
(71,'201','7',NULL,'0','0',1,1,1,NULL),
(72,'202','7',NULL,'0','0',1,1,1,NULL),
(73,'203','7',NULL,'0','0',1,1,1,NULL),
(74,'204','7',NULL,'0','0',1,1,1,NULL),
(75,'205','7',NULL,'0','0',1,1,1,NULL),
(76,'301','7',NULL,'0','0',1,1,1,NULL),
(77,'302','7',NULL,'0','0',1,1,1,NULL),
(78,'303','7',NULL,'0','0',1,1,1,NULL),
(79,'304','7',NULL,'0','0',1,1,1,NULL),
(80,'305','7',NULL,'0','0',1,1,1,NULL),
(81,'401','7',NULL,'0','0',1,1,1,NULL),
(82,'402','7',NULL,'0','0',1,1,1,NULL),
(83,'403','7',NULL,'0','0',1,1,1,NULL),
(84,'404','7',NULL,'0','0',1,1,1,NULL),
(85,'405','7',NULL,'0','0',1,1,1,NULL),
(86,'501','7',NULL,'0','0',1,1,1,NULL),
(87,'502','7',NULL,'0','0',1,1,1,NULL),
(88,'503','7',NULL,'0','0',1,1,1,NULL),
(89,'504','7',NULL,'0','0',1,1,1,NULL),
(90,'505','7',NULL,'0','0',1,1,1,NULL),
(91,'403','15','2','2','2',2,NULL,2,NULL),
(92,'403','15','2','2','2',2,NULL,2,NULL),
(93,'403','15','2','2','2',2,NULL,2,NULL),
(94,'403','15','2','2','2',2,NULL,2,NULL),
(95,'403','15','2','2','2',2,NULL,2,NULL),
(96,'403','15','2','2','2',2,NULL,2,NULL),
(97,'403','15','2','2','2',2,NULL,2,NULL),
(98,'403','15','2','2','2',2,NULL,2,NULL),
(99,'403','15','2','2','2',2,NULL,2,NULL),
(100,'403','15','2','2','2',2,NULL,2,NULL),
(101,'403','15','2','2','2',2,NULL,2,NULL),
(102,'403','15','2','2','2',2,NULL,2,NULL),
(103,'101','15','2','2','2',2,NULL,2,NULL),
(104,'102','15','2','2','2',2,NULL,2,NULL),
(105,'103','15','2','2','2',2,NULL,2,NULL),
(106,'201','15','2','2','2',2,NULL,2,NULL),
(107,'202','15','2','2','2',2,NULL,2,NULL),
(108,'203','15','2','2','2',2,NULL,2,NULL),
(109,'301','15','2','2','2',2,NULL,2,NULL),
(110,'302','15','2','2','2',2,NULL,2,NULL),
(111,'303','15','2','2','2',2,NULL,2,NULL),
(112,'401','15','2','2','2',2,NULL,2,NULL),
(113,'402','15','2','2','2',2,NULL,2,NULL),
(114,'403','15','2','2','2',2,NULL,2,NULL),
(115,'101','15','5','5','5',5,NULL,5,NULL),
(116,'102','15','5','5','5',5,NULL,5,NULL),
(117,'103','15','5','5','5',5,NULL,5,NULL),
(118,'201','15','5','5','5',5,NULL,5,NULL),
(119,'202','15','5','5','5',5,NULL,5,NULL),
(120,'203','15','5','5','5',5,NULL,5,NULL),
(121,'301','15','5','5','5',5,NULL,5,NULL),
(122,'302','15','5','5','5',5,NULL,5,NULL),
(123,'303','15','5','5','5',5,NULL,5,NULL),
(124,'401','15','5','5','5',5,NULL,5,NULL),
(125,'402','15','5','5','5',5,NULL,5,NULL),
(126,'403','15','5','5','5',5,NULL,5,NULL),
(127,'101','85','90','222','2222',222,1,1,NULL),
(128,'102','85','90','222','2222',222,1,1,NULL),
(129,'103','85','90','222','2222',222,1,1,NULL),
(130,'104','85','90','222','2222',222,1,1,NULL),
(131,'105','85','90','222','2222',222,1,1,NULL),
(132,'201','85','90','222','2222',222,1,1,NULL),
(133,'202','85','90','222','2222',222,1,1,NULL),
(134,'203','85','90','222','2222',222,1,1,NULL),
(135,'204','85','90','222','2222',222,1,1,NULL),
(136,'205','85','90','222','2222',222,1,1,NULL),
(137,'301','85','90','222','2222',222,1,1,NULL),
(138,'302','85','90','222','2222',222,1,1,NULL),
(139,'303','85','90','222','2222',222,1,1,NULL),
(140,'304','85','90','222','2222',222,1,1,NULL),
(141,'305','85','90','222','2222',222,1,1,NULL),
(142,'401','85','90','222','2222',222,1,1,NULL),
(143,'402','85','90','222','2222',222,1,1,NULL),
(144,'403','85','90','222','2222',222,1,1,NULL),
(145,'404','85','90','222','2222',222,1,1,NULL),
(146,'405','85','90','222','2222',222,1,1,NULL),
(147,'501','85','90','222','2222',222,1,1,NULL),
(148,'502','85','90','222','2222',222,1,1,NULL),
(149,'503','85','90','222','2222',222,1,1,NULL),
(150,'504','85','90','222','2222',222,1,1,NULL),
(151,'505','85','90','222','2222',222,1,1,NULL),
(152,'87','1','101','90','55',5,8,1,1),
(153,'87','1','102','90','55',5,8,1,1),
(154,'87','2','201','90','55',5,8,1,1),
(155,'87','2','202','90','55',5,8,1,1),
(156,'87','3','301','90','55',5,8,1,1),
(157,'87','3','302','90','55',5,8,1,1),
(158,'87','1','101','90','5555',5,555,1,55),
(159,'87','1','102','90','5555',5,555,1,55),
(160,'87','2','201','90','5555',5,555,1,55),
(161,'87','2','202','90','5555',5,555,1,55),
(162,'87','3','301','90','5555',5,555,1,55),
(163,'87','3','302','90','5555',5,555,1,55),
(164,'87','1','101','90','5555',5,555,1,55),
(165,'87','1','102','90','5555',5,555,1,55),
(166,'87','2','201','90','5555',5,555,1,55),
(167,'87','2','202','90','5555',5,555,1,55),
(168,'87','3','301','90','5555',5,555,1,55),
(169,'87','3','302','90','5555',5,555,1,55),
(170,'101','87','90','66','6',6,1,1,1),
(171,'102','87','90','66','6',6,1,1,1),
(172,'201','87','90','66','6',6,1,1,2),
(173,'202','87','90','66','6',6,1,1,2),
(174,'301','87','90','66','6',6,1,1,3),
(175,'302','87','90','66','6',6,1,1,3),
(176,'101','88','90','500','500',666,1,1,1),
(177,'102','88','90','500','500',666,1,1,1),
(178,'103','88','90','500','500',666,1,1,1),
(179,'201','88','90','500','500',666,1,1,2),
(180,'202','88','90','500','500',666,1,1,2),
(181,'203','88','90','500','500',666,1,1,2),
(182,'301','88','90','500','500',666,1,1,3),
(183,'302','88','90','500','500',666,1,1,3),
(184,'303','88','90','500','500',666,1,1,3),
(185,'401','88','90','500','500',666,1,1,4),
(186,'402','88','90','500','500',666,1,1,4),
(187,'403','88','90','500','500',666,1,1,4),
(188,'501','88','90','500','500',666,1,1,5),
(189,'502','88','90','500','500',666,1,1,5),
(190,'503','88','90','500','500',666,1,1,5),
(191,'101','89','9','8','8',8,1,3,1),
(192,'102','89','9','8','8',8,1,3,1),
(193,'201','89','9','8','8',8,1,3,2),
(194,'202','89','9','8','8',8,1,3,2),
(195,'301','89','9','8','8',8,1,3,3),
(196,'302','89','9','8','8',8,1,3,3),
(197,'401','89','9','8','8',8,1,3,4),
(198,'402','89','9','8','8',8,1,3,4);

/*Table structure for table `tbl_flat_setup` */

CREATE TABLE `tbl_flat_setup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `flat_number` int(20) DEFAULT NULL,
  `carpet_area` int(20) DEFAULT NULL,
  `built_up` int(20) DEFAULT NULL,
  `super_built_up` int(20) DEFAULT NULL,
  `bhk` varchar(30) DEFAULT NULL,
  `use_type` varchar(30) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_flat_setup` */

/*Table structure for table `tbl_floor_setup` */

CREATE TABLE `tbl_floor_setup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Floor` int(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_floor_setup` */

/*Table structure for table `tbl_follow_up` */

CREATE TABLE `tbl_follow_up` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `lead_id` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `next_followup_date` date DEFAULT NULL,
  `Details` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_follow_up` */

/*Table structure for table `tbl_groundunit` */

CREATE TABLE `tbl_groundunit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phase_id` varchar(20) DEFAULT NULL,
  `carpet_area` decimal(10,0) DEFAULT NULL,
  `builtup_area` decimal(10,0) DEFAULT NULL,
  `superbuiltup_area` decimal(10,0) DEFAULT NULL,
  `BHK` int(11) DEFAULT NULL,
  `use_Type` int(11) DEFAULT NULL,
  `use_type_category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_groundunit` */

insert  into `tbl_groundunit`(`id`,`phase_id`,`carpet_area`,`builtup_area`,`superbuiltup_area`,`BHK`,`use_Type`,`use_type_category`) values 
(1,'20','2','2','2',2,1,2),
(2,'20','2','2','2',2,1,2),
(3,'21','5','5','5',5,1,5),
(4,'21','5','5','5',5,1,5),
(5,'21','5','5','5',5,1,5),
(6,'21','5','5','5',5,1,5),
(7,'21','5','5','5',5,1,5);

/*Table structure for table `tbl_lead` */

CREATE TABLE `tbl_lead` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(50) DEFAULT NULL,
  `reference` varchar(30) DEFAULT NULL,
  `contact_number` varchar(10) DEFAULT NULL,
  `remarks` text,
  `lead_type` varchar(15) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priority` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `req_start_date` varchar(30) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `property_type` varchar(20) DEFAULT NULL,
  `property_type_detail` varchar(50) DEFAULT NULL,
  `max_rent` int(11) DEFAULT NULL,
  `max_deposit` int(11) DEFAULT NULL,
  `min_deposit` int(11) DEFAULT NULL,
  `min_rent` int(11) DEFAULT NULL,
  `landline_no` varchar(15) DEFAULT NULL,
  `assign_staff_id` int(15) DEFAULT NULL,
  `furnished_type` varchar(45) DEFAULT NULL,
  `details` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead` */

insert  into `tbl_lead`(`ID`,`person_name`,`reference`,`contact_number`,`remarks`,`lead_type`,`status`,`date`,`priority`,`email`,`req_start_date`,`location`,`property_type`,`property_type_detail`,`max_rent`,`max_deposit`,`min_deposit`,`min_rent`,`landline_no`,`assign_staff_id`,`furnished_type`,`details`) values 
(16,'kiran K','www.99acres.com','7028232601','750 Sq Ft. for Bachlors & Family','owner','pending','2016-09-07 22:37:54',NULL,'KK@gmail.com','2016-06-09','WAKAD','residential','1BHK',NULL,NULL,50000,10000,NULL,3,NULL,NULL),
(17,'Kailas','www.99acres.com','8237744695','Building name- Bhayal House/ Add- Dangechowk To Marunji Road Main Road Opp Air Castles / Key-Kailas / Semi-Furnished ( 3 Bed/Curtin/Fan/Light/Kabt/) 2 Wheeler Parking Only / Balconies- not / Propertity On Floor-6 (No Of Floor- 1/3) / Bachlor Allowed Only Girls / Boys / Sqft - 660','owner','pending','2016-06-23 19:40:27',NULL,'Kailaschoudhary21@gmail.com','2016-06-09','MARUNJI','residential','1BHK',NULL,NULL,30000,10000,NULL,NULL,NULL,NULL),
(18,'Kailas','www.99acres.com','8237744695','Buliding Name- Bhayal House / Add- Dangechowk To Marunji Road Main Road Opp Air Castles /Balconies- Not / Parking - 2 Wheeler Parking / Keys With - Kailas / Properity On Floor - 6 ( No Of Floor - 1/3 ) / Bachlor Allowed Only / Sqft - 660 / Main Door Facing- North / Semi Furnished ( Fan, Light, 3 Bed, Curtin, Kabt ) Visiting Time -9 Am  To  9 Pm','owner','pending','2016-06-23 19:40:27',NULL,'Kailaschoudhary21@gmail.com','2016-06-09','MARUNJI','residential','1BHK',NULL,NULL,30000,10000,NULL,NULL,NULL,NULL),
(19,'Namrata','www.justdial.com','9850902140','Building Name- Mithila Home / Add- dangechowk to Ravet Breach  Cross Right Bodanve Chowk Left Wrong Side First Right First Left / Main Door Facing - east / Keys With - Namarta /  Flat Status - Unfurnished / Lift Available  / Property On Floor - 3 ( No Of Floor - 2 ) / Family Allowed Only / How Old Buliding - 5 Years / Flat No - Wing B - 206','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-07','RAVET','residential','1BHK',NULL,NULL,30000,10000,NULL,NULL,NULL,NULL),
(20,'Mrunalini','www.justdial.com','8554093003','Building Name - Life Republic / Flat No - Sector R-7 D Wing ,707 / Add- Dangechowk  To Bhumkarchowk Agey se Right Dead end Wapas Right / Keys With - Mrunalini / Furnished / Lift - Avaliable / Properity Of Floor -  22 ( No Of Floor - 7 ) Both Akllowed / And 3 Bedroom / 3 Bathroom / 2 Balconies','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-01','MARUNJI','residential','3BHK',NULL,NULL,68000,17000,NULL,NULL,NULL,NULL),
(21,'Mane','www.justdial.com','9579122771','Building Name - Xrbia Developers / Flat No - A-7 / Add- Dangechowk To Bhumkarchowk to Marunji Road Right mai International Management Collge  Side mai xrbia developers / Keys With - Mane / Unfurnished / 215 Sqft / Lift Avavalible / Properity On Floor - 7 ( No Of Floor - 3 ) Both Avaliable / New Buliding','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-04','MARUNJI','residential',' 1RK',NULL,NULL,20000,6000,NULL,NULL,NULL,NULL),
(22,'poonam','www.justdial.com','8856849967','Building name- Sundhar bhag  / Dangechowk to chaferkar Chowk se darsan hall se thoda agey Domonlds pizza Near / Keys With - poonam / Flat No - C Buliding B Wing 18 / Lift Available / Property On Floor - 6 ( No Of Floor - 5 )   / How Old Bldg- 10 / Family Allowed Only','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-07-01','Chinchwad','residential','1BHK',NULL,NULL,45000,10500,NULL,NULL,NULL,NULL),
(23,'Sumit','www.justdial.com','9765212126','Bldg Name- Divini Home / Flat No - 607 / Add- Dangechowk To Akurdi Side Hanging Breach / Keys With - Sumit / Lift Available / Propertity On Floor - 8 ( No Of Floor - 6 ) / Both allowed /  How Old Bldg - New','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-06','Akurdi','residential','2BHK',NULL,NULL,30000,14000,NULL,NULL,NULL,NULL),
(24,'Swapnil','www.justdial.com','8408971510','Bldg Name - Girja Society / Sale Krna Hai 75 lakh mai  / Add- Dangechowk  to Bhumkarchowk Right To Highway Sankiti Hotel baju Mai / Flat No - 401 / Keys With - Swapnil / 16.20Sqft / Lift Available / Properity On Floor - 5 ( No Of Floor - 4 ) / How Old Bldg -6','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-09','BHUMKAR CHOWK','residential','3BHK',NULL,NULL,NULL,7500000,NULL,NULL,NULL,NULL),
(25,'Hemant','www.justdial.com','9881740388','Bldg Name- Pragati / Flat Name - B-102 / Add -Dangechowk To Dy Patil Ke Baju Mai Ravet / Keys With - Hemant / Unfurnished / Properity On Floor - 3 ( No Of Floor -1 ) / Lift Available / How Old Bldg - 1 Year / Family Available','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-04','RAVET','residential','2BHK',NULL,NULL,50000,15000,NULL,NULL,NULL,NULL),
(26,'Jayaram','www.justdial.com','9561516129','Building Name - Indra Pasta Building / Flat No - Tereces3 / Add- Dangechowk To Kalewadi To  Tapkir Chowk / Family Allowed Only / How Old Bldg - 5 Year','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-25','THERGAON','residential',' 1RK',NULL,NULL,7500,2500,NULL,NULL,NULL,NULL),
(27,'Sheetal','www.justdial.com','9422341772','Buliding Name - Tower 2 C Wing Kulecoloch Society / Add- Dangechowk To Balewadi Statdium Behind Hinjewadi Phase 1 Tree  House School / Keys With - Sheetal & Security / Properity On Floor - 7 ( No Of Floor- 6 ) / How Old Bldg - New / Family Allowed','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-25','BALEWADI','residential','1BHK',NULL,NULL,25000,8000,NULL,NULL,NULL,NULL),
(28,'Sardul','www.justdial.com','9423321567','Bldg Name- Bhagwati Royal / Flat No - B-305 / Add- Dangechowk  To 16.no Mc Donalds Ke Baju Mai  / Keys With - Sardul / Semi Furnished / Properity On Floor - 8( No Of Floor - 3 ) / How Old Bldg - 3 / Family Allowed Only / Lift Available','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-26','DANGE CHOWK','residential','2BHK',NULL,NULL,50000,16000,NULL,NULL,NULL,NULL),
(29,'Tushar','www.justdial.com','9920279077','Bldg Name- Nanda Society / Flat No - 9 B Wing / Add-  Dangechowk Se ChaffekaR Chowk Se SKF Company Ke Agey Prem Log Park / Key With- Tushar / Family Allowed/  No Of Floor - 2','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-26','Chinchwad','residential','1BHK',NULL,NULL,45000,9000,NULL,NULL,NULL,NULL),
(30,'Sagar','www.justdial.com','9833214330','Bldg Name - Element 5 Coperative Housing Society / Flat No - A- 504 / Add-  Dangechowk To Shivr Garden Hotel Opp. Sunshine Jilla Shivr Chowk  Near Jattadiary Pimple','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-26','PIMPLE SAUDAGAR','residential','3BHK',NULL,NULL,125000,33000,NULL,NULL,NULL,NULL),
(31,'vishal','www.justdial.com','8888893001','Bldg Name -  Royal Casa /  Flat No - B-702 / Dangechowk To Ravet/ Unfurnished / No Of Floor - 8 /  How Old  Bldg - 4 Year/ Family Allowed','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-25','RAVET','residential','2BHK',NULL,NULL,40000,15000,NULL,NULL,NULL,NULL),
(32,'Prashant','www.justdial.com','9923930646','Bldg Name- Kalptru Enclabe / Flat No- A2-402 / Add- Dangechowk To Aundh / Landmark - Kaka Halwai / Keys With- Prashant / Lift avaliable / No Of Floor- 4 / How Old Bldg- 12 Year / Family Allowed','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-25','Aundh','residential','3BHK',NULL,NULL,500000,75000,NULL,NULL,NULL,NULL),
(33,'Pankaj','www.justdial.com','8879688052','Bldg Name - Venkathshe / Flat no -  C- 703 / Add - Dangechowk To Indira Collge National highway Behind HP size service Station Bhumkarchowk / Lift Available / Sale Krna Hai Flat 55 Lakh / Property On Floor - 7 ( No Of Floor 7 ) / Both Allowed / How Old Bldg - New','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-26','BHUMKAR CHOWK','residential','2BHK',NULL,NULL,NULL,55,NULL,NULL,NULL,NULL),
(34,'Surekha','www.justdial.com','8855917092','Sale krna hai Flat 38 lakh mai  / bldg Name - Bhagwati Royal B- Wing / 625 Sqft / Keys with - Security / Lift Available / How Old Bldg - 3 Year / Properity On Floor - 8 ( No Of Floor - 7 ) Both allowed','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-26','DANGE CHOWK','residential','1BHK',NULL,NULL,NULL,38,NULL,NULL,NULL,NULL),
(35,'Somesh','www.justdial.com','9881060136','Sale Krna hai  flat  38 lakh / Bldg Name - sonigraha park / Add - dangechowk To Sonigraha Park  C/ 602 / Lift Available / Properity On Floor - 7 ( No Of Floor 6 ) Both Allowed','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-05-31','DANGE CHOWK','residential','1BHK',NULL,NULL,-1,3800000,NULL,NULL,NULL,NULL),
(36,'Praveen','www.99acres.com','8177888587','1bhk chahiye dangehowk mai khud call krege agar chahiye hoga toh','tenant','pending','2016-06-23 19:40:27',NULL,'','2016-06-11','DANGE CHOWK','residential','1BHK',NULL,NULL,20000,10000,NULL,NULL,NULL,NULL),
(37,'Deepak','www.justdial.com','9579106721','1rk chahiye  wakad mai 2 lady ke liye Budget 6000  d- 14000','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','WAKAD','residential',' 1RK',NULL,NULL,14000,6000,NULL,NULL,NULL,NULL),
(38,'Sidhart','www.99acres.com','7768075050','1bhk chahiye dangechowk mai 2 bachlor ka kal call krna hai / khud call krege friendko puch ke11/6/16','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-06','DANGE CHOWK','residential','1BHK',NULL,NULL,20000,10000,NULL,NULL,NULL,NULL),
(39,'meghraj','www.99acres.com','9921341946','new sangvi 1rk chahiye Family  ke liye Budget 5500','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-06','New sangvi',NULL,NULL,NULL,NULL,15000,5500,NULL,NULL,NULL,NULL),
(40,'Satish','www.justdial.com','9096827007','1bhk chahiye 2 bachlor hai job pimple saudagar b-9000/10/6/16 aaj visit ke liye aa rhe','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-10','WAKAD','residential','1BHK',NULL,NULL,20000,9000,NULL,NULL,NULL,NULL),
(41,'Rahul','www.99acres.com','9657942493','2bhk chahiye wakad mai 4 bachlor hai/ thode der mai call krna hai11/6/16','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','DANGE CHOWK',NULL,NULL,NULL,NULL,NULL,15000,NULL,NULL,NULL,NULL),
(42,'juber','www.justdial.com','9049639494','1bhk chahiye family ke liye chkan mai job hai/ kal aa rhe10/6/15/5 bje call krna hai 11/6/16','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','DANGE CHOWK','residential','1BHK',NULL,NULL,20000,10000,NULL,NULL,NULL,NULL),
(43,'Sampdhaa','www.99acres.com','9820347572','1bhk chahiye family bhi bachlor bhi aur dangechowk wala dikhna hai/10/6/16','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-10','DANGE CHOWK','residential','1BHK',NULL,NULL,20000,10000,NULL,NULL,NULL,NULL),
(44,'Vijay','www.99acres.com','9004455905','1bhk chahiye Dangechowk mai 10000 budget khud call krege/ chahiye hoga toh khud call krege','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-10','DANGE CHOWK','residential','1BHK',NULL,NULL,20000,10000,NULL,NULL,NULL,NULL),
(45,'Ashis','www.justdial.com','7045414787','1rk chahiye marunji job hinjewadi 3 bachlor b-6000 10/6 kal call krna hai','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-10','DANGE CHOWK','residential','1BHK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(46,'Ameer','www.99acres.com','8446380318','1rk chahiye family hai budget 6000 dangechowk mai','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','DANGE CHOWK','residential',' 1RK',NULL,NULL,12000,6000,NULL,NULL,NULL,NULL),
(47,'Triputi','www.99acres.com','9021243298','1rk chahiye kuthur mai 3 bachlor ladies b-7000','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','VISHAL NAGAR','residential',' 1RK',NULL,NULL,12000,7000,NULL,NULL,NULL,NULL),
(48,'Rakesh','www.99acres.com','9766294312','1bhk chahiye kalewadi mai ya chinchwd mai ya dangechowk mai family hai 9500  budget but unhe busstop najdik chahiye mon ko aa rhe visit krne dangechowk ka flat but unhe sonigraha park nhi chahiye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','DANGE CHOWK,Chinchwad','residential','1BHK',NULL,NULL,NULL,9500,NULL,NULL,NULL,NULL),
(49,'Ravi','www.99acres.com','8177888587','1rk /1 bhk bhi chlega Family hai  but Without brokerage chahiye','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','DANGE CHOWK','residential',' 1RK',NULL,NULL,NULL,10000,NULL,NULL,NULL,NULL),
(50,'Pooja','www.justdial.com','8600111483','1bhk Chahiye kalyani  nagar 3 ladkiya haii budget 13000','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','PIMPLE SAUDAGAR,NIGADI','residential','1BHK',NULL,NULL,20000,13000,NULL,NULL,NULL,NULL),
(51,'Gaurav','www.justdial.com','9665004794','4 bachlor hai chinchwd mai 1 bhk chahiye rent budget 8000','tenant','pending','2016-06-23 19:40:27',NULL,'gauravchaudhari1307@gmail.com','2016-06-11','Chinchwad','residential','1BHK',NULL,NULL,15000,8000,NULL,NULL,NULL,NULL),
(52,'Chiranjeet','www.justdial.com','9922434803','Bulding Name- Swarna Ayoehya /  Flat no- 203 /  Add-  Dangechowk to Ganpati Mandir Se Andar ki Traf Manisi hotel First Left Anpurana Hotel Ushse Agey Second right Dead End / Keys with - owner /Property on floor -G+2 ( No of Floor-1) / 900 saqt / 2/4 wheeler Parking / main door facing - north / Both allowed','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-10','DANGE CHOWK','residential','2BHK',NULL,NULL,30000,14000,NULL,NULL,NULL,NULL),
(53,'Shubhmkar','www.justdial.com','9673491472','1bhk chahiye dattamandir ke waha ka visit krayege sir /3 Bachlor Hinjewadi job','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-11','DANGE CHOWK','residential','1BHK',NULL,NULL,20000,90000,NULL,NULL,NULL,NULL),
(54,'Sumant','www.99acres.com','9686973437','1rk chahiye wakad mai family hai but abhi alkela rhega 7000 budget','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','WAKAD','residential',' 1RK',NULL,NULL,15000,7000,NULL,NULL,NULL,NULL),
(55,'Shivni','www.99acres.com','8975761370','Ravet mai 1bhk chahiye family hai 13000 budget kiran sir se puchna hai','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','RAVET','residential','1BHK',NULL,NULL,25000,13000,NULL,NULL,NULL,NULL),
(56,'Ankit','www.99acres.com','8378986782','1bhk / 2bhk chahiye bachlor ke liye b-15000 fully furnished chahiye maine bataya dangechowk ka flat fully furnished 13500 jyada laga soch ke batayege','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','DANGE CHOWK','residential','1BHK',NULL,NULL,20000,15000,NULL,NULL,NULL,NULL),
(57,'Abhijeet','www.justdial.com','7588612869','1bhk chahiye chaffer chowk ke waha pe family hai b-9000 d-25000-30000','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','Chinchwad','residential','1BHK',NULL,NULL,30000,9000,NULL,NULL,NULL,NULL),
(58,'Shinde',NULL,'8698836616','1rk chahiye wakad mai 3 bachlor ke liye b-3000-4000 tk aur without brokerge chahiye','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','WAKAD','residential',' 1RK',NULL,NULL,10000,4000,NULL,NULL,NULL,NULL),
(59,'juber','www.99acres.com','9820347572','1rk  wakad mai chahiye 1 lady rhegi but kabhi kabhi family aayegi /12/6/16','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','WAKAD','residential',' 1RK',NULL,NULL,15000,7000,NULL,NULL,NULL,NULL),
(60,'Brejas','www.justdial.com','8511859845','Walking lead hai unhe dangechowk ka flat dekhna hai duggal sir ke admi unhe flat dikhyege sham tk','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','DANGE CHOWK','residential','1BHK',NULL,NULL,25000,10000,NULL,NULL,NULL,NULL),
(61,'Sachin',NULL,'9226104956','nhi chahiye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-12','DANGE CHOWK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(62,'Rajini',NULL,'8879454792','Viman nagar mai purchase  krna hai flat  2bhk sb include chahiye  tax , register','tenant','pending','2016-06-23 19:40:27',NULL,'kakadehb@bharatpetroleum.in','2016-06-13','HINJEWADI','residential','2BHK',NULL,NULL,NULL,600000,NULL,NULL,NULL,NULL),
(63,'Ravi','www.justdial.com','7387744654','1bhk chahiye kalewadi , ratni mai family hai b-7000 dangechowk bhi chlega but 7000 mai chahiye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-13','DANGE CHOWK','residential','1BHK',NULL,NULL,NULL,7000,NULL,NULL,NULL,NULL),
(64,'Bhushan','www.99acres.com','9049221436','1Bhk Chahiye chinhwd mai 5 bachlor chinchwd mai he job hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-13','Chinchwad','residential','1BHK',NULL,NULL,NULL,10500,NULL,NULL,NULL,NULL),
(65,'viyaj','www.justdial.com','8483873779','chinchwd mai 1rk chahiye 2 bachlor hai study krte hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-13','Chinchwad','residential',' 1RK',NULL,NULL,NULL,5000,NULL,NULL,NULL,NULL),
(66,'shital','www.99acres.com','7774079079','4000 Mai 1rk chahiye single lady hai hinjewadi job','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-13','DANGE CHOWK','residential',' 1RK',NULL,NULL,NULL,4000,NULL,NULL,NULL,NULL),
(67,'Kiran Katkar','www.99acres.com','7387545354','jadfsfsafk;dslff lllllllllllllllllllllllllllllllllllllllmmm            mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk','owner','pending','2016-06-23 19:40:27',NULL,'kirank@gmail.com','2016-06-13','WAKAD','residential','1BHK',15000,NULL,50000,10000,NULL,NULL,'SemiFurnished',NULL),
(68,'Pallavi','www.99acres.com','8177851714','1bhk chahiye husband wife ke liye 8000 tk chinchwd mai','owner','pending','2016-06-23 19:40:27',NULL,'pallavigurav23@gmail.com','2016-06-13','Chinchwad','residential','1BHK',8000,NULL,NULL,7000,NULL,NULL,'Unfurnished',NULL),
(69,'Tanvi','www.justdial.com','7506182294','2bhk chahiye fullyfurnished single rhegi pimple saudagar y chinchwd side chahiye','tenant','pending','2016-06-23 19:40:27',NULL,'tanvisharma.eee@gmail.com','2016-06-13','PIMPLE SAUDAGAR','residential','2BHK',25000,NULL,NULL,20000,NULL,NULL,'Furnished',NULL),
(70,'nitin','www.99acres.com','9820587702','2bhk chahiye baner ,Kalewadi mai aur balewadi  Family hai Fully furnished chahiye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-13','BALEWADI','residential','2BHK',NULL,NULL,NULL,18000,NULL,NULL,'Furnished',NULL),
(71,'Krishna','www.justdial.com','9673482075','1rk chahiye chinchwd mai but wakad ka 1rk dekhne aayege kal','tenant','pending','2016-06-23 19:40:27',NULL,'dasakekrishna6@gmail.com','2016-06-13','Chinchwad','residential',' 1RK',8000,NULL,NULL,7000,NULL,NULL,'Unfurnished',NULL),
(72,'Amol Ghare','www.99acres.com','7028232602','Bacholrs 3 Persons','tenant','pending','2016-06-23 19:40:27',NULL,'amolg@gmail.com','2016-06-13','WAKAD,HINJEWADI,NIGADI,MARUNJI,TATHAWADE','residential','1BHK',15000,NULL,20000,10000,NULL,NULL,'Unfurnished',NULL),
(73,'Pooja','www.99acres.com','9763717034','1bhk chahiye chinchwd mai family hai 6000 budget','tenant','pending','2016-06-23 19:40:27',NULL,'pooja2137@gmail.com','2016-06-14','Chinchwad','residential','1BHK',6000,NULL,NULL,5000,NULL,NULL,'Unfurnished',NULL),
(74,'Rehaan','www.justdial.com','7276939969','2bhk chahiye kaspate wasti, vishal nagar, wakad dattamandir road family hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-14','WAKAD,VISHAL NAGAR','residential','2BHK',16000,NULL,40000,15000,NULL,NULL,'Unfurnished',NULL),
(75,'ratnprabha Tealng','www.justdial.com','8888400999','unhe shop ko rent mai lagana hai /  Buliding name - Ratnarekha buidind/ Flat No 2/3 / Flat available- 1/8/16 / Propertity On Floor -3 ( No Of Floor - 2/3 ) How Old building- 13 / Keys With- Owner / Lift available / 1,500 Build area / 1,300 Carpet area / 2 Wheeler parking / and  4 wheeler parking bridge ke pass / Add- Dangechowk to Chafferchowk opp PNG Jewellers Ke samne vaman hari pethe jewellry','owner','pending','2016-06-23 19:40:27',NULL,'','2016-06-14','Chinchwad','commercial','Commercial Office Space',NULL,NULL,420000,70000,NULL,NULL,'Unfurnished',NULL),
(76,'sunil','www.99acres.com','9511903076','1rk chahiye family hai nigidi proper dangechowk mai chahiye 5000 budget job nigidi','tenant','pending','2016-06-23 19:40:27',NULL,'mishra.sunil206@gmail.com','2016-06-14','DANGE CHOWK,NIGADI','residential',' 1RK',6000,NULL,10000,5000,NULL,NULL,'Unfurnished',NULL),
(77,'Moukashi',NULL,'8308451537','shiv colony mai 1 rk hai aab book ho chuka','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-14','WAKAD','residential',' 1RK',NULL,NULL,20000,5500,NULL,NULL,'Unfurnished',NULL),
(78,'Samir','www.justdial.com','9167573888','1bhk chahiye ravet ke  dmart ke pass family hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-14','RAVET','residential','1BHK',7000,NULL,20000,6000,NULL,NULL,'Unfurnished',NULL),
(79,'meghna','www.justdial.com','9921494250','Pradikaran mai 1bhk chahimily hai aur job bhi pradikaran mai he hai b-7000','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-14','Akurdi','residential','1BHK',7000,NULL,25000,6000,NULL,NULL,'Unfurnished',NULL),
(80,'Priyanka','www.99acres.com','8180047057','1bhk chahiye  Hinjewadi mai 7000 Budget Family Hai Job Hinjewadi','tenant','pending','2016-06-23 19:40:27',NULL,'priyamalve28@gmail.com','2016-06-14','HINJEWADI','residential','1BHK',7500,NULL,15000,7000,NULL,NULL,'Unfurnished',NULL),
(81,'Snehal','www.justdial.com','8855080117','1rk chahiye family hai b-5000','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-14','DANGE CHOWK','residential',' 1RK',NULL,NULL,15000,5000,NULL,NULL,NULL,NULL),
(82,'Jyoti','www.99acres.com','9167374704','1bhk chahiye  ravet , dangechowk ya wakad mai budget 7500 family hai','tenant','pending','2016-06-23 19:40:27',NULL,'jyotid@gmail.com','2016-06-16','WAKAD,DANGE CHOWK,RAVET','residential','1BHK',7500,NULL,15000,7000,NULL,NULL,'Unfurnished',NULL),
(83,'Pruthviraj','www.justdial.com','9822981619','Single room chahiye Ravet Akurdi Mai  Dy Patil Collage ke pass single he rhege','tenant','pending','2016-06-23 19:40:27',NULL,'pruthviraj@gmail.com','2016-06-16','RAVET','residential',' 1RK',3500,NULL,7000,3000,NULL,NULL,'Unfurnished',NULL),
(84,'miss varma',NULL,'9850058677','1Bhk rent mai lagana hai fully furnished kalewadi mai / Building name-Danraj Park / Flat No - I Block Flat-3 / Propertity On Floor -3 ( No Of Floor- 1 ) How Old Blg-10 Years / Avaliable For Family Only / Parking- Common Parking ( 2 Wheeler Charge - 100 / 4 Wheeler Charge - 200)  Furnished - Bed/ Cubt/ fan/frige/ tv/ sofa/gas/light / 560 Sqft','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-16','DANGE CHOWK','residential','1BHK',16000,NULL,50000,15000,NULL,NULL,'Furnished',NULL),
(85,'Chetan','www.justdial.com','9765817638','1Bhk chahiye Ranjan gao pune mai bachlor hai 4 Job  pune','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-16','VISHAL NAGAR','residential','1BHK',8000,NULL,15000,7000,NULL,NULL,'Unfurnished',NULL),
(86,'Satish','www.justdial.com','7875069863','1bhk chahiye dangechowk mai  5 to 6 bachlor hai budget 8000','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-16','DANGE CHOWK','residential','1BHK',8000,NULL,15000,7000,NULL,NULL,'Unfurnished',NULL),
(87,'Sandeep','www.justdial.com','9075099117','1rk chahiye wakad mai  family hai baner mai job krte hai 1 july s4e possession chahiye unhe aur acchi society mai chahiye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-16','WAKAD','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(88,'Sandeep','www.justdial.com','9075099117','1bhk ke rent mai dena hai wakad road mai / Blg name- Nisrag Gandh / flat no-1 / Add- Dngechowk to wakad road poniris hospital ke piche /  Avaliable From- 1 july 2016 / Keys with - sandeep / Properity on floor - 5 ( No Of floor 1 ) Parking - Open Parking 2/4 Wheeler / 550 Sqft / Avaliable For Family','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-07-01','WAKAD','residential','1BHK',NULL,NULL,40000,11500,NULL,NULL,'Unfurnished',NULL),
(89,'Rohan','www.justdial.com','7024120011','1bhk / 2 bhk chahiye fully furnished wakad ,hinjewadi ,dangechowk mai bachlor hai job hinjewadi phase 2 mai krta hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-16','DANGE CHOWK','residential','1BHK',14000,NULL,25000,13000,NULL,NULL,'Furnished',NULL),
(90,'Pratikshya','www.justdial.com','9579254003','1rk chahiye chinchwd gao mai family hai job bhi chinchwd mai hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-16','Chinchwad','residential',' 1RK',4000,NULL,NULL,3000,NULL,NULL,'Unfurnished',NULL),
(91,'Sushil','www.justdial.com','9881333044','Row House Chahiye 1 Crore Mai Dangechowk , Baner ,','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-17','DANGE CHOWK,BANER,New sangvi','residential','Residential House',NULL,NULL,NULL,1000000,NULL,NULL,'Unfurnished',NULL),
(92,'Anita','www.justdial.com','8421831461','1rk chahiye chinchwd gao mai family hai Job Wakad','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-17','Chinchwad','residential',' 1RK',4000,NULL,10000,3000,NULL,NULL,'Unfurnished',NULL),
(93,'Deshpande','www.99acres.com','9833738558','1rk chahiye wakad mai family hai agar kh ka chahiye  hoga room toh khud call krege','tenant','pending','2016-06-23 19:40:27',NULL,'srmr@gmail.com','2016-06-17','WAKAD','residential',' 1RK',5000,NULL,15000,4000,NULL,NULL,'Unfurnished',NULL),
(94,'Apurva Phansalkar','www.99acres.com','7507566220','1bhk chahiye wakad Mai Society Chahiye  akleli rhegi But Urgent Reuirement Chahiye maine 1bhk batayi hu wajad ka poniris hospital ke pass unhone kaha khud call kregi','tenant','pending','2016-06-23 19:40:27',NULL,'apurva.phansalkar212@gmail.com','2016-06-17','WAKAD','residential','1BHK',11000,NULL,30000,10000,NULL,NULL,'Unfurnished',NULL),
(95,'Anil','www.99acres.com','9594532264','1rk chahiye 5000 tk dangechowk ke traf chahiye family hai job hinjewadi','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-17','DANGE CHOWK','residential',' 1RK',5000,NULL,10000,4000,NULL,NULL,'Unfurnished',NULL),
(96,'Bapurao','www.justdial.com','7040596470','2bhk chahiye akudi ya chinchwd traf family hai thatwade job hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-17','Chinchwad,Akurdi','residential','2BHK',11000,NULL,30000,10000,NULL,NULL,'Unfurnished',NULL),
(97,'Harish','www.justdial.com','9172061111','Row house chahiye fully furnished 6 bachlor rhege job internet ka hai rowhouse mai he krege','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-17','PIMPLE SAUDAGAR','residential','3BHK',40000,NULL,NULL,30000,NULL,NULL,'Furnished',NULL),
(98,'Hardik','www.justdial.com','9167313181','nigidi chinchwd akurdi mai 1rk / ya 1bhk  chahiye bachlor hai single hai','tenant','pending','2016-06-23 19:40:27',NULL,'har19051987@gmail.com','2016-06-17','NIGADI','residential',' 1RK',8000,NULL,20000,7000,NULL,NULL,'Unfurnished',NULL),
(99,'Sushant Roy','www.99acres.com','9978700222','2bhk chahiye wakad ya pimole saudagar mai fully furnished single rhega aur hinjewadi mai job hai','tenant','pending','2016-06-23 19:40:27',NULL,'sushant.roys@gmail.com','2016-06-17','WAKAD','residential','2BHK',17500,NULL,30000,17000,NULL,NULL,'Furnished',NULL),
(100,'Rohit','www.99acres.com','7507209888','1bhk chahiye wakad mai family hai mom aur rohit sir rhege unhe 30 june se possession chaihiye hinjewadi mai job krta hai','tenant','pending','2016-06-23 19:40:27',NULL,'aarveepro@gmail.com','2016-06-17','WAKAD','residential','1BHK',NULL,NULL,30000,11000,NULL,NULL,'Unfurnished',NULL),
(101,'shishir','www.99acres.com','7798259240','1rk chahiye dangechowk ya 16.no mai 2 girls rhegi aur  student hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-17','DANGE CHOWK','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(102,'Anushka','www.99acres.com','7744834340','1rk chahiye wakad mai ya pimple saudagar ke waha aur family hai bhai aur mom kbhi kbhi aayegi job hinjewadi','tenant','pending','2016-06-23 19:40:27',NULL,'anu_shirodkar@yahoo.co.in','2016-06-18','WAKAD','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(103,'Parth','www.justdial.com','9689621579','2bhk rent mai dena hai 16.no mai / \nBldg name- Karishma Apartment / flat No-B-101/\n Add- Dangechowk to 16.no bus stop mamta hospital Baju mai gali ke andr hai /\n Falt available - 1 July 2016 / \nKeys with - Parth / \nProperty on Floor -3 ( No of floor-1 ) / And \nFlat type -  Semi Furnished ( Cubt , Fan , Light , Modular Kitchen , Lost ) \nParking - Cover parking ( 2/4 Wheeler )\n800 Sqft','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-07-01','DANGE CHOWK','residential','2BHK',NULL,NULL,35000,13000,NULL,NULL,'SemiFurnished',NULL),
(104,'Nitin',NULL,'8857890945','shop chaiye jattadiary, Pimple saudagar ya vishal nagar ke waha','tenant','pending','2016-06-23 19:40:27',NULL,'nitindhare28@gmail.com','2016-06-18','PIMPLE SAUDAGAR,VISHAL NAGAR','residential','Multistorey Apartment',10000,NULL,30000,9000,NULL,NULL,'Unfurnished',NULL),
(105,'shubham','www.justdial.com','9922504346','plorent mai dena hai 8 acres dangechowk to punawal se jambe kuthe patil office  ke bju wala / landmark - life republic','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-18','MARUNJI','residential','Residential Plot',NULL,NULL,NULL,5000000,NULL,NULL,NULL,NULL),
(106,'Ashok','www.justdial.com','9595492020','1rk chahiye dangechowk mai but 19/6/16 ko possession chahiye 3 bachlors hai  aur student hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-18','DANGE CHOWK','residential',' 1RK',5000,NULL,12000,4000,NULL,NULL,'Unfurnished',NULL),
(107,'Vittal','www.justdial.com','9022071777','1Bhk chahiye chinchwd ya pimperi mai 4 bachlor rhege','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-18','Chinchwad','residential','1BHK',7500,NULL,16000,7000,NULL,NULL,'Unfurnished',NULL),
(108,'Priyanka','www.99acres.com','8623019178','1bhk chahiye wakad kalewadi dangechowk family hai job hinjewadi','tenant','pending','2016-06-23 19:40:27',NULL,'','2016-06-18','WAKAD,DANGE CHOWK','residential','1BHK',7000,NULL,12000,6000,NULL,NULL,'Unfurnished',NULL),
(109,'milind','www.99acres.com','9011446098','1rk/1bhk chlega  7000 ke budget mai family hai mom aur milind','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','DANGE CHOWK,VISHAL NAGAR,New sangvi','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,NULL,NULL),
(110,'Akash','www.99acres.com','9595549251','1bhk chahiye  wakad ya chinchwd, akurdi ki traf 4 bachlor hai job chakkan','tenant','pending','2016-06-23 19:40:27',NULL,'aakashsankpal.47@gmail.com','2016-06-19','Chinchwad','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(111,'mozes','www.justdial.com','8805561616','1bhk chahiye kalewadi mai  family hai 2 members but 6000 ke range mai he chahiye 1bhk','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','DANGE CHOWK','residential','1BHK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(112,'Sujay','www.justdial.com','8087631842','1bhk chahiye bujpal chowk , wakad, 4 bachlor hai hinjewadi job','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','BHUMKAR CHOWK','residential','1BHK',12000,NULL,20000,11000,NULL,NULL,'Unfurnished',NULL),
(113,'Avadhuv',NULL,'9881003540','2bhk chahiye 6 bachlor hai  wakad dangechowk tk chlega aur abhi duggal sir se baat ki hu but woh batyega wakad link ka 2bhk and walking leads','tenant','pending','2016-06-23 19:40:27',NULL,'ad.deuskar@yahoo.com','2016-06-19','WAKAD','residential','2BHK',13000,NULL,25000,12000,NULL,NULL,'Unfurnished',NULL),
(114,'snehal','','8421606217','1rk chahiye bachlor 2 and job haddapsar mai','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','DANGE CHOWK','residential',' 1RK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(115,'ganesh sindhe',NULL,'9881512890','1rk chahiye 2 he rhege sister and brother walking leads / study krte hai balewadi mai','tenant','pending','2016-06-23 19:40:27',NULL,'ganrajshinde@gmail.com','2016-06-19','DANGE CHOWK','residential',' 1RK',6000,NULL,13000,5000,NULL,NULL,'Unfurnished',NULL),
(116,'sushmita','www.justdial.com','8857970242','1bhk chahiye chinchwd mai family hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','Chinchwad','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(117,'Vitthal','www.99acres.com','9975701168','16.no 1rk  ka visit ke liye aa rhe kal 20/6/162 ladies hai study krte hai 16.no ke pass he','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','DANGE CHOWK','residential',' 1RK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(118,'Priyank Srivastava','www.99acres.com','7309676991','1bhk chahiye dangechowk mai but 2 bachlor hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','DANGE CHOWK','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(119,'rahul','www.99acres.com','8275583781','1rk chahiye 3 bachlor hai dangechowk , Ravet Bhumkarchowk abhi kh ka pic whtsup ki  job hinjewadi','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','DANGE CHOWK','residential',' 1RK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(120,'Deeplaxmi','www.99acres.com','9970717616','1bhk chahiye wakad pimple saudagar,kalewadi , rahatani, but budget 8000 family hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','WAKAD,DANGE CHOWK,PIMPLE SAUDAGAR','residential','1BHK',8000,NULL,20000,7000,NULL,NULL,'Unfurnished',NULL),
(121,'Varun','www.99acres.com','7774950780','2bhk chahiye dangechowk,pimperi saudagar mai 2 bachlor hai job hinjewadi','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','DANGE CHOWK','residential','2BHK',14000,NULL,30000,13000,NULL,NULL,'Unfurnished',NULL),
(122,'ram','www.justdial.com','9850984560','1rk cahhiye ratni , pimpel saudagar, kalewadi but b-5000 family hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-19','PIMPLE SAUDAGAR','residential',' 1RK',5000,NULL,10000,4000,NULL,NULL,'Unfurnished',NULL),
(123,'preeti','www.99acres.com','9765952265','2bhk chahiye wakad , pimple saudagar ki traf 3 bachlor hai','tenant','pending','2016-06-23 19:40:27',NULL,'vermapreeti61@gmail.com','2016-06-20','WAKAD,PIMPLE SAUDAGAR','residential','2BHK',15000,NULL,30000,14000,NULL,NULL,'Unfurnished',NULL),
(124,'Farida','www.justdial.com','7249024231','1rk chahiye pimperi mai family hai bacche pimperi mai study krte hai isliye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-20','Pimpri','residential',' 1RK',8000,NULL,20000,7000,NULL,NULL,'Unfurnished',NULL),
(125,'Kranti','www.justdial.com','8600257239','1bhk chahiye pimperi chinchwd traf chafferckr chowk ke waha family hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-20','Pimpri,Chinchwad','residential','1BHK',7000,NULL,2000,6000,NULL,NULL,'Unfurnished',NULL),
(126,'sanatanu','www.99acres.com','9130089763','1bhk ya 1rk chahiye hinjewadi mai ya wakad mai 1rk dekhne aa rhe kh ka 7.00 bje single hai hinjewadi job hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-20','WAKAD,HINJEWADI','residential',' 1RK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(127,'Natsih','www.justdial.com','7276232443','1bhk chahiye ravet akurdi mai 4 bachlor hai study krte hai r unhe broker throw nhi chahiye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-20','RAVET,Akurdi','residential','1BHK',8000,NULL,15000,7000,NULL,NULL,'Unfurnished',NULL),
(128,'Sanket Jadhav','www.99acres.com','8380076955','1bhk chahiye chinchwd mai family ke liye sundhar bhag ka batati but costly laga','tenant','pending','2016-06-23 19:40:27',NULL,'sanketbjadhav@gmail.com','2016-06-20','Chinchwad','residential','1BHK',9000,NULL,25000,8000,'',NULL,'Unfurnished',NULL),
(129,'Rahul','www.99acres.com','8149786231','1rk chahiye wakad ya hinjewadi mai 2 bachlor hai hinjewadi job','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-20','WAKAD,HINJEWADI','residential',' 1RK',6000,NULL,NULL,5000,NULL,NULL,'Unfurnished',NULL),
(130,'sameer','www.justdial.com','9823062344','shop purchase krna  chahte hai ravet ke acche area mai b- 300000','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-20','RAVET','commercial','Commercial Shop',300000,NULL,NULL,2800000,NULL,NULL,'Unfurnished',NULL),
(131,'Anil','www.justdial.com','9860013528','1bhk chahiye chinchwd mai family hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-20','Chinchwad','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(132,'pradnya','www.justdial.com','9028729137','1bhk chahiye chinchwd mai 4 bachlor girl hai study krte hai chinchwd mai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-21','Chinchwad','residential','1BHK',8000,NULL,10000,7000,NULL,NULL,'Unfurnished',NULL),
(133,'prabhat','www.justdial.com','9723820877','1bhk chahiye single hai hinewadi ke aaju baju chlega hinjewadi mai job hai 1 rk nhi chahiye','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-21','HINJEWADI','residential','1BHK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(134,'surbhi','www.justdial.com','8484877486','1rk chahiye dangechowk mai family hai husband wife maine sh ka batatyi husband ko puch ke call krege','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-21','DANGE CHOWK','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(135,'Tushar','www.99acres.com','9096907040','unhe dangechowk mai 1rk/1bhk chahiye 4 bachlor hai 7000-8000 budget brokerge se nhi chahiye unhe isiye  call cut he kr diya unhone','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-21','DANGE CHOWK','residential','1BHK',8000,NULL,15000,7000,NULL,NULL,'Unfurnished',NULL),
(136,'Jadhav','www.justdial.com','7385545274','rent mai flat dena hai devroad mai single room hai /Add-  Dangechowk to Devroad se vikas nagar , Nagar Palika Marathi School Ke pass / ground floor he hai aur patarega ghar hai / bachlor allowed','owner','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-21','WAKAD,DANGE CHOWK,HINJEWADI,RAVET,PIMPLE SAUDAGAR','residential','Studio Apartment',NULL,NULL,5000,2500,NULL,NULL,'Unfurnished',NULL),
(137,'johnson','www.99acres.com','8975750137','1rk chahiye wakad ya hinjewadi ke ass pass wakad ka maine kh ka batayi aaj aa rhe visiting ke liye 6 bje tk 1 bachlor he rhega aur hinjewadi job','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','WAKAD,DANGE CHOWK,HINJEWADI','residential',' 1RK',6500,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(138,'Kasturi Laxmikant Thakur','www.99acres.com','9657989575','1bhk / 1rk  bhi chlega hinjewadi wakad  acchi society mai hoga toh 11000 bhi pay krne tayae hai husband wife rhege hinjewadi job abhi banglore mai possession 1 july se chahiye pic send krne bole hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','WAKAD','residential','1BHK',9000,NULL,20000,8000,NULL,NULL,'Unfurnished',NULL),
(139,'vishal','www.99acres.com','9730110600','1bhk chahiye chinchwd mai family hai job hinjewadi mai hai','tenant','pending','2016-06-23 19:40:27',NULL,'vmsawant91@gmail.com','2016-06-23','Chinchwad','residential','1BHK',8000,NULL,25000,7000,NULL,NULL,'Unfurnished',NULL),
(140,'Ankit','www.99acres.com','9426451679','1rk chahiye wakad mai 1 single lady rhegi','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','WAKAD','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(141,'Avishek Singh','www.99acres.com','7774041805','1bhk chahiye hinjewadi mai ya wakad dangechowk single hai hinjewadi job but unhe without brokerage chahiye','tenant','pending','2016-06-23 19:40:27',NULL,'avishek18singh@gmail.com','2016-06-23','WAKAD,DANGE CHOWK,HINJEWADI','residential','1BHK',9000,NULL,25000,8000,NULL,NULL,'Unfurnished',NULL),
(142,'Gautam Dhali','www.99acres.com','9545778122','1rk chahiye wakad dangechowk ki traf bhai behen rhege sh ka batayi visit krne aayege','tenant','pending','2016-06-23 19:40:27',NULL,'gautampv26@gmail.com','2016-06-23','WAKAD,DANGE CHOWK','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(143,'Sumit','www.justdial.com','9405603240','1rk chahiye dangechowk mai 3  bachlor hai bhiumkarchowk mai training krte hai maine sh batatyi visit ke liye aa rhe','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','WAKAD,DANGE CHOWK','residential',' 1RK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(144,'Aditya','www.justdial.com','9764831417','1bhk chahiye chinchwd mai 4 ladkiya hai hinjewadi job mai hai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','Chinchwad','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(145,'Anji','www.justdial.com','8177875716','1rk chahiye dangechowk mai  bhai behen rhege job hinjewadi mai','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','DANGE CHOWK','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(146,'kashab','www.justdial.com','7040520826','1bhk chahiye hinjewadi phase 2 mai single bhi reh skta hai aur friend bhi aa skte hai job hinjewadi','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','HINJEWADI','residential','1BHK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(147,'priya','www.justdial.com','8879113397','1rk chahiye chinchwd ya dangechowk ki traf 3 ladkiya hai sh ka bata di chahiye hoga toh  khud call krege','tenant','pending','2016-06-23 19:40:27',NULL,NULL,'2016-06-23','DANGE CHOWK,Chinchwad','residential',' 1RK',6000,NULL,1500,5000,NULL,NULL,'Unfurnished',NULL),
(148,'Bhalerao','www.99acres.com','9730731150','1bhk chahiye chinchwd gaon mai family hai jon talegao','tenant','pending','2016-06-23 19:40:27',NULL,'bhaleraoab2512@gmail.com','2016-06-23','Chinchwad','residential','1BHK',8000,NULL,2000,7000,NULL,NULL,'Unfurnished',NULL),
(149,'Amol','www.justdial.com','9552321032','1bhk ya 2bhk chahiye dangechowk mai / 2 ladies  hai bachlor hai society mai chahiye','tenant','pending','2016-06-23 19:40:27',NULL,'amol7786@yahoo.com','2016-06-27','DANGE CHOWK','residential','1BHK',15000,NULL,30000,14000,NULL,NULL,'Unfurnished',NULL),
(150,'Kiran k','www.99acres.com','8888888888',NULL,'owner',NULL,'2016-06-24 10:01:48',NULL,'kk@gmail.com','2016-06-24','DANGE CHOWK','residential','1BHK',8000,NULL,20000,10000,NULL,1,'Unfurnished',NULL),
(151,'Girish','www.99acres.com','8600944407','1bhk chahiye  chinchwd mai family hai sudhar bhag wala flat dikhna hai aaj','tenant',NULL,'2016-06-24 10:54:47',NULL,'giriyn2k4@yahoo.co.in','2016-07-01','Chinchwad','residential','1BHK',10000,NULL,40000,9000,NULL,NULL,'Unfurnished',NULL),
(152,'Jasmin','www.99acres.com','9820955067','1bhk/ya 2bhk chhiye acchi apartment mai akurdi , ravet ki traf family hai','tenant',NULL,'2016-06-24 11:14:20',NULL,'jazgangani@gmail.com','2016-07-01','RAVET,Akurdi','residential','2BHK',13000,NULL,NULL,12000,NULL,NULL,'Unfurnished',NULL),
(153,'Pooja','www.justdial.com','8796660180','1rk chahiye dangechowk mai ya wakad mai 2 girls rhegi','tenant',NULL,'2016-06-24 13:54:48',NULL,NULL,'2016-07-01','WAKAD,DANGE CHOWK','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(154,'Rajshree','www.justdial.com','9623877763','1bhk chahiye  dy patil collge ke waha 4 ladies rhege study krte hai','tenant',NULL,'2016-06-24 14:07:32',NULL,NULL,'2016-06-25','RAVET,Akurdi','residential','1BHK',10000,NULL,10000,9000,NULL,NULL,'Unfurnished',NULL),
(155,'Mayuresh','www.99acres.com','9011622878','1bhk chaihiye wakad ya ravet ki traf husbnd wife hai Hinjewadi job','tenant',NULL,'2016-06-24 15:28:17',NULL,NULL,'2016-07-01','WAKAD,RAVET','residential','1BHK',8000,NULL,30000,7000,NULL,NULL,'Unfurnished',NULL),
(156,'Abhinav','www.justdial.com','9545074433','1rk chahiye dangechowk ya kalewadi ,mai 3 bachlor hai','tenant',NULL,'2016-06-24 17:04:20',NULL,NULL,'2016-06-27','DANGE CHOWK','residential',' 1RK',6000,NULL,10000,5000,NULL,NULL,'Unfurnished',NULL),
(157,'prasad','www.justdial.com','9545097003','1bhk chahiye pimple saudagar mai family hai wakad ka 1 bhk batayi wakad mai nhi chahiye','tenant',NULL,'2016-06-24 17:20:33',NULL,NULL,'2016-07-01','PIMPLE SAUDAGAR','residential','1BHK',11000,NULL,40000,10000,NULL,NULL,'Unfurnished',NULL),
(158,'Shantesh Katke','www.99acres.com','9881239597','1bhk chaiye chinchwd mai family hai job bhi chinchwd','tenant',NULL,'2016-06-25 13:05:49',NULL,NULL,'2016-07-01','Chinchwad','residential','1BHK',9000,NULL,20000,8000,NULL,NULL,'Unfurnished',NULL),
(159,'navnath','www.justdial.com','9762922195','2bhk chahiye chinchwd pimperi mai but b- 30 lakh only','owner',NULL,'2016-06-25 13:37:13',NULL,NULL,'2016-06-25','Pimpri,Chinchwad','residential','Residential House',NULL,NULL,NULL,300000,NULL,NULL,'Unfurnished',NULL),
(160,'Rahul','www.justdial.com','8275519572','1/2 bhk chahiye kalewadi , pimplesaudagar ya sangvi mai 5 bachlor hai job hinjewadi','tenant',NULL,'2016-06-25 13:45:09',NULL,NULL,'2016-06-25','PIMPLE SAUDAGAR,New sangvi','residential','1BHK',10000,NULL,NULL,9000,NULL,NULL,'Unfurnished',NULL),
(161,'siddessh','www.justdial.com','9922410486','1rk chahiye wakad dangechowk 2 bachlor hai indra collge mai sh ka flat batayi but unhe 16.no nhi chahiye','tenant',NULL,'2016-06-25 14:00:37',NULL,NULL,'2016-07-01','DANGE CHOWK','residential',' 1RK',6500,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(162,'Sunita','www.justdial.com','9822708484','1bhk chahiye dangechowk ya punewala ke waha husband wife rhege 16.no batayi 1bhk mana kiye job punewala','tenant',NULL,'2016-06-25 16:10:19',NULL,NULL,'2016-07-01','DANGE CHOWK','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(163,'Sachin','www.99acres.com','8390902537','1rk chahiye aundh mai bachlor hai','tenant',NULL,'2016-06-26 12:05:36',NULL,NULL,'2016-07-01','Aundh','residential',' 1RK',5000,NULL,12000,4000,NULL,NULL,'Unfurnished',NULL),
(164,'Gobinda','www.99acres.com','9768286963','1bhk chahiye wakad hinjewadi ke ass pss mai family hai job hinjewadi broker se nhi chahiye falt','tenant',NULL,'2016-06-26 12:13:49',NULL,'gobindakrushna@gmail.com','2016-07-01','WAKAD,HINJEWADI','residential','1BHK',9000,NULL,15000,8000,NULL,NULL,'Unfurnished',NULL),
(165,'pratiska','www.justdial.com','8856095549','single room chahiye pimperi chinchwd mAi bachlor hai','tenant',NULL,'2016-06-26 12:22:27',NULL,NULL,'2016-06-28','Pimpri,Chinchwad','residential',' 1RK',2000,NULL,4000,1000,NULL,NULL,'Unfurnished',NULL),
(166,'Mayuresh','www.99acres.com','9011622878','1bhk chahiye wakad mai family hai husbnd wife maine 16.no ka 7000 1bhk batayi but unhe broker se nhi chahiye /  chahiye hoga toh khud call krege','tenant',NULL,'2016-06-26 13:06:30',NULL,NULL,'2016-07-01','WAKAD','residential','1BHK',8000,NULL,20000,7000,NULL,NULL,'Unfurnished',NULL),
(167,'gajendra','www.justdial.com','8888849265','1bhk chahiye chinchwd mai 4 bachlor hai','tenant',NULL,'2016-06-26 13:15:08',NULL,NULL,'2016-06-27','Chinchwad','residential','1BHK',7000,NULL,15000,6000,NULL,NULL,'Unfurnished',NULL),
(168,'Swati','www.99acres.com','8605384042','1rk chahiye dangechowk ya hinjewadi ke udhar 1 ladki chahiye','tenant',NULL,'2016-06-26 15:50:51',NULL,NULL,'2016-07-01','DANGE CHOWK,HINJEWADI','residential',' 1RK',6000,NULL,15000,5000,NULL,NULL,'Unfurnished',NULL),
(173,'hemant','www.justdial.com','7878787878','hhhhhh  hhhhh hhhhhhhhhhh','tenant','close','2017-04-08 23:11:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'87878787878',NULL,NULL,NULL),
(174,'aniket','www.makan.com','4545454545','hjjhjhjh hjhjhj hjhj jhj jhjhjh','owner','pending','2017-01-09 16:59:06',NULL,NULL,NULL,'Pune','Residential',NULL,33333,33333,NULL,3333,'54545454545',NULL,'Furnished','dddddd'),
(176,'test user','www.quikr.com','9898988989','test','owner','pending','2017-04-10 11:05:57',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'98989898988',NULL,NULL,NULL),
(177,'test user','www.quikr.com','9898988989','test','owner','pending','2017-04-10 11:06:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'98989898988',NULL,NULL,NULL),
(178,'qpatilggg','www.quikr.com','7777777777','qqqqqqqqqqqqqqqqqqqpatil','tenant','pending','2017-05-20 16:15:58',NULL,'qpatil@gmail.com','20/05/2017','New sangvi',NULL,NULL,5000,NULL,9000,4000,'45454554545',NULL,NULL,NULL);

/*Table structure for table `tbl_lead_followup` */

CREATE TABLE `tbl_lead_followup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `details` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `staff_id` int(10) DEFAULT NULL,
  `next_followup_date` varchar(30) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `next_followup_time` varchar(10) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead_followup` */

insert  into `tbl_lead_followup`(`ID`,`details`,`date`,`staff_id`,`next_followup_date`,`lead_id`,`next_followup_time`,`status`) values 
(1,'test','2016-09-27 14:42:55',NULL,'09/27/2016',3,NULL,NULL),
(2,'hello','2016-09-27 14:42:57',NULL,'09/27/2016',3,NULL,NULL),
(3,'hello again','2016-09-27 14:42:58',NULL,'09/27/2016',3,NULL,NULL),
(4,'my god hello again','2016-09-27 14:42:59',NULL,'09/27/2016',3,NULL,NULL),
(5,'test on 21st','2016-09-27 14:43:00',NULL,'09/27/2016',3,NULL,NULL),
(6,'dummy','2016-09-27 14:43:01',NULL,'09/27/2016',7,NULL,NULL),
(7,'pleasse call me  afterr two days','2016-09-27 14:43:03',NULL,'09/27/2016',3,NULL,NULL),
(8,'hthhyh','2016-09-27 14:43:02',NULL,'09/27/2016',7,NULL,NULL),
(9,'test','2016-09-27 14:43:04',NULL,'09/27/2016',10,NULL,NULL),
(10,'test agaiin for m testing','2016-09-27 14:43:05',NULL,'09/27/2016',10,NULL,NULL),
(11,'first followup','2016-09-27 14:43:09',NULL,'09/27/2016',8,NULL,NULL),
(12,'2nd teest','2016-09-27 14:43:11',NULL,'09/27/2016',8,NULL,NULL),
(15,'3rd test','2016-09-27 14:43:12',NULL,'09/27/2016',8,NULL,NULL),
(16,'test','2016-09-27 14:43:13',NULL,'09/27/2016',6,NULL,NULL),
(17,'test2','2016-09-27 14:43:17',NULL,'09/27/2016',0,NULL,NULL),
(18,'test2','2016-09-27 14:43:19',NULL,'09/27/2016',6,NULL,NULL),
(19,'test3','2016-09-27 14:43:20',NULL,'09/27/2016',6,NULL,NULL),
(20,'test','2016-09-27 14:43:22',NULL,'09/27/2016',6,NULL,NULL),
(21,'first follow up','2016-09-27 14:43:24',NULL,'09/27/2016',11,NULL,NULL),
(22,'2nd followup','2016-09-27 14:43:25',NULL,'09/27/2016',11,NULL,NULL),
(23,'third followup','2016-09-27 14:43:26',NULL,'09/27/2016',11,NULL,NULL),
(24,'tthjh','2016-09-27 14:43:28',NULL,'09/27/2016',6,NULL,NULL),
(25,'please call him again at 4 pm','2016-09-27 14:43:29',NULL,'09/27/2016',16,NULL,NULL),
(26,'5pm followup','2016-09-27 14:43:31',NULL,'09/27/2016',16,NULL,NULL),
(27,'he asked to call at 4PM today','2016-09-27 14:43:33',NULL,'09/27/2016',10,NULL,NULL),
(28,'kal kiran sir visit ke liye gye thy ya nhi puchna hai','2016-09-27 14:43:35',NULL,'09/27/2016',40,NULL,NULL),
(29,'Aaj 5 bje call krna hai','2017-01-24 12:17:52',NULL,'09/27/2016',42,NULL,'close'),
(30,'call ki recevied nhi kiye','2016-09-27 14:43:38',NULL,'09/27/2016',16,NULL,NULL),
(31,'Call ki recevied nhi kiye','2016-09-27 14:43:40',NULL,'09/27/2016',45,NULL,NULL),
(32,'Tenent hai','2016-09-27 14:43:41',NULL,'09/27/2016',46,NULL,NULL),
(33,'Tenent hai','2016-09-27 14:43:42',NULL,'09/27/2016',45,NULL,NULL),
(34,'Tenent hai','2016-09-27 14:43:43',NULL,'09/27/2016',44,NULL,NULL),
(35,'Tenent hai','2016-09-27 14:43:44',NULL,'09/27/2016',43,NULL,NULL),
(36,'Tenent hai','2017-01-24 12:17:52',NULL,'09/27/2016',42,NULL,'close'),
(37,'mil gya flai','2016-09-27 14:43:49',NULL,'09/27/2016',46,NULL,NULL),
(38,'Tenent hai','2016-09-27 14:43:47',NULL,'09/27/2016',49,NULL,NULL),
(39,'nhi chahiye','2016-09-27 14:43:50',NULL,'09/27/2016',45,NULL,NULL),
(40,'He visited Yesterday but not able to show him flat he is comming Today to see Flat @ Wakad 1BHK Mr. Shivaji Kalates Flat','2016-09-27 14:43:51',NULL,'09/27/2016',40,NULL,NULL),
(41,'Tenent hai','2016-09-27 14:43:52',NULL,'09/27/2016',54,NULL,NULL),
(42,'Tenent hai','2016-09-27 14:43:53',NULL,'09/27/2016',55,NULL,NULL),
(43,'test','2016-09-27 14:43:56',NULL,'09/27/2016',10,NULL,NULL),
(44,'Call today 5.PM','2016-09-27 14:43:55',NULL,'09/27/2016',8,NULL,NULL),
(45,'1rk milega toh 16 call krna hai','2016-09-27 14:43:59',NULL,'09/27/2016',59,NULL,NULL),
(46,'Dugal sir pic bhejege 1 bhk ka send krna hai rohan ko','2016-09-27 14:43:57',NULL,'09/27/2016',89,NULL,NULL),
(47,'Call krna hai 1 bhk ke liye','2016-09-27 14:44:00',NULL,'09/27/2016',94,NULL,NULL),
(48,'1rk dekhne aa rhe sh ka call krna hai','2016-09-27 14:44:01',NULL,'09/27/2016',95,NULL,'close'),
(49,'Call ki thi aab 1rk nhi chahiye','2016-09-27 14:44:03',NULL,'09/27/2016',54,NULL,NULL),
(50,'call krna hai ki chahie ki nhi 1rk','2016-09-27 15:56:33',NULL,'09/26/2016',102,NULL,'close'),
(51,'kal call krna hai 1rk ke liye','2016-09-27 14:44:06',NULL,'09/27/2016',95,NULL,'close'),
(52,'call ki not recevied','2017-01-24 12:17:52',NULL,'09/27/2016',42,NULL,'close'),
(53,'call krna hai','2016-09-27 15:49:42',NULL,'09/27/2016',117,NULL,NULL),
(54,'kal call krna hai','2016-09-27 14:44:07',NULL,'09/27/2016',121,NULL,NULL),
(55,'kal call krna hai','2016-09-27 15:49:44',NULL,'09/27/2016',134,NULL,NULL),
(56,NULL,'2016-09-27 15:56:29',NULL,'09/26/2016',126,NULL,NULL),
(57,'dekhe kh ka batayege khud call kr ke','2016-09-27 14:44:14',NULL,'09/27/2016',126,NULL,NULL),
(58,'dekha kh khud call krege','2016-09-28 12:49:03',NULL,'09/27/2016',129,NULL,'close'),
(59,'call ki thi call nhi laga','2016-09-27 14:44:13',NULL,'09/27/2016',117,NULL,NULL),
(60,'unko sh ka 1rk dekhna tha call nhi recevied kr rhe','2016-09-27 14:44:09',NULL,'09/27/2016',113,NULL,NULL),
(61,'call krna hai','2016-09-27 14:44:18',NULL,'09/27/2016',137,NULL,NULL),
(62,'call krna hai','2016-09-28 13:10:25',NULL,'09/27/2016',138,NULL,'close'),
(63,NULL,'2016-09-27 14:44:24',NULL,'09/27/2016',137,NULL,NULL),
(64,NULL,'2016-09-28 13:10:25',NULL,'09/26/2016',138,NULL,'close'),
(65,'call krna hai','2016-09-27 15:49:48',NULL,'09/27/2016',140,NULL,NULL),
(66,'call krna hai','2016-09-27 14:32:47',NULL,'09/27/2016',142,NULL,'close'),
(67,'call krna hai','2016-09-27 14:32:46',NULL,'09/27/2016',143,NULL,NULL),
(68,'call krna hai','2016-09-27 14:32:42',NULL,'09/27/2016',145,NULL,NULL),
(69,'kal call krna hai','2016-09-27 16:20:24',NULL,'09/26/2016',147,NULL,NULL),
(70,'Test','2016-09-27 14:32:41',NULL,'09/27/2016',144,'02:00',NULL),
(71,'cal krna hai','2016-09-28 12:48:26',NULL,'09/27/2016',151,NULL,'close'),
(72,'call krna hai','2016-09-27 14:32:39',NULL,'09/27/2016',152,NULL,'close'),
(73,'khud call krege','2016-09-27 15:56:26',NULL,'09/26/2016',152,NULL,'close'),
(74,'room accha nhi laga kyuki bohot dush tha kh sh dono','2016-09-27 14:32:36',NULL,'09/27/2016',140,NULL,NULL),
(75,'sh ka dikhya but pasand nhi aaya unhone dusra book kr diya','2016-09-27 16:20:28',NULL,'09/26/2016',143,NULL,'close'),
(76,'apni friend ko puch ke batyegi call krna hai','2016-09-27 15:49:54',NULL,'09/27/2016',153,NULL,'close'),
(77,'call krna hai','2016-09-27 15:49:57',NULL,'09/27/2016',156,NULL,'close'),
(78,'book ho gyachinchwd ka flat sundarbhag','2016-09-27 15:49:59',NULL,'09/27/2016',151,NULL,'close'),
(79,'call nhi recevied kiye','2016-09-27 15:56:24',NULL,'09/26/2016',152,NULL,'close'),
(80,'call ki but nhi chahiye','2016-09-27 15:50:02',NULL,'09/27/2016',153,NULL,'close'),
(81,'Call ki but cut kiye call','2016-09-27 15:50:03',NULL,'09/27/2016',156,NULL,'close'),
(82,'test','2016-09-27 16:20:30',NULL,'09/26/2016',149,'19:37','close'),
(83,'test','2016-09-27 15:50:07',NULL,'09/27/2016',149,'09:00 am','close'),
(84,'test again with moment','2016-09-30 11:28:05',NULL,'09/27/2016',149,'19:49','close'),
(85,'ttes','2016-09-30 11:28:05',NULL,'09/26/2016',149,'02:01','close'),
(86,'ttt','2016-09-30 11:28:05',NULL,'09/27/2016',149,'00:00','close'),
(87,'ttttt','2016-09-30 11:28:05',NULL,'09/26/2016',149,'13:00','close'),
(88,'ttttt','2016-09-30 11:28:05',NULL,'09/27/2016',149,'14:00 PM','close'),
(89,'test on 6th july','2016-09-27 15:50:13',NULL,'09/27/2016',142,'15:33 PM',NULL),
(90,'test','2016-09-30 11:28:05',NULL,'09/27/2016',149,'14:20 PM','close'),
(91,'TEST AGAIN','2016-09-30 11:28:05',NULL,'09/27/2016',149,'15:00 PM','close'),
(92,'not recive call','2016-09-30 11:28:05',NULL,'09/27/2016',149,'12:59 PM','close'),
(93,'asdss','2016-10-05 16:03:41',NULL,'09/26/2016',152,'12:04 PM','close'),
(112,'hhhhhhhhh','2016-09-27 15:50:27',NULL,'09/27/2016',92,'17:30 PM',NULL),
(113,'hemant call','2016-09-27 15:48:30',NULL,'09/27/2016',112,'17:00 PM',NULL),
(114,'today','2016-09-27 14:35:50',NULL,'09/27/2016',112,'17:00 PM',NULL),
(115,'today test data','2017-01-12 16:50:49',NULL,'09/28/2016',151,'17:00 PM','close'),
(116,'test data2','2016-09-28 12:49:02',NULL,'09/28/2016',129,'20:00 PM',NULL),
(117,'test','2016-09-28 13:10:25',NULL,'09/28/2016',138,'17:05 PM',NULL),
(118,'test mail','2016-09-30 11:28:04',NULL,'09/30/2016',149,'17:05 PM',NULL),
(124,NULL,'2017-01-12 17:41:51',NULL,NULL,173,NULL,'close'),
(125,NULL,'2017-01-09 15:49:55',NULL,NULL,174,NULL,'close'),
(126,NULL,'2017-01-12 16:49:17',NULL,NULL,174,'03:49 PM','close'),
(127,NULL,'2017-01-12 16:49:17',NULL,'2017-01-12T18:30:00.000Z',174,'10:3 PM','pending'),
(128,'BJGVUYUC','2017-01-18 18:12:30',NULL,'01/12/2017',151,'17:00 PM','close'),
(129,'ffffffffffffffffffffffffffffffffffffff','2017-01-31 18:04:39',NULL,NULL,173,'05:41 PM','close'),
(130,NULL,'2017-01-16 12:37:06',NULL,NULL,175,NULL,'close'),
(131,NULL,'2017-01-16 12:37:05',NULL,'2017-01-15T18:30:00.000Z',175,'13:36 PM',NULL),
(132,'remmmmmm','2017-01-18 18:12:30',NULL,'01/18/2017',151,'22:00 PM',NULL),
(133,'hhhhhhhh','2017-01-24 12:18:06',NULL,NULL,42,'12:17 PM','close'),
(134,'remarkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk','2017-01-24 12:18:06',NULL,NULL,42,'12:17 PM',NULL),
(135,'XDFYJHNF','2017-01-31 18:04:39',NULL,'01/31/2017',173,'18:06 PM',NULL),
(136,NULL,'2017-04-10 11:05:57',NULL,NULL,176,NULL,NULL),
(137,NULL,'2017-04-10 11:06:18',NULL,NULL,177,NULL,NULL),
(138,NULL,'2017-04-10 11:12:36',NULL,NULL,178,NULL,NULL),
(139,NULL,'2017-04-10 12:04:49',NULL,NULL,179,NULL,NULL),
(140,NULL,'2017-04-10 12:04:52',NULL,NULL,180,NULL,NULL),
(141,NULL,'2017-04-10 12:04:57',NULL,NULL,181,NULL,NULL),
(142,'qqqqqqqqqqqqqqq','2017-04-13 15:36:25',NULL,'03:36 PM',178,NULL,NULL);

/*Table structure for table `tbl_lead_status` */

CREATE TABLE `tbl_lead_status` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead_status` */

/*Table structure for table `tbl_lead_type` */

CREATE TABLE `tbl_lead_type` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_lead_type` */

/*Table structure for table `tbl_membership_users` */

CREATE TABLE `tbl_membership_users` (
  `memberID` int(20) NOT NULL AUTO_INCREMENT,
  `passMD5` varchar(40) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `groupID` int(10) unsigned DEFAULT NULL,
  `isBanned` tinyint(4) DEFAULT NULL,
  `isApproved` tinyint(4) DEFAULT NULL,
  `custom1` text,
  `custom2` text,
  `custom3` text,
  `custom4` text,
  `comments` text,
  `pass_reset_key` varchar(100) DEFAULT NULL,
  `pass_reset_expiry` int(10) unsigned DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `mobileno` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`memberID`),
  KEY `groupID` (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_membership_users` */

insert  into `tbl_membership_users`(`memberID`,`passMD5`,`email`,`signupDate`,`groupID`,`isBanned`,`isApproved`,`custom1`,`custom2`,`custom3`,`custom4`,`comments`,`pass_reset_key`,`pass_reset_expiry`,`name`,`mobileno`) values 
(1,'UGFzcw==','test@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(5,'anlvdGk=','jyoti@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'jyoti','8525874512'),
(6,'MTIzNDU2','ajinkya.rental@gmail.com',NULL,NULL,NULL,NULL,'9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(10,'YWRGT2FGVw==','aniket@gmail.com',NULL,NULL,NULL,NULL,'32',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tbl_menu` */

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `html` varchar(200) DEFAULT NULL,
  `order` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_menu` */

insert  into `tbl_menu`(`id`,`name`,`html`,`order`) values 
(1,'Dashboard','<a><i class=\"fa fa-dashboard fa-lg\"></i> Dashboard<i class=\'fa fa-dashboard fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',1),
(2,'Staff','<a><i class=\'fa fa-users fa-lg\'></i> Staff <i class=\'fa fa-users fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',2),
(3,'Branch','<a><i class=\'fa fa-university fa-lg\'></i> Branch<i class=\'fa fa-university fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',3),
(4,'Accounts','<a href=\'#\'><i class=\'fa fa-flask\'></i> Accounts </a>',4),
(5,'HelpDesk','<a><i class=\'fa fa-question-circle fa-lg\'></i> Help Desk <i class=\'fa fa-question-circle fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',5),
(6,'UserManaement','<a><i class=\'fa fa-wrench fa-lg\'></i> Access Management <i class=\'fa fa-wrench fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',6),
(7,'Property','<a><i class=\'fa fa-university fa-lg\'></i> Property <i class=\'fa fa-university fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',7),
(10,'OwnerTenant','<a><i class=\'fa fa-user fa-lg\'></i> Owner&Tenant<i class=\'fa fa-user fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',8),
(11,'Project','<a><i class=\'fa fa-book fa-lg\'></i>Project<i class=\'fa fa-book fa-lg btn pull-right\' style=\'margin-top:5px\'></i></a>',9);

/*Table structure for table `tbl_menu_items` */

CREATE TABLE `tbl_menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `html` varchar(200) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_menu_items` */

insert  into `tbl_menu_items`(`id`,`name`,`html`,`menu_id`) values 
(1,'AddLead','<li><a href=\'#/addlead\'>Add Lead</a></li>',5),
(2,'Lead','<li><a href=\'#/showallleads\'>Lead</a></li>',5),
(3,'AddNewBranch','<li><a href=\'#/addbranch\'>Add Branch</a></li>',3),
(4,'BranchList','<li><a href=\'#/branch\'>Branch List</a></li>',3),
(5,'StaffList','<li><a href=\'#/staff\'>Staff List</a></li>',2),
(6,'AddNewStaffMember','<li><a href=\'#/addstaff\'>Add Staff</a></li>',2),
(7,'FeatureList','<li><a href=\'#/feature\'>Access</a></li>',6),
(8,'Home','<li><a href=\"#/\">Home</a></li>',1),
(9,'AssignLead','<li><a href=\'#/assignlead\'>Assign Lead</a></li>',5),
(10,'Property','<li><a href=\'#/property\'>Property</a></li>',7),
(11,'AddProperty','<li><a href=\'#/addproperty\'>Add Property</a></li>',7),
(12,'Edit Lead','<li><a href=\'#/editlead\'>Edit Lead</a></li>',5),
(13,'Owner','<li><a href=\'#/owner\'>Owner List</a></li>',10),
(14,'Add Project','<li><a href=\'#/basic_info\'>Add Project</a></li>',11),
(15,'Tenant','<li><a href=\'#/tenant\'>Tenant List</a></li>',10),
(16,'Project List','<li><a href=\'#/project_list/all\'>Project List</a></li>',11),
(17,'Agreement','<li><a href=\'#/agreement\'>Agreement</a></li>',11);

/*Table structure for table `tbl_owner` */

CREATE TABLE `tbl_owner` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(50) DEFAULT NULL,
  `contact_number` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `landline_no` varchar(15) DEFAULT NULL,
  `birth_date` varchar(50) DEFAULT NULL,
  `owner_anni_date` varchar(50) DEFAULT NULL,
  `owner_add` tinytext,
  `state` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `poa` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  `bank_branch_name` varchar(50) DEFAULT NULL,
  `Bank_account_no` varchar(50) DEFAULT NULL,
  `pin_code` varchar(20) DEFAULT NULL,
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_owner` */

insert  into `tbl_owner`(`ID`,`person_name`,`contact_number`,`email`,`landline_no`,`birth_date`,`owner_anni_date`,`owner_add`,`state`,`city`,`poa`,`bank_name`,`bank_branch_name`,`Bank_account_no`,`pin_code`) values 
(9,'Pallavi patil hp','8177851714','pallavigurav23@gmail.com',NULL,'10/11/2016','10/13/2016','aaaaaaa','Maharashtra','Pune','hhhhh','hhhhhhhh','hhhhhhhh','333333333333','33333333333'),
(10,'Kiran Katkar new','7387545354','kirank@gmail.com',NULL,'10/12/2016','10/11/2016','qqqqqqq','Maharashtra','Nagpur','qqqqqqqqqqq','qqqqqqqqqq','qqqqqqqqq','333333333333','33333333333'),
(99,'Shivni','8975761370','Shivni@gmail.com','2255845','Invalid date','Invalid date','pune test date','Maharashtra','Pune','hhhhh','aaaaaaaaaaa','aaaaaaaaaaa','333333333333','199166'),
(100,'Kiran Katkar','7387545354','kirank@gmail.com','2255256','04/05/2016','10/27/2016','test addrsss','Jharkhand','Nagpur','kiran test','bank test','bank Branch  test','78945612311','123456'),
(101,'Ameer','8446380318',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(102,'Sumant','9686973437',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(103,'jyoti',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(104,'jyoti',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tbl_person_menu_items` */

CREATE TABLE `tbl_person_menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) DEFAULT NULL,
  `menu_items_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_person_menu_items` */

insert  into `tbl_person_menu_items`(`id`,`staff_id`,`menu_items_id`) values 
(1,1,1),
(2,1,2),
(3,1,7),
(4,1,5),
(5,1,6),
(6,1,1),
(7,1,2),
(8,1,8),
(9,1,9),
(10,1,10),
(11,1,13),
(12,1,14),
(13,1,15),
(14,1,16),
(15,1,17),
(16,6,1),
(17,6,2),
(18,6,8),
(19,6,9),
(20,6,10),
(21,6,11),
(22,6,12),
(23,6,13),
(24,6,15),
(25,8,1),
(26,8,2),
(27,8,NULL),
(28,8,NULL),
(29,8,NULL),
(30,8,NULL),
(31,8,9),
(32,8,12),
(33,9,1),
(34,9,2),
(35,9,NULL),
(36,9,5),
(37,9,6),
(38,9,8),
(39,9,9),
(40,9,12),
(41,9,13),
(42,9,14),
(43,9,15),
(44,9,16),
(45,10,1),
(46,10,2),
(47,10,8),
(48,10,9),
(49,10,12),
(50,10,13),
(51,10,15);

/*Table structure for table `tbl_phase` */

CREATE TABLE `tbl_phase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_phase` */

insert  into `tbl_phase`(`id`,`name`,`projectid`) values 
(1,'testPhase',NULL),
(2,'testphase',17),
(3,'testphase2',17),
(4,'test1CT',18),
(5,'3rd phase',17),
(6,'phase1',26),
(7,'1',31),
(8,'1',32),
(9,'phase1',34),
(10,'phase1',36),
(11,'phase1',37),
(12,'phase2',37),
(13,'a',38),
(14,'phase1',39),
(15,'1212',40),
(16,'phase1',41),
(17,'ggb1',42),
(18,'1',43),
(19,'1',43),
(20,'1',43),
(21,'1',44),
(22,'2',44),
(23,'1',44),
(24,'1',44),
(25,'3232',47),
(26,'3333',47),
(27,'1212',47),
(28,'33333333',47),
(29,'zscsss',50),
(30,'ddddd',51),
(31,'phase11',51),
(32,'No Phase',51),
(33,'No Phase',51),
(34,'No Phase',51),
(35,'No Phase',52),
(36,'No Phase',53),
(37,'No Phase',54),
(38,'No Phase',55),
(39,'phase1',56),
(40,'2',56),
(41,'dd',57),
(42,'dsdsds',58),
(43,'dsdsa',59),
(44,'sada',60),
(45,'sdadsad',60),
(46,'asas',60),
(47,'sasas',61),
(48,'ddd',62),
(49,'zxzx',62),
(50,'sasa',62),
(51,'dsds',63),
(52,'ddssd',64),
(53,'No Phase',65),
(54,'p1',65),
(55,'gfg',66),
(56,'nbnj',67),
(57,'2',68),
(58,'No Phase',69),
(59,'ttt',5),
(60,'12',70),
(61,'443',71),
(62,'No Phase',72),
(63,'No Phase',73),
(64,'fdfd',74),
(65,'No Phase',75),
(66,'rr',76),
(67,'No Phase',77),
(68,'kasturi',77),
(69,'aaaa',41);

/*Table structure for table `tbl_phase_setup` */

CREATE TABLE `tbl_phase_setup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `how-many_phases` int(20) DEFAULT NULL,
  `select_phases_number` int(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_phase_setup` */

/*Table structure for table `tbl_project` */

CREATE TABLE `tbl_project` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(50) DEFAULT NULL,
  `project_add` text,
  `state` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `pin_code` varchar(10) DEFAULT NULL,
  `society_amenities` varchar(80) DEFAULT NULL,
  `property_type` varchar(30) DEFAULT NULL,
  `residential_property` varchar(30) DEFAULT NULL,
  `commercial_property` varchar(30) DEFAULT NULL,
  `pg_type` varchar(30) DEFAULT NULL,
  `residentialcommercial` varchar(30) DEFAULT NULL,
  `Phases` decimal(10,0) DEFAULT NULL,
  `Buildings` decimal(10,0) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_project` */

insert  into `tbl_project`(`ID`,`project_name`,`project_add`,`state`,`city`,`pin_code`,`society_amenities`,`property_type`,`residential_property`,`commercial_property`,`pg_type`,`residentialcommercial`,`Phases`,`Buildings`,`location`) values 
(73,'sdfgweasd','dfsdf','Maharashtra','Pune','232332','Swimming Pool,Power Back Up ','residential','Multistorey apartment',NULL,NULL,NULL,NULL,NULL,'{\"lat\":18.59315841616027,\"lng\":73.7769985194609}'),
(74,'abc','dv','Maharashtra','Thane','34545','Swimming Pool','residential','Multistorey apartment',NULL,NULL,NULL,NULL,NULL,'{\"lat\":18.613061101476315,\"lng\":73.74650001525879}'),
(75,'DSD?','EFD>:S','Maharashtra','Pune','411036','Lift ','residentialcommercial',NULL,NULL,NULL,'Hostel',NULL,NULL,'{\"lat\":18.594351557445417,\"lng\":73.76100540161133}'),
(76,'csd','sdczsd','Maharashtra','Thane','543534','Lift ','residential','Builder Floor Apartment',NULL,NULL,NULL,NULL,NULL,''),
(77,'Krushna Heights','Yamuna Nagar Wakad','Maharashtra','Pune','411033','Parking (Covered),Security','residential','Service Apartment',NULL,NULL,NULL,NULL,NULL,'{\"lat\":18.603909951012323,\"lng\":73.76173496246338}');

/*Table structure for table `tbl_property` */

CREATE TABLE `tbl_property` (
  `property_id` int(6) NOT NULL AUTO_INCREMENT,
  `property_type` varchar(30) DEFAULT NULL,
  `property_name` varchar(30) DEFAULT NULL,
  `apartment_facility` varchar(20) DEFAULT NULL,
  `residential_property` varchar(30) DEFAULT NULL,
  `commercial_property` varchar(30) DEFAULT NULL,
  `flat_type` varchar(20) DEFAULT NULL,
  `pg_type` varchar(30) DEFAULT NULL,
  `pg_category_type` varchar(30) DEFAULT NULL,
  `property_Add` text,
  `state` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `pincode` varchar(10) DEFAULT NULL,
  `building_name` varchar(30) DEFAULT NULL,
  `flat_shop_unit_no` varchar(20) DEFAULT NULL,
  `wing` varchar(30) DEFAULT NULL,
  `lene1` varchar(20) DEFAULT NULL,
  `lene2` varchar(20) DEFAULT NULL,
  `society_amenities` varchar(80) DEFAULT NULL,
  `property_photo` varchar(50) DEFAULT NULL,
  `keyrdate` varchar(20) DEFAULT NULL,
  `followup_service` varchar(50) DEFAULT NULL,
  `project_id` int(10) DEFAULT NULL,
  `owner_id` int(10) DEFAULT NULL,
  UNIQUE KEY `property_id` (`property_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_property` */

insert  into `tbl_property`(`property_id`,`property_type`,`property_name`,`apartment_facility`,`residential_property`,`commercial_property`,`flat_type`,`pg_type`,`pg_category_type`,`property_Add`,`state`,`city`,`pincode`,`building_name`,`flat_shop_unit_no`,`wing`,`lene1`,`lene2`,`society_amenities`,`property_photo`,`keyrdate`,`followup_service`,`project_id`,`owner_id`) values 
(17,'commercial','kasturi','furnished',NULL,'Commercial Shop','3BHK',NULL,NULL,'3rd Floor, Kasturi Heights, Plot No. 6, Chinchwad.','Maharashtra','pune','545454','kasturi','test1','A3','3','test1','Swimming Pool,Power Back Up ,Playground',NULL,'2016-04-21',NULL,2,9),
(18,'commercial','careertech','furnished',NULL,'Commercial Shop','3BHK',NULL,NULL,'bhgbhb','Manipur','chinchwad','46545485','careertech it','careertech test2','B5','5','154','Lift ',NULL,'2016-05-11',NULL,4,99),
(19,'residential','hemant','furnished','Multistorey apartment',NULL,'1RK',NULL,NULL,'tikak nager','Manipur','3','122221','hemant','98','A','3','3','Lift ,Swimming Pool,Power Back Up ',NULL,'2016-010-26',NULL,2,9),
(21,'commercial','chetan tast date','furnished',NULL,' Commercial Showroom','3BHK',NULL,NULL,'chetan tast date','Punjab','2','123456','chetan tast date','98','A','35','35','Lift ,Swimming Pool,Power Back Up ,Playground',NULL,'2016-010-27',NULL,2,99),
(22,'residential','kiran tast date','furnished','Multistorey apartment',NULL,'2BHK',NULL,NULL,'kiran tast date','Maharashtra','2','444444','kiran tast date','kiran','B','39','35','Swimming Pool,Power Back Up ,Playground',NULL,'2016-010-29',NULL,4,100),
(23,'pg','kiran name 2','furnished',NULL,NULL,NULL,'Hostel',' boys only','kiran name 2 test','Manipur','4','123131','kiran tast date','98','A','39','35','Lift ,Swimming Pool,Power Back Up ',NULL,'2016-010-28',NULL,2,100);

/*Table structure for table `tbl_role` */

CREATE TABLE `tbl_role` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_role` */

insert  into `tbl_role`(`ID`,`name`) values 
(1,'Admin'),
(2,'HelpDesk'),
(3,'RelationshipManager'),
(4,'BranchManager');

/*Table structure for table `tbl_role_staff` */

CREATE TABLE `tbl_role_staff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL,
  `staff_id` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_role_staff` */

/*Table structure for table `tbl_staff` */

CREATE TABLE `tbl_staff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `contact` varchar(10) DEFAULT NULL,
  `address` text,
  `city` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `zipcode` varchar(6) DEFAULT NULL,
  `image_path` text,
  `permanent_address` varchar(100) DEFAULT NULL,
  `landline_no` varchar(15) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `address_proof_list` varchar(200) DEFAULT NULL,
  `id_proof_list` varchar(200) DEFAULT NULL,
  `assigned_branch` varchar(30) DEFAULT NULL,
  `assigned_desination` varchar(25) DEFAULT NULL,
  `upload_image` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_staff` */

insert  into `tbl_staff`(`ID`,`name`,`email`,`contact`,`address`,`city`,`state`,`zipcode`,`image_path`,`permanent_address`,`landline_no`,`comment`,`address_proof_list`,`id_proof_list`,`assigned_branch`,`assigned_desination`,`upload_image`) values 
(1,'testEmployee1','test@mail.com','22222444','Chinchwad Pune','pune','maharashtra','411033',NULL,'jalgaon','02026777691','Address proof not provvided',NULL,NULL,'Pune','collection officer',NULL),
(3,'testEmp1fordemo','test1demo@mail.com','111111','Akurdi','Pune','Maharashtrra','411035',NULL,'Amravati','123456788','ID proof not  provvided ok eligible','driving license','voting card','Pune','relation manager',NULL),
(4,'testEmp2','test2@mail.com','111111','chinchwad station','Pune','Maharashtra','411033',NULL,'Pune','123345678','All documents ok','Adharcard,Passport','Passport','Pune','collection officer',NULL),
(5,'ttt','ppp@pp.com','5555555555','null','Pune','Maharashtra','null',NULL,'null','55555555555','tttttt','null','null','Pune','relation manager',NULL),
(6,'tttt','555@44com','5555555555','yyyy','Pune','Punjab','555555',NULL,'bbbbbbb','55555555555','ttttttttt',' adhar card','driving license','Pune','collection officer',NULL),
(7,'vicky','vicky@gmail.com','8989898989','pune','Pune','Maharashtra','425001',NULL,'jalgaon','89898989898','no comment','voting card','driving license','Pune','help desk',NULL),
(8,'hemant','heman.patil@gmail.com','2342342343','asfsdfsd','Nagpur','Maharashtra','434234',NULL,'asdfasdfasdfsd','34234234234','343423434',' adhar card','driving license','Pune','collection officer',NULL),
(9,'Amol Ghare','ajinkya.rental@gmail.com','9762292171','16 no. Thergaon Pune','Pune','Maharashtra','411036',NULL,'as above','02065111900','Nil',' adhar card','passport','Pune','help desk',NULL),
(10,'hemantrajesh','hemantrajesh@gmail.com','4444444444','hemantrajesh','Pune','Maharashtra','444444',NULL,'hemantrajesh','45545445454','hemantrajeshxxxxxxxx','driving license','voting card','Pune','relation manager',NULL),
(11,'vikas borse','vikas@gmail.com','9623003037','tilak','Pune','Maharashtra','425001',NULL,'tilak','25722552581','hi','Electricity Bill ','Bank Passbook','Pune','collection officer',NULL),
(19,'bhushanbb','bhushan@gmail.com','4545454545','bhushan','Pune','Maharashtra','454545',NULL,'bhushan','54545454545','bhushan','driving license','passport','Pune','help desk',NULL),
(26,'amol','amol@gmail.com','4545454545','amol','Pune','Maharashtra','454545',NULL,'amol','54545454545','bhushan','driving license','driving license','Pune','help desk',NULL),
(32,'hemant','aaa@g.n','1111111111','aaaaaaaaa','Pune','Maharashtra','111111',NULL,'aaaaaaaaaaaa','11111111111','aaaaaaaaa','passport','driving license','Pune','relation manager',NULL);

/*Table structure for table `tbl_tenant` */

CREATE TABLE `tbl_tenant` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `person_name` varchar(50) DEFAULT NULL,
  `contact_number` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `landline_no` varchar(15) DEFAULT NULL,
  `property_id` int(10) DEFAULT NULL,
  `tenent_add` text,
  `state` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `pin_code` varchar(10) DEFAULT NULL,
  `birth_date` varchar(50) DEFAULT NULL,
  `tenant_anni_date` varchar(50) DEFAULT NULL,
  `companynameaddress` text,
  `collagenameaddress` text,
  `address_proof` varchar(30) DEFAULT NULL,
  `photo_proof` varchar(30) DEFAULT NULL,
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tenant` */

insert  into `tbl_tenant`(`ID`,`person_name`,`contact_number`,`email`,`landline_no`,`property_id`,`tenent_add`,`state`,`city`,`pin_code`,`birth_date`,`tenant_anni_date`,`companynameaddress`,`collagenameaddress`,`address_proof`,`photo_proof`) values 
(2,'Apurva Phansalkar','7507566220','apurva.phansalkar212@gmail.com','5555555',17,'hhhhhhhhhhhh','Punjab','Thane','44444','11/17/2016','11/25/2016','dfdfsadf','sdfsdfasfd','ration_card','pan_card'),
(3,'nitin','9820587702',NULL,NULL,17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(4,'sushmita','8857970242','sushmita@gmail.com','55656565565',NULL,'ghjgjhg bjhbj',NULL,NULL,NULL,'11/17/2016','11/18/2016','fghfghf','fghfdghdfgfdg',NULL,NULL),
(5,'Vitthal','9975701168',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(6,'Vitthal','9975701168',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(7,'ganesh sindhe','9881512890','ganrajshinde@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(8,'Priyank Srivastava','7309676991',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(11,'Anji','8177875716',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(12,'Ankit','9426451679',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(13,'Girish','8600944407','giriyn2k4@yahoo.co.in',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(14,'Girish','8600944407','giriyn2k4@yahoo.co.in',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tbl_unit_setup` */

CREATE TABLE `tbl_unit_setup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `flat_on_floor` int(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_unit_setup` */

/*Table structure for table `tbl_wing_setup` */

CREATE TABLE `tbl_wing_setup` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `wing_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_wing_setup` */

/*Table structure for table `tbl_wings` */

CREATE TABLE `tbl_wings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `building_id` int(11) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  `numbering_style` varchar(20) DEFAULT NULL,
  `flat_count_per_floor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_wings` */

insert  into `tbl_wings`(`id`,`name`,`building_id`,`floor`,`numbering_style`,`flat_count_per_floor`) values 
(1,'test',1,4,NULL,5),
(2,'test',1,2,'2',3),
(3,'test',1,5,'2',5),
(4,'A wing',4,5,'2',5),
(5,'3p1buildwing1',3,3,NULL,4),
(6,'B',4,2,NULL,5),
(7,'1',5,5,NULL,4),
(8,'1',6,4,NULL,4),
(9,'a',7,1,NULL,1),
(10,'A',8,1,'1',1),
(11,'A',5,4,NULL,4),
(12,'1',5,1,NULL,1),
(13,'a',5,2,NULL,2),
(14,'a',11,2,NULL,2),
(15,'AB',11,4,NULL,3),
(16,'A',12,2,NULL,3),
(64,'A',15,3,NULL,4),
(65,'B',15,3,NULL,4),
(66,'C',15,3,NULL,4),
(67,'A',16,3,NULL,4),
(68,'B',16,2,NULL,3),
(69,'C',16,4,NULL,5),
(70,'D',16,4,NULL,3),
(71,'hhuygy',17,2,NULL,2),
(72,'A',23,7,NULL,4),
(73,'B',23,10,NULL,3),
(74,'AB',32,2,NULL,2),
(75,'A wing',32,3,NULL,3),
(76,'A wing',25,7,NULL,4),
(77,'w1',33,2,NULL,4),
(78,'w1',39,2,NULL,4),
(79,'32',40,2,NULL,4),
(80,'w1',41,3,NULL,3),
(81,'hbdsfjh',41,7,NULL,4),
(82,'a',42,2,NULL,3),
(83,'A',43,2,NULL,2),
(84,'Krushna Heights',44,5,NULL,4),
(85,'test',13,5,NULL,5),
(86,'aaaa',46,6,NULL,4),
(87,'bbbbbb',47,3,NULL,2),
(88,'test',47,5,NULL,3),
(89,'ffff',48,4,NULL,2);

/* Procedure structure for procedure `sp_add_branch` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_branch`(in ID int(10),
in name varchar(20),
in contact varchar(10),
in email varchar(30),
in address text,
in city varchar(20),
in state varchar(20),
in zipcode varchar(6),
in bank_name varchar(60),
in bank_account_no varchar(35),
in bank_ifsc_code varchar(35),
in description varchar(500),
In landline_no varchar(15))
BEGIN
	insert into tbl_branch
    (ID, name, contact, email, address, city, state, zipcode, bank_name, 
    bank_account_no, bank_ifsc_code, description,landline_no )
    values
    (ID,name,contact ,email,address,city,state,zipcode,bank_name,
    bank_account_no,bank_ifsc_code,description,landline_no );
    
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_follow_up` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_follow_up`(
in ID int(11),
in lead_id int(11),
in date timestamp,
in ext_followup_date date, 
in Details varchar(500)
)
BEGIN
	insert into tbl_follow_up
    (ID, lead_id, date, next_followup_date, Details)
    values
    (ID, lead_id, date, next_followup_date, Details);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_lead` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_lead`(
   IN  person_name varchar(50),
   IN  reference varchar(30), 
   IN  contact_number varchar(10), 
   IN  remarks text, 
   IN  lead_type varchar(15), 
   IN  `status` varchar(20), 
   
   IN  priority varchar(10), 
   IN  email varchar(30), 
   IN   req_start_date date, 
   IN   location varchar(200), 
   IN	property_type varchar(20), 
   IN	property_type_detail varchar(50), 
   IN	max_rent int, 
   IN	max_deposit int,
   IN	min_rent INT, 
   IN	min_deposit INT,
   IN	landline_no INT)
BEGIN
INSERT INTO `rentmanagement`.`tbl_lead` 
	(
	`person_name`, 
	`reference`, 
	`contact_number`, 
	`remarks`, 
	`lead_type`, 
	
	`priority`, 
	`email`, 
	`req_start_date`, 
	`location`, 
	`property_type`, 
	`property_type_detail`, 
	`max_rent`, 
	`max_deposit`,
	`min_rent`, 
	`min_deposit`,
	`landline_no`
	
	)
 VALUES(
  person_name,
  reference, 
  contact_number, 
  remarks, 
  lead_type, 
  
  priority, 
  email, 
  req_start_date, 
  location, 
  property_type, 
  property_type_detail, 
  max_rent, 
  max_deposit,
  min_rent, 
  min_deposit,
  landline_no);
  
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_add_staff` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_staff`(
in ID int(10),
in name varchar(20),
in email varchar(30),
in contact varchar(10),
in address text,
in city varchar(20),
in state varchar(20),
in zipcode varchar(6),
in image_path text,
in permanent_address varchar(100), 
in landline_no varchar(50),
in comment varchar(100),
in address_proof_list varchar(200),
in id_proof_list varchar(200),
IN assigned_branch varchar(80),
IN assigned_desination varchar(80)
)
BEGIN
	insert into tbl_staff(ID, name,email,contact,address,city,state,zipcode,image_path,
    permanent_address,landline_no,comment,address_proof_list,id_proof_list)
    values
    (ID, name,email,contact,address,city,state,zipcode,image_path,
    permanent_address,landline_no,comment,address_proof_list,id_proof_list,assigned_desination,assigned_branch);
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_branch` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_branch`()
BEGIN
	select * from tbl_branch;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_follow_up` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_follow_up`()
BEGIN
	select * from tbl_follow_up;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_lead` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_lead`()
BEGIN
	select * from tbl_lead;
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_get_staff` */

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_staff`()
BEGIN
	select * from tbl_staff;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
